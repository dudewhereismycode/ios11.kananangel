//
//  AXABeaconManager.h
//  AXASDK
//
//  Created by AXAET_APPLE on 15/7/15.
//  Copyright (c) 2015年 axaet. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreLocation/CoreLocation.h>
#import <CoreBluetooth/CoreBluetooth.h>

@class AXABeacon;
@class AXABeaconManager;

@protocol AXABeaconManagerDelegate <NSObject>

@optional


- (void)didRangeBeacons:(NSArray *)beacons
             inRegion:( CLBeaconRegion *)region;


-(void)rangingBeaconsDidFailForRegion:(CLBeaconRegion *)region
           withError:(NSError *)error;


-(void)didStartMonitoringForRegion:(CLRegion *)region;


-(void)monitoringDidFailForRegion:(CLRegion *)region
           withError:(NSError *)error;


-(void)didEnterRegion:(CLRegion *)region;


-(void)didExitRegion:(CLRegion *)region;

@end

@protocol AXATagManagerDelegate <NSObject>


@optional
#pragma mark - CoreBlueTooth


- (void)didDiscoverBeacon:(AXABeacon *)beacon;

- (void)didConnectBeacon:(AXABeacon *)beacon;


- (void)didDisconnectBeacon:(AXABeacon *)beacon;


- (void)didGetProximityUUIDForBeacon:(AXABeacon *)beacon;


- (void)didGetMajorMinorPowerAdvInterval:(AXABeacon *)beacon;


- (void)didWritePassword:(BOOL)correct;

#pragma mark - add in version 1.2
- (void)didModifyPasswordRight;

@end

@interface AXABeaconManager : NSObject
@property (nonatomic) id<AXABeaconManagerDelegate> beaconDelegate;
@property (nonatomic) id<AXATagManagerDelegate> tagDelegate;

+ (AXABeaconManager *)sharedManager;


-(void)startRangingBeaconsInRegion:(CLBeaconRegion *)region;


-(void)startMonitoringForRegion:(CLRegion *)region;

-(void)stopRangingBeaconsInRegion:(CLBeaconRegion *)region;


-(void)stopMonitoringForRegion:(CLRegion *)region;


-(void)startAdvertisingWithProximityUUID:(NSString *)proximityUUID
                                   major:(CLBeaconMajorValue)major
                                   minor:(CLBeaconMinorValue)minor
                              identifier:(NSString*)identifier
                                   power:(NSNumber *)power;
/**
 * 是否正在模拟beacon广播
 *
 * @return BOOL
 */
-(BOOL)isAdvertising;


-(void)stopAdvertising;

/**
 *  获取定位权限：允许后台定位，可以支持后台区域推送，网络数据传输等
 * 
 *  需要在plist 文件里面添加NSLocationAlwaysUsageDescription key
 */
- (void)requestAlwaysAuthorization;

/**
 *  获取定位权限：只允许APP运行期间定位，不支持后台区域感知
 *  
 *  需要在plist 文件里面添加NSLocationWhenInUseUsageDescription key
 */
- (void)requestWhenInUseAuthorization;


#pragma mark - CoreBluetooth

// scan ble device
- (void)startFindBleDevices;

// stop scan ble device
- (void)stopFindBleDevices;

// connect ble device
- (void)connectBleDevice:(AXABeacon *)beacon;

// disconnect ble device
- (void)disconnectBleDevice:(AXABeacon *)beacon;

// send  proximityUUID value to ble device
- (void)writeProximityUUID:(NSString *)proximityUUID;

// send value to ble device
- (void)writeMajor:(NSString *)major withMinor:(NSString *)minor withPower:(NSString *)power withAdvInterval:(NSString *)advInterval;

#pragma mark - add in version 1.1
// send  name value to ble device
- (void)writeName:(NSString *)name;

// send  password value to ble device, enter - (void)didWritePassword:(BOOL)correct;
- (void)writePassword:(NSString *)password;

// send  reset commond value to ble device
- (void)resetDevice;

#pragma mark - add in version 1.2
//send change password which must be length of six, right will return none. no will enter - (void)didWritePassword:(BOOL)correct;
- (void)writeModifyPassword:(NSString *)originPsw newPSW:(NSString *)newPsw;

@end
