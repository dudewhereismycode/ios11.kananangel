//
//  Button1ViewController.h
//  KANAN angel
//
//  Created by Jorge Macias on 11/22/17.
//  Copyright © 2017 Jorge Macias. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <ContactsUI/ContactsUI.h>
#import "EnableAccountViewController.h"
#import "AlarmsViewController.h"

@interface Button1ViewController : UIViewController <UITableViewDataSource,UITableViewDelegate,CNContactPickerDelegate>
@property (strong,nonatomic) EnableAccountViewController *enableViewController;
@property (strong,nonatomic) AlarmsViewController *alarmsViewController;
@end
