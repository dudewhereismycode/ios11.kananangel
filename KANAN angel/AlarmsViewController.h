//
//  AlarmsViewController.h
//  KANAN angel
//
//  Created by Jorge Macias on 11/28/17.
//  Copyright © 2017 Jorge Macias. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "PositionsViewController.h"

@interface AlarmsViewController : UIViewController <UINavigationControllerDelegate,UITableViewDataSource,UITableViewDelegate>
@property (nonatomic, assign) NSString *phoneNumber;
@property (nonatomic, assign) NSString *userName;
@property (nonatomic, assign) NSString *lastName;
@property (nonatomic, assign) NSString *button;
@property (strong,nonatomic) PositionsViewController *positionsViewController;

@end
