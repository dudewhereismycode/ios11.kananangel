//
//  PanicViewController.h
//  KANAN angel
//
//  Created by Jorge Macias on 11/29/17.
//  Copyright © 2017 Jorge Macias. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "PositionsViewController.h"
@interface PanicViewController : UIViewController <UITableViewDataSource,UITableViewDelegate>
@property (strong,nonatomic) PositionsViewController *positionsViewController;
@end
