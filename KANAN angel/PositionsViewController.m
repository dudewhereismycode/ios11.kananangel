//
//  PositionsViewController.m
//  KANAN angel
//
//  Created by Jorge Macias on 11/28/17.
//  Copyright © 2017 Jorge Macias. All rights reserved.
//

#import "PositionsViewController.h"
#import <MapKit/MapKit.h>
#import <UIKit/UIKit.h>



@interface PositionsViewController ()

@property (weak, nonatomic) IBOutlet MKMapView *mapView;
@end

@implementation PositionsViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/
-(void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    self.title = NSLocalizedString(@"Map",nil);
    self.navigationController.delegate = self;
    self.navigationController.navigationBar.backgroundColor = [UIColor whiteColor];
    self.navigationController.navigationBar.translucent = YES;
    self.navigationController.navigationBar.backItem.title=@"Alarms";
    [[self navigationController] setNavigationBarHidden:NO animated:YES];
    CLLocationCoordinate2D coord = {.latitude= [self.latitude floatValue], .longitude= [self.longitude floatValue]};
    
    MKCoordinateRegion region;
    MKCoordinateSpan span;
    span.latitudeDelta = 0.005;
    span.longitudeDelta = 0.005;
    region.span = span;
    region.center = coord;
    
      MKPointAnnotation*    newAnnotation= [[MKPointAnnotation alloc] init];
    newAnnotation.title=self.maptitle;
    NSLog(@"MAp title:%@",self.maptitle);
    newAnnotation.coordinate=coord;
    
    [self.mapView addAnnotation:newAnnotation];
    [self.mapView setRegion:region animated:TRUE];
    [self.mapView regionThatFits:region];
    
}
@end
