//
//  BeaconScanViewController.m
//  KANAN angel
//
//  Created by Jorge Macias on 11/3/17.
//  Copyright © 2017 Jorge Macias. All rights reserved.
//

#import "BeaconScanViewController.h"
#import "AppDelegate.h"
#import <CoreBluetooth/CoreBluetooth.h>

@interface BeaconScanViewController () <AXATagManagerDelegate>
@property (nonatomic, strong) NSMutableArray *bleDevices;
@property (nonatomic, strong) NSArray *sortedDevices;
@property (weak, nonatomic) IBOutlet UIProgressView *progressView;
@property (weak, nonatomic) IBOutlet UIButton *scanButton;
@property (weak, nonatomic) IBOutlet UILabel *remainingLabel;
@property (weak, nonatomic) IBOutlet UILabel *headerLabel;
@property (weak, nonatomic) IBOutlet UITableView *beaconTableView;
@property (nonatomic, strong) CBCentralManager* bluetoothManager;
@property (strong, nonatomic) NSTimer *timer;
@property (strong, nonatomic) NSTimer *timer2;
@property (strong, nonatomic) NSString *uuid;
@property (strong, nonatomic) NSString *major;
@property (strong, nonatomic) NSString *minor;
@property (strong, nonatomic) NSString *bleState;
@property NSUInteger *seek21;
@end
int remainingCounts;
int seconds;
int seek;
int wait_seconds;
NSString * bleswitch;

@implementation BeaconScanViewController
{
    NSMutableArray *tableData;
}
- (void)centralManagerDidUpdateState:(CBCentralManager *)central {
    if (central.state == CBManagerStatePoweredOn) {
        [AXABeaconManager sharedManager].tagDelegate = self;
        [[AXABeaconManager sharedManager] startFindBleDevices];
         self.scanButton.enabled=YES;
        [self.scanButton setTitle: NSLocalizedString(@"Tune",nil) forState:UIControlStateNormal];
        self.remainingLabel.text=NSLocalizedString(@"Panic Button List",nil);
        self.navigationItem.title = NSLocalizedString(@"Panic Button Tunner",nil);
        //[self showAlert:@"Altert" :[self detectBluetooth]];
        tableData =  [[NSMutableArray alloc] init];
        self.bleDevices = [[NSMutableArray alloc] init];
        self.sortedDevices = [[NSArray alloc] init];
        AppDelegate *appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
        NSManagedObjectContext *context =
        [appDelegate managedObjectContext];
        NSEntityDescription *entityDesc =
        [NSEntityDescription entityForName:@"Settings"
                    inManagedObjectContext:context];
        NSFetchRequest *request = [[NSFetchRequest alloc] init];
        [request setEntity:entityDesc];
        NSPredicate *pred = [NSPredicate predicateWithFormat:@"(id = %@)", @"1"];
        [request setPredicate:pred];
        NSManagedObject *matches = nil;
        NSError *error;
        NSArray *objects = [context executeFetchRequest:request
                                                  error:&error];
        NSString *valid=@"";
        if ([objects count] == 0) {
            NSLog(@"vacio");
            
        } else {
            matches = objects[0];
            
            valid=[matches valueForKey:@"valid"];
            if ([valid isEqualToString:@"YES"])
            {
                bleswitch=[matches valueForKey:@"blepanicstate"];
                if ([bleswitch isEqualToString:@"YES"]){
                    [self showAlert:NSLocalizedString(@"Message",nil) :NSLocalizedString(@"You must turn off the bluetooth switch in the SOS section",nil)];
                    self.scanButton.enabled=NO;
                }
                else{
                    //NSArray *entries = [NSKeyedUnarchiver unarchiveObjectWithData:[matches valueForKey:@"beaconlist"]];
                    //
                    //tableData= [NSMutableArray arrayWithArray:entries];
                    tableData =  [self retrieveArray];
                    self.scanButton.enabled=YES;
                    [self blecredentials];
                    [self.beaconTableView reloadData];
                }
            }
            else
            {
                [self showAlert:NSLocalizedString(@"Alert",nil) :NSLocalizedString(@"Go to settings and configure account first",nil)];
                self.scanButton.enabled=NO;
            }
            
        }
        //Do what you intend to do
    } else if(central.state == CBManagerStatePoweredOff) {
         self.scanButton.enabled=NO;
        [AXABeaconManager sharedManager].tagDelegate = nil;
        //Bluetooth is disabled. ios pops-up an alert automatically
    }
}
- (void)detectBluetooth
{
    if(!self.bluetoothManager)
    {
        // Put on main queue so we can call UIAlertView from delegate callbacks.
        self.bluetoothManager = [[CBCentralManager alloc] initWithDelegate:self queue:dispatch_get_main_queue()];
    }
    [self centralManagerDidUpdateState:self.bluetoothManager]; // Show initial state
}
- (void)viewDidLoad {
    [super viewDidLoad];
    [self detectBluetooth];
    [self blecredentials];
    self.progressView.progress=0.0;
    // Do any additional setup after loading the view.
}
- (void)viewDidAppear:(BOOL)animated{
    self.headerLabel.text=NSLocalizedString(@"You can add multiple panics buttons but you must tune one at a time. When you tune a button it will be added in the list. You can also delete it.  When you done enable the  switch in the SOS section.",nil);
}
-(NSMutableArray*) retrieveArray {
    NSUserDefaults *userDefaults = [NSUserDefaults standardUserDefaults];
    NSMutableArray *beaconlist = [userDefaults objectForKey:@"beaconlist"];
    NSMutableArray *new = [[NSMutableArray alloc] init];
    for (int i=0;i<beaconlist.count;i++){
        [new addObject:beaconlist[i]];
    }
    NSLog(@"Retrieve Len of array: %lu",(sizeof beaconlist) / (sizeof beaconlist[0]));
    return new;
}
- (void) storeArray: (NSMutableArray *) beaconlist{
    NSUserDefaults *userDefaults = [NSUserDefaults standardUserDefaults];
    [userDefaults setObject:beaconlist forKey:@"beaconlist"];
    [userDefaults synchronize];
    NSLog(@"Store Len of array: %lu",(sizeof beaconlist) / (sizeof beaconlist[0]));
}
- (void)viewWillAppear:(BOOL)animated {
    //[AXABeaconManager sharedManager].tagDelegate = self;
    //[[AXABeaconManager sharedManager] startFindBleDevices];
}

- (void)viewWillDisappear:(BOOL)animated {
    [AXABeaconManager sharedManager].tagDelegate = nil;
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

// General Routines
- (IBAction)scanButtonTapped:(UIButton *)sender {
    [AXABeaconManager sharedManager].tagDelegate = self;
    [[AXABeaconManager sharedManager] startFindBleDevices];
    remainingCounts = 30;
    seconds=0;
    self.scanButton.enabled=NO;
    self.timer = [NSTimer scheduledTimerWithTimeInterval:1
                                                  target:self
                                                selector:@selector(countDown)
                                                userInfo:nil
                                                 repeats:YES];
}

-(void)showAlert:(NSString *)title :(NSString *)body {
    UIAlertController * alert=   [UIAlertController
                                  alertControllerWithTitle:title
                                  message:body
                                  preferredStyle:UIAlertControllerStyleAlert];
    
    UIAlertAction* ok = [UIAlertAction
                         actionWithTitle:@"OK"
                         style:UIAlertActionStyleDefault
                         handler:^(UIAlertAction * action)
                         {
                             [alert dismissViewControllerAnimated:YES completion:nil];
                             
                         }];
    
    [alert addAction:ok];
    [self presentViewController:alert animated:YES completion:nil];
}
- (void) blecredentials{
    NSString *myret;
    AppDelegate *appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    
    NSManagedObjectContext *context =
    [appDelegate managedObjectContext];
    
    NSEntityDescription *entityDesc =
    [NSEntityDescription entityForName:@"Settings"
                inManagedObjectContext:context];
    
    NSFetchRequest *request = [[NSFetchRequest alloc] init];
    [request setEntity:entityDesc];
    
    [request setEntity:entityDesc];
    
    NSPredicate *pred = [NSPredicate predicateWithFormat:@"(id = %@)", @"1"];
    
    [request setPredicate:pred];
    
    NSManagedObject *matches = nil;
    
    NSError *error;
    
    NSArray *objects = [context executeFetchRequest:request
                                              error:&error];
    NSArray* credArr;
    if ([objects count] == 0) {
        NSLog(@"No Data");
        credArr = [NSArray arrayWithObjects: @"", @"",@"",nil];
    }
    else
    {
        matches = objects[0];
        myret=[matches valueForKey:@"valid"];
        //NSArray *entries = [NSKeyedUnarchiver unarchiveObjectWithData:[matches valueForKey:@"beaconlist"]];
        //tableData= [NSMutableArray arrayWithArray:entries];
        tableData =  [self retrieveArray];
        [self.beaconTableView reloadData];
        //        credArr = [NSArray arrayWithObjects: [matches valueForKey:@"iuuid"], [matches valueForKey:@"major"],[matches valueForKey:@"minor"],nil];
    }
    
}
- (void) storeData: (NSString *) email phone: (NSString *) phone_number group: (NSString *) group server: (NSString *) server port: (NSString *) port password: (NSString *) password valid: (NSString *) valid iuuid: (NSString *) iuuid major: (NSString *) major minor: (NSString *) minor blepanicstate: (NSString *) blepanicstate beaconlist: (NSMutableArray *) beaconlist
{
    unsigned long items;
    items=[self countItems];
    if (items==0)
    {
        AppDelegate *appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
        NSManagedObjectContext *context =
        [appDelegate managedObjectContext];
        NSManagedObject *settings;
        settings = [NSEntityDescription
                    insertNewObjectForEntityForName:@"Settings"
                    inManagedObjectContext:context];
        [settings setValue:@"1" forKey:@"id"];
        [settings setValue:iuuid forKey:@"iuuid"];
        [settings setValue:major forKey:@"major"];
        [settings setValue:minor forKey:@"minor"];
        [settings setValue:blepanicstate forKey:@"blepanicstate"];
        //NSData *arrayData = [NSKeyedArchiver archivedDataWithRootObject:beaconlist];
        //[settings setValue:arrayData forKey:@"beaconlist"];
        [self storeArray:beaconlist];
        NSError *error;
        [context save:&error];
        
    }
    else
    {
        AppDelegate *appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
        
        NSManagedObjectContext *context =
        [appDelegate managedObjectContext];
        
        NSEntityDescription *entityDesc =
        [NSEntityDescription entityForName:@"Settings"
                    inManagedObjectContext:context];
        
        NSFetchRequest *request = [[NSFetchRequest alloc] init];
        [request setEntity:entityDesc];
        
        [request setEntity:entityDesc];
        
        NSPredicate *pred = [NSPredicate predicateWithFormat:@"(id = %@)", @"1"];
        
        [request setPredicate:pred];
        NSError *error;
        
        NSArray *results = [context executeFetchRequest:request
                                                  error:&error];
        
        NSManagedObject* favoritsGrabbed = [results objectAtIndex:0];
        [favoritsGrabbed setValue:iuuid forKey:@"iuuid"];
        [favoritsGrabbed setValue:major forKey:@"major"];
        [favoritsGrabbed setValue:minor forKey:@"minor"];
        [favoritsGrabbed setValue:blepanicstate forKey:@"blepanicstate"];
        //NSData *arrayData = [NSKeyedArchiver archivedDataWithRootObject:beaconlist];
        //[favoritsGrabbed setValue:arrayData forKey:@"beaconlist"];
        [self storeArray:beaconlist];
        [context save:&error];
        NSLog(@"updated core data");
    }
    
}
- (unsigned long) countItems {
    
    AppDelegate *appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    
    NSManagedObjectContext *context =
    [appDelegate managedObjectContext];
    
    NSEntityDescription *entityDesc =
    [NSEntityDescription entityForName:@"Settings"
                inManagedObjectContext:context];
    
    NSFetchRequest *request = [[NSFetchRequest alloc] init];
    [request setEntity:entityDesc];
    
    [request setEntity:entityDesc];
    
    NSPredicate *pred = [NSPredicate predicateWithFormat:@"(id = %@)", @"1"];
    
    [request setPredicate:pred];
    
    NSError *error;
    
    NSArray *objects = [context executeFetchRequest:request
                                              error:&error];
    
    return [objects count];
}
-(void)waitBeacon {
    wait_seconds++;
    
    if (wait_seconds==10)
    {
        [self.timer2 invalidate];
        
        NSLog(@"Is the value in the array %d",[tableData containsObject: [NSString stringWithFormat:@"%@-%@", self.major,self.minor]]);
        if([tableData containsObject: [NSString stringWithFormat:@"%@-%@", self.major,self.minor]]
           )
        {
            self.remainingLabel.text=NSLocalizedString(@"Panic Button List",nil);
            NSLog(@"it contains");
        }
        else
        {
            self.remainingLabel.text=NSLocalizedString(@"Panic Button List",nil);
            NSLog(@"does not contains");
            [tableData addObject:[NSString stringWithFormat:@"%@-%@", self.major,self.minor]];
            [self.beaconTableView reloadData];
            
            [self storeData:@"" phone:@"" group:@"" server:@"" port:@"" password:@"" valid:@"" iuuid:self.uuid major:self.major minor:self.minor blepanicstate:@"NO" beaconlist:tableData];
        }
        [self showAlert:NSLocalizedString(@"Message",nil) :NSLocalizedString(@"Tunning Finished! Turn off the panic button",nil)];
        self.remainingLabel.text=NSLocalizedString(@"Panic Button List",nil);
        
    }
    else{
        remainingCounts=10-wait_seconds;
        self.remainingLabel.text=
        [NSString stringWithFormat:NSLocalizedString(@"Remaining seconds: %d",nil), remainingCounts];
    }
}
-(void)countDown {
    seconds++;
    NSLog(@"Progress: %f",(float)seconds/30.0f);
    self.progressView.progress=(float)seconds/30.0f;
    NSLog(@"Remain: %d",remainingCounts);
    self.remainingLabel.text=[NSString stringWithFormat:NSLocalizedString(@"Remaining seconds: %d",nil), remainingCounts];
    if (--remainingCounts == 0) {
        self.remainingLabel.text=NSLocalizedString(@"Panic Button List",nil);
        self.scanButton.enabled=YES;
        [self.timer invalidate];
        self.progressView.progress=0;
        NSLog(@"Found %lu",self.sortedDevices.count);
        for (int i = 0; i < self.sortedDevices.count; i++)
        {
            NSLog(@"%d", i);
            AXABeacon *beacon = [[AXABeacon alloc] init];
            beacon = [self.sortedDevices objectAtIndex:i];
            NSLog(@"%@",[NSString stringWithFormat:@"name: %@", beacon.name]);
            NSLog(@"%@",[NSString stringWithFormat:@"rssi: %@", [beacon.rssi stringValue]]);
            NSLog(@"%@",[NSString stringWithFormat:@"UUID: %@", beacon.proximityUUID]);
            NSLog(@"%@",[NSString stringWithFormat:@"Major: %@", beacon.major]);
            NSLog(@"%@",[NSString stringWithFormat:@"Minor: %@", beacon.minor]);
            if([beacon.name isEqualToString:@"pBeacon_n"]){
                self.remainingLabel.text=@"Wait! connecting with button";
                [AXABeaconManager sharedManager].tagDelegate = self;
                [[AXABeaconManager sharedManager] connectBleDevice:beacon];
                wait_seconds=0;
                self.timer2 = [NSTimer scheduledTimerWithTimeInterval:1
                                                               target:self
                                                             selector:@selector(waitBeacon)
                                                             userInfo:nil
                                                              repeats:YES];}
            
        }
        
    }
}
// Table View
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    // Number of rows is the number of time zones in the region for the specified section.
    return [tableData count];
}


- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    static NSString *simpleTableIdentifier = @"SimpleTableItem";
    
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:simpleTableIdentifier];
    
    if (cell == nil) {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:simpleTableIdentifier];
    }
    
    cell.textLabel.text = [tableData objectAtIndex:indexPath.row];
    cell.textLabel.textAlignment = NSTextAlignmentCenter;
    return cell;
}
- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (editingStyle == UITableViewCellEditingStyleDelete)
    {    AppDelegate *appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
        NSManagedObjectContext *context =
        [appDelegate managedObjectContext];
        NSEntityDescription *entityDesc =
        [NSEntityDescription entityForName:@"Settings"
                    inManagedObjectContext:context];
        NSFetchRequest *request = [[NSFetchRequest alloc] init];
        [request setEntity:entityDesc];
        NSPredicate *pred = [NSPredicate predicateWithFormat:@"(id = %@)", @"1"];
        [request setPredicate:pred];
        NSManagedObject *matches = nil;
        NSError *error;
        NSArray *objects = [context executeFetchRequest:request
                                                  error:&error];
        NSString *valid=@"";
        if ([objects count] == 0) {
            NSLog(@"vacio");
            
        } else {
            matches = objects[0];
            
            valid=[matches valueForKey:@"valid"];
            if ([valid isEqualToString:@"YES"])
            {
                bleswitch=[matches valueForKey:@"blepanicstate"];
                if ([bleswitch isEqualToString:@"YES"]){
                    [self showAlert:NSLocalizedString(@"Message",nil) :NSLocalizedString(@"You must turn off the bluetooth switch in the SOS section",nil)];
                    self.scanButton.enabled=NO;
                }
                else{
                    [tableData removeObjectAtIndex:indexPath.row];
                    [self storeData:@"" phone:@"" group:@"" server:@"" port:@"" password:@"" valid:@"" iuuid:self.uuid major:self.major minor:self.minor blepanicstate:@"NO" beaconlist:tableData];
                    // Delete the row from the data source
                    [tableView deleteRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationAutomatic];
                }
            }
            else
            {
                [self showAlert:NSLocalizedString(@"Alert",nil) :NSLocalizedString(@"Go to settings and configure account first",nil)];
                self.scanButton.enabled=NO;
            }
            
        }
        
    }
}
// Bluetooth
- (void)didDiscoverBeacon:(AXABeacon *)beacon {
    AXABeacon *temp = [self findBeaconWithBeacon:beacon inArray:self.bleDevices];
    if (temp) {
        int index = (int)[self.bleDevices indexOfObject:temp];
        [self.bleDevices replaceObjectAtIndex:index withObject:beacon];
    }
    else {
        [self.bleDevices addObject:beacon];
    }
    
    self.sortedDevices = [self.bleDevices sortedArrayUsingComparator:^NSComparisonResult(id obj1, id obj2) {
        AXABeacon *beacon1 = obj1;
        AXABeacon *beacon2 = obj2;
        return [beacon2.rssi compare:beacon1 .rssi];
    }];
    
}
- (AXABeacon *) findBeaconWithBeacon:(AXABeacon *)beacon inArray:(NSMutableArray *)array {
    for (AXABeacon *temp in array) {
        if ([temp.uuidString isEqualToString:beacon.uuidString]) {
            return temp;
        }
    }
    return nil;
}
- (void)didConnectBeacon:(AXABeacon *)beacon {
    NSLog(@"%@", beacon.name);
}

- (void)didDisconnectBeacon:(AXABeacon *)beacon {
    NSLog(@"%@", beacon.name);
}

- (void)didGetProximityUUIDForBeacon:(AXABeacon *)beacon {
    NSLog(@"uuid: %@", beacon.proximityUUID);
    self.uuid=beacon.proximityUUID;
}

- (void)didGetMajorMinorPowerAdvInterval:(AXABeacon *)beacon {
    NSLog(@"major: %@ minor: %@ power: %@ advInterval: %@", beacon.major, beacon.minor, beacon.power, beacon.advInterval);
    self.major=beacon.major;
    self.minor=beacon.minor;
}
@end

