//
//  LoginViewController.h
//  KANAN angel
//
//  Created by Jorge Macias on 10/30/17.
//  Copyright © 2017 Jorge Macias. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "SOS_ViewController.h"

@interface LoginViewController: UIViewController <UIPickerViewDelegate, UIPickerViewDataSource,UITextFieldDelegate>

{
    
    NSDictionary *dictCodes;
}
@property (strong,nonatomic) SOS_ViewController *locationViewController;
@end
