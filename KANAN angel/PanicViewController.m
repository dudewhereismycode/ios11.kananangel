//
//  PanicViewController.m
//  KANAN angel
//
//  Created by Jorge Macias on 11/29/17.
//  Copyright © 2017 Jorge Macias. All rights reserved.
//

#import "PanicViewController.h"
#import "CoreData.h"
#import "AppDelegate.h"
#import "Panic.h"
@interface PanicViewController ()
@property (weak, nonatomic) IBOutlet UITableView *tableViewAlarm;
@property (weak, nonatomic) NSString *kananError;
@property (weak, nonatomic) NSString *kananMessage;
@end

@implementation PanicViewController{
    NSMutableArray *tableData;
    long  index;
    
}

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
- (void) viewWillAppear:(BOOL)animated{
    [self retrieveAlarms];
}
-(NSString *) getphonenumber{
    CoreData *orm=[[CoreData alloc] init];
    return [orm retrieve:@"phone_number"];
}
-(void)showAlert:(NSString *)title :(NSString *)body {
    UIAlertController * alert=   [UIAlertController
                                  alertControllerWithTitle:title
                                  message:body
                                  preferredStyle:UIAlertControllerStyleAlert];
    
    UIAlertAction* ok = [UIAlertAction
                         actionWithTitle:@"OK"
                         style:UIAlertActionStyleDefault
                         handler:^(UIAlertAction * action)
                         {
                             [alert dismissViewControllerAnimated:YES completion:nil];
                             
                         }];
    
    [alert addAction:ok];
    [self presentViewController:alert animated:YES completion:nil];
}
-(void) retrieveAlarms{
    tableData =  [[NSMutableArray alloc] init];

    NSString *mask=[NSString stringWithFormat:@"%@%@%@", @"http://", KANAN_url, @"/api/noa/listPanicAll?phone=%@"];
    NSString *urlString = [NSString stringWithFormat:mask,
                           [self getphonenumber]
                           ];
    
    NSLog(@"URL: %@",urlString);
    
    NSMutableURLRequest *request = [[NSMutableURLRequest alloc] initWithURL:[NSURL URLWithString:urlString]];
    [request setHTTPMethod:@"GET"];
    [request setValue:@"application/json" forHTTPHeaderField:@"Accept"];
    [request setValue:@"application/json" forHTTPHeaderField:@"Content-Type"];
    NSURLSession *session = [NSURLSession sharedSession];
    [[session dataTaskWithURL:[NSURL URLWithString:urlString]
            completionHandler:^(NSData *responseData,
                                NSURLResponse *responseCode,
                                NSError *responseError)
      {
          // This will get the NSURLResponse into NSHTTPURLResponse format
          NSHTTPURLResponse* httpResponse = (NSHTTPURLResponse*)responseCode;
          // This will Fetch the status code from NSHTTPURLResponse object
          int responseStatusCode = (int)[httpResponse statusCode];
          //Just to make sure, it works or not
          NSLog(@"Status Code :: %d", responseStatusCode);
          NSDictionary *userInfo = [responseError userInfo];
          NSString *errorString = [[userInfo objectForKey:NSUnderlyingErrorKey] localizedDescription];
          NSLog(@"Error string: %@",errorString);
          if ([responseData length] > 0 && responseError == nil)
          {
              id jsonData = [NSJSONSerialization JSONObjectWithData:responseData options:NSJSONReadingAllowFragments error:&responseError];
              NSDictionary *responseDict = jsonData;
              //NSString *kananError = @"";
              //NSString *kananMessage = @"Distributor";
              NSString *title=@"Alert!";
              NSLog(@"sucess reading");
              dispatch_async(dispatch_get_main_queue(), ^{
                  // UI Update
                  if ([responseDict objectForKey:@"Error"])
                  {
                      
                      self.kananError = [responseDict objectForKey:@"Error"];
                      
                      if ([[responseDict objectForKey:@"Error"] isEqual:@"none"])
                      {
                          NSMutableArray *resultArray = [responseDict objectForKeyedSubscript:@"myangels"];
                          Panic *cell = [[Panic alloc]init];
                          
                          
                          for(NSObject *obj in resultArray){
                              NSLog(@"Received: %@", [obj valueForKey:@"received"]);
                              NSLog(@"Last Name: %@", [obj valueForKey:@"battery"]);
                              cell=[[Panic alloc]init];
                              cell.time=[obj valueForKey:@"received"];
                              cell.battery=[obj valueForKey:@"battery"];
                              cell.latitude=[obj valueForKey:@"latitude"];
                              cell.longitude=[obj valueForKey:@"longitude"];
                              cell.response=[obj valueForKey:@"response"];
                              [self->tableData addObject:cell];
                          }
                          
                          [self.tableViewAlarm reloadData];
                      }
                      
                      else
                      {
                          [self showAlert:title : [responseDict objectForKey:@"message"]];
                      }
                      
                  }
              });
              
          }
          
          else if ([responseData length] == 0 && responseError == nil)
          {
              
              [self showAlert: @"Data Error" :@"Please check credentials or connection"];
              NSLog(@"data error: %@", responseError);
          }
          else if (responseError != nil && responseError.code == NSURLErrorTimedOut)
          {
              [self showAlert:@"Data Time Out" :@"Please check internet is available"];
              NSLog(@"data timeout: %ld", (long)NSURLErrorTimedOut);
          }
          else if (responseError != nil)
          {
              [self showAlert:@"Access is denied" :@"Please check credentials or connection"];
              NSLog(@"data download error: %@",responseError);
          }
          
          
          
      }]
     resume];
}
// Table View

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    // Number of rows is the number of time zones in the region for the specified section.
    return [tableData count];
}
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    static NSString *simpleTableIdentifier = @"SimpleTableItem";
    
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:simpleTableIdentifier];
    
    if (cell == nil) {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleSubtitle reuseIdentifier:simpleTableIdentifier];
    }
    Panic *mycell = [[Panic alloc]init];
    mycell=[tableData objectAtIndex:indexPath.row];
    cell.textLabel.text = mycell.time;
    //NSString *battery_info=[mycell.response stringByAppendingString:[@"Battery: " stringByAppendingString:mycell.battery]];
    cell.detailTextLabel.text = mycell.response;
    cell.imageView.image = [UIImage imageNamed:@"SOS_button_24.png"];
    cell.textLabel.textAlignment = NSTextAlignmentLeft;
    cell.accessoryType = UITableViewCellAccessoryDisclosureIndicator;
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    
    /*
     UIButton *button = [UIButton buttonWithType:UIButtonTypeCustom];
     [button addTarget:self
     action:@selector(myCustomFunction:)
     forControlEvents:UIControlEventTouchUpInside];
     [button setTitle:@">" forState:UIControlStateNormal];
     button.frame = CGRectMake(80.0, 210.0, 160.0, 40.0);
     [button setTitleColor:[UIColor blueColor] forState:UIControlStateNormal];
     
     cell.accessoryView = button;
     */
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath
                                                                    *)indexPath

{
    Panic *mycell = [[Panic alloc]init];
    mycell=[tableData objectAtIndex:indexPath.row];
    NSLog(@"latitude: %@",mycell.latitude);
    NSLog(@"latitude: %@",mycell.longitude);
    self.positionsViewController=[self.storyboard instantiateViewControllerWithIdentifier:@"positions"];
    self.positionsViewController.latitude=mycell.latitude;
    self.positionsViewController.longitude=mycell.longitude;
    self.positionsViewController.maptitle=mycell.response;
    self.navigationController.navigationBar.backItem.title=@"Alarms";
    [self.navigationController pushViewController:self.positionsViewController animated:NO];
    
}

@end
