//
//  AppDelegate.m
//  KANAN angel
//
//  Created by Jorge Macias on 10/30/17.
//  Copyright © 2017 Jorge Macias. All rights reserved.
//

#import "AppDelegate.h"
#import "LoginViewController.h"
#import "GetLocationViewController.h"
#import "ViewController.h"
#import "ViewController.h"

#import <Security/Security.h>
#import "LocationService.h"
#import "LocationService3G.h"
#import "LocationServiceSat.h"
#import <UserNotifications/UserNotifications.h>

@interface AppDelegate ()

@end
//NSString *const KANAN_url = @"app.dwim.mx";
NSString *const KANAN_url = @"angelapp.dwim.mx";

@implementation AppDelegate
@synthesize inputStream,outputStream,myWIFILocation,my3GLocation,mySatLocation,myLocation;
//http://app.dwim.mx/api/noa/setAngel/?myphone=525543584656&angel_phone=525548885144&enable=True
//http://app.dwim.mx/api/noa/panic/?imei=525543584656&email=angel%40dwim.mx&group=angel&latitude=19.52339293902696&longitude=-99.23887448394599&speed=0&battery=100&type=Medical

- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions {
 
    // Override point for customization after application launch.
    //share model for significative changes tracking in the background
    self.shareModel = [LocationManager sharedManager];
    self.shareModel.afterResume = NO;
    [self.shareModel addApplicationStatusToPList:@"didFinishLaunchingWithOptions"];
    // Ask the user to allow notifications
    if ([application respondsToSelector:@selector(isRegisteredForRemoteNotifications)]) {
        
        UNUserNotificationCenter *center = [UNUserNotificationCenter currentNotificationCenter];
        [center requestAuthorizationWithOptions:(UNAuthorizationOptionAlert + UNAuthorizationOptionSound + UNAuthorizationOptionBadge) completionHandler:^(BOOL granted, NSError * _Nullable error) {
            if (!granted) {
                //Show alert asking to go to settings and allow permission
            }
        }];
    }
    application.applicationIconBadgeNumber = 0;
    // Start remote notifications
    [self registerForRemoteNotification];
    //Register NS Notification Center for Local Notifications
    [[NSNotificationCenter defaultCenter] removeObserver:self name:@"updateLeftTable"
                                                  object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(method4NSnotificationCenter:) name:@"localNotification" object:nil];
    //[self showLocalNotification:@"Hola":@"Como estas?"];
    //[self showAlert:@"Hello" :@"world"];
    if([[UIApplication sharedApplication] backgroundRefreshStatus] == UIBackgroundRefreshStatusDenied){
        [self showAlert:@"Error" :@"The app doesn't work without the Background App Refresh enabled. To turn it on, go to Settings > General > Background App Refresh"];
        
    }else if([[UIApplication sharedApplication] backgroundRefreshStatus] == UIBackgroundRefreshStatusRestricted){
        [self showAlert:@"Error" :@"The functions of this app are limited because the Background App Refresh is disable."];
    } else{
        
        // When there is a significant changes of the location,
        // The key UIApplicationLaunchOptionsLocationKey will be returned from didFinishLaunchingWithOptions
        // When the app is receiving the key, it must reinitiate the locationManager and get
        // the latest location updates
        
        // This UIApplicationLaunchOptionsLocationKey key enables the location update even when
        // the app has been killed/terminated (Not in th background) by iOS or the user.
        
        NSLog(@"UIApplicationLaunchOptionsLocationKey : %@" , [launchOptions objectForKey:UIApplicationLaunchOptionsLocationKey]);
        if ([launchOptions objectForKey:UIApplicationLaunchOptionsLocationKey]) {
            
            // This "afterResume" flag is just to show that he receiving location updates
            // are actually from the key "UIApplicationLaunchOptionsLocationKey"
            AppDelegate *appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
            NSManagedObjectContext *context =
            [appDelegate managedObjectContext];
            NSEntityDescription *entityDesc =
            [NSEntityDescription entityForName:@"Settings" inManagedObjectContext:context];
            NSFetchRequest *request = [[NSFetchRequest alloc] init];
            [request setEntity:entityDesc];
            [request setEntity:entityDesc];
            NSPredicate *pred = [NSPredicate predicateWithFormat:@"(id = %@)", @"1"];
            [request setPredicate:pred];
            NSManagedObject *matches = nil;
            NSError *error;
            NSArray *objects = [context executeFetchRequest:request error:&error];
            if ([objects count] != 0) {
                matches = objects[0];
                NSString *slc;
                NSString *valid;
                slc = [matches valueForKey:@"slc"];
                valid = [matches valueForKey:@"valid"];
                //debug = [matches valueForKey:@"debug"];
                if ([valid isEqualToString:@"YES"]) {
                    if ([slc isEqualToString:@"enable"]){
                        self.shareModel.afterResume = YES;
                        [self.shareModel startMonitoringLocation];
                        [self.shareModel addResumeLocationToPList];
                    }
                }
            }
        }
    }
    [[LocationService sharedInstance] addObserver:self forKeyPath:@"LocationWifi" options:NSKeyValueObservingOptionNew context:nil];
    [[LocationService3G sharedInstance] addObserver:self forKeyPath:@"Location3g" options:NSKeyValueObservingOptionNew context:nil];
    [[LocationServiceSat sharedInstance] addObserver:self forKeyPath:@"LocationSat" options:NSKeyValueObservingOptionNew context:nil];
    
    // test
    //Model *test=[[Model alloc] init];
    //test.number1=43;
    //test.number2=23;
    //NSLog(@"Suma: %d",[test suma]);
    NSString *valid = [self retrieveValid];
    NSLog(@"valid:%@",valid);
    if ([valid isEqualToString:@"NO"])
    {
       // [self showMainScreen:NO];
        [self showGetLocationScreen:NO];
    }
    return YES;
}

-(void) showMainScreen:(BOOL)animated
{
    
    // Get login screen from storyboard and present it
    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
    LoginViewController *viewController = (LoginViewController *)[storyboard instantiateViewControllerWithIdentifier:@"sos"];
    [self.window makeKeyAndVisible];
    [self.window.rootViewController presentViewController:viewController
                                                 animated:animated
                                               completion:nil];
}
-(void) showGetLocationScreen:(BOOL)animated
{
    
    // Get login screen from storyboard and present it
    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
    GetLocationViewController *viewController = (GetLocationViewController *)[storyboard instantiateViewControllerWithIdentifier:@"getlocation"];
    [self.window makeKeyAndVisible];
    [self.window.rootViewController presentViewController:viewController
                                                 animated:animated
                                               completion:nil];
}
- (void)applicationWillResignActive:(UIApplication *)application {
    // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
    // Use this method to pause ongoing tasks, disable timers, and invalidate graphics rendering callbacks. Games should use this method to pause the game.
}


- (void)applicationDidEnterBackground:(UIApplication *)application {
    // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
    // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
    NSLog(@"applicationDidEnterBackground");
    [self.shareModel restartMonitoringLocation];
    [self.shareModel addApplicationStatusToPList:@"applicationDidEnterBackground"];
    
}


- (void)applicationWillEnterForeground:(UIApplication *)application {
    // Called as part of the transition from the background to the active state; here you can undo many of the changes made on entering the background.
    application.applicationIconBadgeNumber=0;
}


- (void)applicationDidBecomeActive:(UIApplication *)application {
    // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
    NSLog(@"applicationDidBecomeActive");
    
    [self.shareModel addApplicationStatusToPList:@"applicationDidBecomeActive"];
    //Remove the "afterResume" Flag after the app is active again.
    self.shareModel.afterResume = NO;
    [self.shareModel startMonitoringLocation];
    
}


- (void)applicationWillTerminate:(UIApplication *)application {
    // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
    NSLog(@"applicationWillTerminate");
    [self.shareModel addApplicationStatusToPList:@"applicationWillTerminate"];
    [self saveContext];
    
}

#pragma mark - Class Methods

/*  Alert  */

-(void)showAlert:(NSString *)title :(NSString *)body {
    UIWindow* topWindow = [[UIWindow alloc] initWithFrame:[UIScreen mainScreen].bounds];
    topWindow.rootViewController = [UIViewController new];
    topWindow.windowLevel = UIWindowLevelAlert + 1;
    
    UIAlertController* alert = [UIAlertController alertControllerWithTitle:title message:body preferredStyle:UIAlertControllerStyleAlert];
    
    [alert addAction:[UIAlertAction actionWithTitle:NSLocalizedString(@"OK",@"confirm") style:UIAlertActionStyleCancel handler:^(UIAlertAction * _Nonnull action) {
        // continue your work
        
        // important to hide the window after work completed.
        // this also keeps a reference to the window until the action is invoked.
        topWindow.hidden = YES;
    }]];
    
    [topWindow makeKeyAndVisible];
    [topWindow.rootViewController presentViewController:alert animated:YES completion:nil];
}
/* Local Notifications */

- (void)userNotificationCenter:(UNUserNotificationCenter *)center willPresentNotification:(UNNotification *)notification withCompletionHandler:(void (^)(UNNotificationPresentationOptions options))completionHandler{
    
    NSLog(@"User Info = %@",notification.request.content.userInfo);
    
    completionHandler(UNNotificationPresentationOptionAlert | UNNotificationPresentationOptionBadge | UNNotificationPresentationOptionSound);
}

- (void)userNotificationCenter:(UNUserNotificationCenter *)center didReceiveNotificationResponse:(UNNotificationResponse *)response withCompletionHandler:(void(^)(void))completionHandler{
    
    NSLog(@"User Info = %@",response.notification.request.content.userInfo);
    completionHandler();
}

-(void)showLocalNotification:(NSString *)title :(NSString *)body {
    UNMutableNotificationContent *content = [[UNMutableNotificationContent alloc] init];
    content.title = [NSString localizedUserNotificationStringForKey:title arguments:nil];
    content.body = [NSString localizedUserNotificationStringForKey:body
                                                         arguments:nil];
    content.sound = [UNNotificationSound defaultSound];
    UNTimeIntervalNotificationTrigger *trigger = [UNTimeIntervalNotificationTrigger
                                                  triggerWithTimeInterval:1
                                                  repeats:NO];
    UNNotificationRequest *request = [UNNotificationRequest requestWithIdentifier:@"NOTIFICATION"
                                                                          content:content
                                                                          trigger:trigger];
    
    
    UNUserNotificationCenter *center = [UNUserNotificationCenter currentNotificationCenter];
    center.delegate=self;
    [center addNotificationRequest:request withCompletionHandler:^(NSError * _Nullable error) {
        if (!error) {
            NSLog(@"success");
        }
    }];
}
-(void)method4NSnotificationCenter:(NSNotification *)notification
{
    if ([[notification name] isEqualToString:@"localNotification"])
    {
        NSLog(@"LOCAL NOTIFICACION");
        NSDictionary* userInfo = notification.userInfo;
        NSString* title = (NSString*)userInfo[@"title"];
        NSString* body = (NSString*)userInfo[@"body"];
        [self showLocalNotification:title:body];
    }
}
-(void) fireLocalNotification:(NSString *) message
{
    
    NSLog(@"fire Local Notification");

        
        //Notification Content
        UNMutableNotificationContent *content = [[UNMutableNotificationContent alloc] init];
        content.body =  [NSString stringWithFormat:@"%@",message];
        content.sound = [UNNotificationSound defaultSound];
        
        //Set Badge Number
        content.badge = @([[UIApplication sharedApplication] applicationIconBadgeNumber] + 1);
        
        // Deliver the notification in five seconds.
        UNTimeIntervalNotificationTrigger *trigger = [UNTimeIntervalNotificationTrigger
                                                      triggerWithTimeInterval:1.0f repeats:NO];
        
        //Notification Request
        UNNotificationRequest *request = [UNNotificationRequest requestWithIdentifier:@"LocalNotification" content:content trigger:trigger];
        
        //schedule localNotification
        UNUserNotificationCenter *center = [UNUserNotificationCenter currentNotificationCenter];
        center.delegate = self;
        [center addNotificationRequest:request withCompletionHandler:^(NSError * _Nullable error) {
            if (!error) {
                NSLog(@"add Notification Request succeeded!");
            }
        }];
        

}
/**
 Remote Notifications
 */

- (void)registerForRemoteNotification {
    [[UIApplication sharedApplication] registerForRemoteNotifications];
    
}
- (void)application:(UIApplication *)application didFailToRegisterForRemoteNotificationsWithError:(NSError *)error
{
    NSLog(@"Failed To Register For Remote Notifications With Error: %@", error);
    
}
- (void)application:(UIApplication *)application didRegisterForRemoteNotificationsWithDeviceToken:(NSData *)newDeviceToken
{
    NSString *rawToken;
    NSString *kananToken;
    NSLog(@"Raw Token: %@", newDeviceToken);
    rawToken=[NSString stringWithFormat:@"%@",newDeviceToken];
    kananToken=[rawToken stringByReplacingOccurrencesOfString:@" " withString:@""];
    kananToken=[kananToken stringByReplacingOccurrencesOfString:@"<" withString:@""];
    kananToken=[kananToken stringByReplacingOccurrencesOfString:@">" withString:@""];
    NSLog(@"Token: %@",kananToken);
    if ([self countItems]==0)
    {
        AppDelegate *appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
        NSManagedObjectContext *context =
        [appDelegate managedObjectContext];
        NSManagedObject *settings;
        settings = [NSEntityDescription
                    insertNewObjectForEntityForName:@"Settings"
                    inManagedObjectContext:context];
        [settings setValue:@"1" forKey:@"id"];
        [settings setValue:kananToken forKey:@"token"];
        [settings setValue:@"NO" forKey:@"valid"];
        [settings setValue:@"kanan_yellow1.png" forKey:@"button1"];
        [settings setValue:@"kanan_yellow5.png" forKey:@"button2"];
        [settings setValue:@"SOS_button_82x83.png" forKey:@"button3"];
        [settings setValue:@"policia verde.png" forKey:@"button4"];
        [settings setValue:@"kanan_yellow2.png" forKey:@"button5"];
        [settings setValue:@"" forKey:@"response1"];
        [settings setValue:@"" forKey:@"response2"];
        [settings setValue:@"" forKey:@"response3"];
        [settings setValue:@"" forKey:@"response4"];
        [settings setValue:@"" forKey:@"response5"];
        [settings setValue:@"0" forKey:@"time1"];
        [settings setValue:@"0" forKey:@"time2"];
        [settings setValue:@"0" forKey:@"time3"];
        [settings setValue:@"0" forKey:@"time4"];
        [settings setValue:@"0" forKey:@"time5"];
        [settings setValue:@"F" forKey:@"follow1"];
        [settings setValue:@"F" forKey:@"follow2"];
        [settings setValue:@"F" forKey:@"follow3"];
        [settings setValue:@"F" forKey:@"follow4"];
        [settings setValue:@"F" forKey:@"follow5"];

        
        NSLog(@"New Stored token: %@",kananToken);
        NSError *error;
        [context save:&error];
        
    }
    else
    {
        AppDelegate *appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
        
        NSManagedObjectContext *context =
        [appDelegate managedObjectContext];
        
        NSEntityDescription *entityDesc =
        [NSEntityDescription entityForName:@"Settings"
                    inManagedObjectContext:context];
        
        NSFetchRequest *request = [[NSFetchRequest alloc] init];
        [request setEntity:entityDesc];
        
        [request setEntity:entityDesc];
        
        NSPredicate *pred = [NSPredicate predicateWithFormat:@"(id = %@)", @"1"];
        
        [request setPredicate:pred];
        NSError *error;
        
        NSArray *results = [context executeFetchRequest:request
                                                  error:&error];
        
        NSManagedObject* favoritsGrabbed = [results objectAtIndex:0];
        [favoritsGrabbed setValue:kananToken forKey:@"token"];
        [context save:&error];
        NSLog(@"Stored token: %@",kananToken);
        
    }
    
}
-(void)application:(UIApplication *)application didReceiveRemoteNotification:(NSDictionary *)userInfo fetchCompletionHandler:(void (^)(UIBackgroundFetchResult))completionHandler
{
    NSLog(@"Remote notification in progress!!");
    NSString *command = userInfo[@"kanan"][@"command"];
    NSLog(@"%@",command);
    //[self showLocalNotification:@"Command:":command];
    if([userInfo[@"aps"][@"content-available"] intValue]== 1) //Silent notification
    {
        if([command isEqual:@"changegroup"]) {
            NSString *mygroup = userInfo[@"kanan"][@"group"];
            NSLog(@"change group command");
            unsigned long items;
            items=[self countItems];
            NSLog(@"Items: %lu",items);
            if (items==1)
            {
                AppDelegate *appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
                
                NSManagedObjectContext *context =
                [appDelegate managedObjectContext];
                
                NSEntityDescription *entityDesc =
                [NSEntityDescription entityForName:@"Settings"
                            inManagedObjectContext:context];
                
                NSFetchRequest *request = [[NSFetchRequest alloc] init];
                [request setEntity:entityDesc];
                
                [request setEntity:entityDesc];
                
                NSPredicate *pred = [NSPredicate predicateWithFormat:@"(id = %@)", @"1"];
                
                [request setPredicate:pred];
                NSError *error;
                
                NSArray *results = [context executeFetchRequest:request
                                                          error:&error];
                
                NSManagedObject* favoritsGrabbed = [results objectAtIndex:0];
                [favoritsGrabbed setValue:mygroup forKey:@"group"];
                [context save:&error];
                NSLog(@"New group: %@",mygroup);
                [[NSNotificationCenter defaultCenter] postNotificationName:@"updategrouptextfield" object:self];
            }
        }
        int pass;
        pass=0;
        if([command isEqual:@"request4GPSlocation"]) {
            NSString *timeStampx = userInfo[@"kanan"][@"Timestamp"];
            NSString *timeStamp = [timeStampx stringByAppendingString:@" UTC"];
            NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
            [dateFormatter setDateFormat:@"yyyyMMddHHmmsszzz"];
            NSDate *timeStampDate = [dateFormatter dateFromString:timeStamp];
            NSDate *currentDate = [[NSDate alloc] init];
            NSInteger days=[self daysBetweenDate:timeStampDate andDate:currentDate];
            //NSLog(@"Days: %d",days);
            NSTimeInterval distanceBetweenDates = [currentDate timeIntervalSinceDate:timeStampDate];
            double secondsInAnHour = 3600;
            NSInteger hoursBetweenDates = distanceBetweenDates / secondsInAnHour;
            //NSLog(@"hours: %d",hoursBetweenDates);
            NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
            [formatter setDateFormat:@"yyyy-MM-dd HH:mm:ss.SSSZ"];
            
            //NSString *msgString = [NSString stringWithFormat:@"D:%d H:%d D1:%@ D2:%@", days,hoursBetweenDates,timeStampDate,currentDate];
            //
            if (days<1){
                if (hoursBetweenDates<1){
                    pass=1;
                }
            }
            
        }
        if(pass==1) {
            _backgroundCompletionHandler = completionHandler;
            myWIFILocation.latitude=0;
            myWIFILocation.longitude=0;
            my3GLocation.latitude=0;
            my3GLocation.longitude=0;
            mySatLocation.latitude=0;
            mySatLocation.longitude=0;
            [[LocationServiceSat sharedInstance] startUpdatingLocation];
            [[LocationService sharedInstance] startUpdatingLocation];
            [[LocationService3G sharedInstance] startUpdatingLocation];
            NSInteger intCount = [[NSUserDefaults standardUserDefaults] integerForKey: @"Count"];
            NSLog(@"Count:%ld",(long)intCount);
            intCount++;
            if(intCount>300)
            {
                [self.shareModel deleteLocationsToPlist];
                intCount=1;
            }
            // Store
            [[NSUserDefaults standardUserDefaults] setInteger: intCount forKey:@"Count"];
            [[NSUserDefaults standardUserDefaults] synchronize];
            
            
            [NSTimer scheduledTimerWithTimeInterval:28.0
                                             target:self
                                           selector:@selector(timeUp:)
                                           userInfo:nil
                                            repeats:NO];
            notificationInProgress=1;
            NSLog(@"Silent Notification Received");
            NSString *scounter= [NSString stringWithFormat:@"%ld", (long)intCount];
            NSString *message=@"silentNotification:";
            message=[message stringByAppendingString:scounter];
            [self.shareModel addApplicationStatusToPList:message];
            
            [NSTimer scheduledTimerWithTimeInterval:10.0
                                             target:self
                                           selector:@selector(stopGPS:)
                                           userInfo:nil
                                            repeats:NO];
            if ([CLLocationManager authorizationStatus] == kCLAuthorizationStatusAuthorizedAlways) {
                //stop GPS after 5 seconds and then send string to kanan server
                
            }
            else{
                [self showLocalNotification:@"KANAN Location services deactivated":@"please enable location services for KANAN Road"];
                
            }
            
            return;
        }
        
    }
    else  //User notifications
    {


        if ([command isEqual:@"showMessage"])
        {
            NSString *message = userInfo[@"aps"][@"alert"];
            [self showAlert:@"Message" : message];
        }
 
        
    }
    completionHandler(UIBackgroundFetchResultNoData);
    return;
    
}

/* General Routines */
- (void) stopGPS:(NSTimer *)timer
{
    NSString* source=@"";
    //[locationManager stopUpdatingLocation];
    
    // Send Data to kanan server
    mySatLocation.latitude=[LocationServiceSat sharedInstance].currentLocation.coordinate.latitude;
    mySatLocation.longitude=[LocationServiceSat sharedInstance].currentLocation.coordinate.longitude;
    
    NSLog(@"GPS Vertical accuracy %f",[LocationServiceSat sharedInstance].currentLocation.verticalAccuracy);
    myWIFILocation.latitude=[LocationService sharedInstance].currentLocation.coordinate.latitude;
    myWIFILocation.longitude=[LocationService sharedInstance].currentLocation.coordinate.longitude;
    self.myWIFILocationAccuracy=[LocationService sharedInstance].currentLocation.horizontalAccuracy;
    NSLog(@"WIFI Vertical accuracy %f",[LocationService sharedInstance].currentLocation.verticalAccuracy);
    my3GLocation.latitude=[LocationService3G sharedInstance].currentLocation.coordinate.latitude;
    my3GLocation.longitude=[LocationService3G sharedInstance].currentLocation.coordinate.longitude;
    self.my3GLocationAccuracy=[LocationService3G sharedInstance].currentLocation.horizontalAccuracy;
    NSLog(@"3G Vertical accuracy %f",[LocationService3G sharedInstance].currentLocation.verticalAccuracy);
    
    // Stop location Manager
    [[LocationService3G sharedInstance] stopUpdatingLocation];
    [[LocationService sharedInstance] stopUpdatingLocation];
    [[LocationServiceSat sharedInstance] stopUpdatingLocation];
    
    NSString *lat_sat = [NSString stringWithFormat:@"%.06f", mySatLocation.latitude];
    NSString *lon_sat = [NSString stringWithFormat:@"%.06f", mySatLocation.longitude];
    NSLog(@"Sat: %@,%@",lat_sat,lon_sat);
    NSString *lat_wifi = [NSString stringWithFormat:@"%.06f", myWIFILocation.latitude];
    NSString *lon_wifi = [NSString stringWithFormat:@"%.06f", myWIFILocation.longitude];
    NSLog(@"Wifi: %@,%@",lat_wifi,lon_wifi);
    NSString *lat_3g = [NSString stringWithFormat:@"%.06f", my3GLocation.latitude];
    NSString *lon_3g = [NSString stringWithFormat:@"%.06f", my3GLocation.longitude];
    NSLog(@"3G: %@,%@",lat_3g,lon_3g);
    
    
    NSString *valid = @"";
    //    NSString *debug = @"";
    NSString *phone_number = @"";
    NSString *group = @"";
    NSString *email = @"";
    NSString *ner=@"35";
    NSString *validstream=@"A";
    if (mySatLocation.latitude!=0 && myLocation.longitude!=0)
    {
        myLocation.latitude=mySatLocation.latitude;
        myLocation.longitude=mySatLocation.longitude;
        self.myHorizontalLocationAccuracy=[LocationServiceSat sharedInstance].currentLocation.horizontalAccuracy;
        self.myVerticalLocationAccuracy=[LocationServiceSat sharedInstance].currentLocation.verticalAccuracy;
        source=@"S";
    }
    else
    {
        if (myWIFILocation.latitude!=0 && myWIFILocation.longitude!=0)
        {
            myLocation.latitude=myWIFILocation.latitude;
            myLocation.longitude=myWIFILocation.longitude;
            self.myHorizontalLocationAccuracy=[LocationService sharedInstance].currentLocation.horizontalAccuracy;
            self.myVerticalLocationAccuracy=[LocationService sharedInstance].currentLocation.verticalAccuracy;
            source=@"W";
        }
        else{
            myLocation.latitude=my3GLocation.latitude;
            myLocation.longitude=my3GLocation.longitude;
            self.myHorizontalLocationAccuracy=[LocationService3G sharedInstance].currentLocation.horizontalAccuracy;
            self.myVerticalLocationAccuracy=[LocationService3G sharedInstance].currentLocation.verticalAccuracy;
            source=@"3";
            
        }
    }
    NSString *latitude = [NSString stringWithFormat:@"%.06f", myLocation.latitude];
    NSString *longitude = [NSString stringWithFormat:@"%.06f", myLocation.longitude];
    if (self.myHorizontalLocationAccuracy>200){
        ner=@"69";
        validstream=@"V";
    }
    
    AppDelegate *appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    NSManagedObjectContext *context =
    [appDelegate managedObjectContext];
    NSEntityDescription *entityDesc =
    [NSEntityDescription entityForName:@"Settings" inManagedObjectContext:context];
    NSFetchRequest *request = [[NSFetchRequest alloc] init];
    [request setEntity:entityDesc];
    [request setEntity:entityDesc];
    NSPredicate *pred = [NSPredicate predicateWithFormat:@"(id = %@)", @"1"];
    [request setPredicate:pred];
    NSManagedObject *matches = nil;
    NSError *error;
    NSArray *objects = [context executeFetchRequest:request error:&error];
    if ([objects count] != 0) {
        matches = objects[0];
        phone_number = [matches valueForKey:@"phone_number"];
        group = [matches valueForKey:@"group"];
        email = [matches valueForKey:@"email"];
        valid = [matches valueForKey:@"valid"];
        //debug = [matches valueForKey:@"debug"];
        if ([valid isEqualToString:@"YES"]) {
            NSLog(@"Valid");
            NSNumber *hacc=[NSNumber numberWithDouble:self.myHorizontalLocationAccuracy];
            NSNumber *vacc=[NSNumber numberWithDouble:self.myVerticalLocationAccuracy];
            [self.shareModel addApplicationStatusToPList:[NSString stringWithFormat:@"%@:%@:%@ %.06f,%.06f",source, hacc,vacc,myLocation.latitude, myLocation.longitude]];
            int sp;
            NSString *myspeed;
            sp=(int) self.speed;
            if (sp<0)
                sp=0;
            myspeed=[NSString stringWithFormat:@"%d", sp];
            [self send2server:latitude longitude:longitude speed:myspeed imei:phone_number group:group email:email];
        }
        else{
            NSLog(@"Not valid");
            
        }
        notificationInProgress=0;
    }
    notificationInProgress=0;
}

- (void)timeUp:(NSTimer *)timer
{
    [self.shareModel addApplicationStatusToPList:@"closePort"];
    
    [self callBackgroundCompletionHandler:NO];
}
- (void)callBackgroundCompletionHandler:(BOOL)succeeded
{
    NSLog(@"Call completion Handler");
    @synchronized(self)
    {
        if (_backgroundCompletionHandler) {
            _backgroundCompletionHandler(succeeded ? UIBackgroundFetchResultNewData : UIBackgroundFetchResultFailed);
            _backgroundCompletionHandler = NULL;
        }
    }
}

- (void) messageReceived:(NSString *)message {
    
}

- (void) send2server: (NSString *) latitude longitude:(NSString *) longitude speed: (NSString *) speed imei: (NSString *) imei group: (NSString *) group email: (NSString *) email
{
    
    NSString *battery = [[NSNumber numberWithDouble:[self batterylevel]] stringValue];
    NSString *femail = [email stringByAddingPercentEncodingWithAllowedCharacters:[NSCharacterSet URLHostAllowedCharacterSet]];
    NSString *fgroup = [group stringByAddingPercentEncodingWithAllowedCharacters:[NSCharacterSet URLHostAllowedCharacterSet]];
    
    NSString *mask=[NSString stringWithFormat:@"%@%@%@", @"http://", KANAN_url, @"/api/noa/location/?imei=%@&email=%@&group=%@&latitude=%@&longitude=%@&speed=%@&battery=%@"];
    NSString *urlString = [NSString stringWithFormat:mask,
                           imei,
                           femail,
                           fgroup,
                           latitude,
                           longitude,
                           speed,
                           battery];
    
    NSMutableURLRequest *request = [[NSMutableURLRequest alloc] initWithURL:[NSURL URLWithString:urlString]];
    
    [request setHTTPMethod:@"GET"];
    [request setValue:@"application/json" forHTTPHeaderField:@"Accept"];
    [request setValue:@"application/json" forHTTPHeaderField:@"Content-Type"];
    // Send your request asynchronously
    NSLog(@"%@",urlString);
    NSLog(@"Just before send asyncronous ");
    NSURLSession *session = [NSURLSession sharedSession];
    [[session dataTaskWithURL:[NSURL URLWithString:urlString]
            completionHandler:^(NSData *responseData,
                                NSURLResponse *responseCode,
                                NSError *responseError)
      {
          // This will get the NSURLResponse into NSHTTPURLResponse format
          NSHTTPURLResponse* httpResponse = (NSHTTPURLResponse*)responseCode;
          // This will Fetch the status code from NSHTTPURLResponse object
          int responseStatusCode = (int)[httpResponse statusCode];
          //Just to make sure, it works or not
          NSLog(@"Status Code :: %d", responseStatusCode);
          NSDictionary *userInfo = [responseError userInfo];
          NSString *errorString = [[userInfo objectForKey:NSUnderlyingErrorKey] localizedDescription];
          NSLog(@"Error string: %@",errorString);
          if ([responseData length] > 0 && responseError == nil)
          {
              id jsonData = [NSJSONSerialization JSONObjectWithData:responseData options:NSJSONReadingAllowFragments error:&responseError];
              NSDictionary *responseDict = jsonData;
              NSString *kananError = @"";
              NSLog(@"Success location");
              if ([[responseDict objectForKey:@"Error"] isEqual:@"none"])
              {
                  NSLog(@"Success");
                  kananError=[responseDict objectForKey:@"Error"];
                  NSLog(@"Error: %@",kananError);
                  unsigned long items;
                  items=[self countItems];
                  if (items>0)
                  {
                      [self sendlostpositions];
                      [self deleteprocessedpositions];
                  }
              }
              
              else
              {
                  [self showLocalNotification:@"Message":@"The account is not longer valid"];
                  [self storelostlocation4imei:imei email:femail group:fgroup latitude:latitude longitude:longitude speed:speed battery:battery];
              }
              
          }
          
          else
          {
              [self storelostlocation4imei:imei email:femail group:fgroup latitude:latitude longitude:longitude speed:speed battery:battery];
          }
          
      }]
     resume];
    
}

- (void) senderror: (NSString *) error message: (NSString *) message
{
    [self showAlert:error :message];
}

- (void) storelostlocation4imei: (NSString*) imei email: (NSString *) email group: (NSString *) group latitude: (NSString *) latitude longitude: (NSString *) longitude speed: (NSString *) speed battery: (NSString *) battery
{
    NSDate *currentDate = [[NSDate alloc] init];
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    [dateFormatter setTimeZone:[NSTimeZone timeZoneWithAbbreviation:@"UTC"]];
    [dateFormatter setDateFormat:@"yyMMddHHmmss"];
    NSString *utcDateString = [dateFormatter stringFromDate:currentDate];
    AppDelegate *appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    NSManagedObjectContext *context =
    [appDelegate managedObjectContext];
    NSManagedObject *settings;
    settings = [NSEntityDescription
                insertNewObjectForEntityForName:@"Lost"
                inManagedObjectContext:context];
    [settings setValue:@"NO" forKey:@"processed"];
    [settings setValue:imei forKey:@"imei"];
    [settings setValue:email forKey:@"email"];
    [settings setValue:group forKey:@"group"];
    [settings setValue:utcDateString forKey:@"date"];
    [settings setValue:latitude forKey:@"latitude"];
    [settings setValue:longitude forKey:@"longitude"];
    [settings setValue:speed forKey:@"speed"];
    [settings setValue:battery forKey:@"battery"];
    NSError *error;
    [context save:&error];
}
- (unsigned long) countLostItems {
    AppDelegate *appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    
    NSManagedObjectContext *context =
    [appDelegate managedObjectContext];
    
    NSEntityDescription *entityDesc =
    [NSEntityDescription entityForName:@"Lost"
                inManagedObjectContext:context];
    
    NSFetchRequest *request = [[NSFetchRequest alloc] init];
    [request setEntity:entityDesc];
    
    [request setEntity:entityDesc];
    
    NSPredicate *pred = [NSPredicate predicateWithFormat:@"(processed = %@)", @"NO"];
    
    [request setPredicate:pred];
    
    NSError *error;
    
    NSArray *objects = [context executeFetchRequest:request
                                              error:&error];
    return [objects count];
}
-(void) deleteprocessedpositions {
    AppDelegate *appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    
    NSManagedObjectContext *context =
    [appDelegate managedObjectContext];
    NSEntityDescription *entityDesc =
    [NSEntityDescription entityForName:@"Lost"
                inManagedObjectContext:context];
    NSFetchRequest *request = [[NSFetchRequest alloc] init];
    [request setEntity:entityDesc];
    [request setEntity:entityDesc];
    NSPredicate *pred = [NSPredicate predicateWithFormat:@"(processed = %@)", @"YES"];
    [request setPredicate:pred];
    NSError *error;
    NSArray *results = [context executeFetchRequest:request
                                              error:&error];
    for (NSManagedObject *product in results) {
        [context deleteObject:product];
        NSLog(@"deleting....");
    }
    [context save:&error];
}
-(void) sendlostpositions{
    AppDelegate *appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    
    NSManagedObjectContext *context =
    [appDelegate managedObjectContext];
    
    NSEntityDescription *entityDesc =
    [NSEntityDescription entityForName:@"Lost"
                inManagedObjectContext:context];
    
    NSFetchRequest *request = [[NSFetchRequest alloc] init];
    [request setEntity:entityDesc];
    
    [request setEntity:entityDesc];
    
    NSPredicate *pred = [NSPredicate predicateWithFormat:@"(processed = %@)", @"NO"];
    
    [request setPredicate:pred];
    NSError *error;
    
    NSArray *results = [context executeFetchRequest:request
                                              error:&error];
    
    for (NSManagedObject *product in results) {
        NSLog(@"Processed: %@",[product valueForKey:@"processed"]);
        [product setValue:@"YES" forKey:@"processed"];
        NSString *latitude=[product valueForKey:@"latitude"];
        NSString *longitude=[product valueForKey:@"longitude"];
        NSString *speed=[product valueForKey:@"speed"];
        NSString *imei=[product valueForKey:@"imei"];
        NSString *group=[product valueForKey:@"group"];
        NSString *email=[product valueForKey:@"email"];
        NSString *date=[product valueForKey:@"date"];
        [self sendlost2server:latitude longitude:longitude speed:speed imei:imei group:group email:email date:date];
        NSLog(@"processed...");
    }
    
    [context save:&error];
    
}
- (void) sendlost2server: (NSString *) latitude longitude:(NSString *) longitude speed: (NSString *) speed imei: (NSString *) imei group: (NSString *) group email: (NSString *) email date: (NSString *) date
{
    
    NSString *battery = [[NSNumber numberWithDouble:[self batterylevel]] stringValue];
    NSString *femail = [email stringByAddingPercentEncodingWithAllowedCharacters:[NSCharacterSet URLHostAllowedCharacterSet]];
    NSString *fgroup = [group stringByAddingPercentEncodingWithAllowedCharacters:[NSCharacterSet URLHostAllowedCharacterSet]];
    
    NSString *mask=[NSString stringWithFormat:@"%@%@%@", @"http://", KANAN_url, @"/api/noa/lostpositions/?imei=%@&email=%@&group=%@&latitude=%@&longitude=%@&speed=%@&battery=%@&date=%@"];
    NSString *urlString = [NSString stringWithFormat:mask,
                           imei,
                           femail,
                           fgroup,
                           latitude,
                           longitude,
                           speed,
                           battery,
                           date];
    
    NSMutableURLRequest *request = [[NSMutableURLRequest alloc] initWithURL:[NSURL URLWithString:urlString]];
    
    [request setHTTPMethod:@"GET"];
    [request setValue:@"application/json" forHTTPHeaderField:@"Accept"];
    [request setValue:@"application/json" forHTTPHeaderField:@"Content-Type"];
    // Send your request asynchronously
    NSLog(@"%@",urlString);
    NSLog(@"Just before send asyncronous ");
    NSURLSession *session = [NSURLSession sharedSession];
    [[session dataTaskWithURL:[NSURL URLWithString:urlString]
            completionHandler:^(NSData *responseData,
                                NSURLResponse *responseCode,
                                NSError *responseError)
      {
          
          // This will get the NSURLResponse into NSHTTPURLResponse format
          NSHTTPURLResponse* httpResponse = (NSHTTPURLResponse*)responseCode;
          // This will Fetch the status code from NSHTTPURLResponse object
          int responseStatusCode = (int)[httpResponse statusCode];
          //Just to make sure, it works or not
          NSLog(@"Status Code :: %d", responseStatusCode);
          NSDictionary *userInfo = [responseError userInfo];
          NSString *errorString = [[userInfo objectForKey:NSUnderlyingErrorKey] localizedDescription];
          NSLog(@"Error string: %@",errorString);
          if ([responseData length] > 0 && responseError == nil)
          {
              id jsonData = [NSJSONSerialization JSONObjectWithData:responseData options:NSJSONReadingAllowFragments error:&responseError];
              NSDictionary *responseDict = jsonData;
              NSLog(@"Success redirector");
              if ([[responseDict objectForKey:@"Error"] isEqual:@"none"])
              {
                  NSLog(@"Success");
              }
              
          }
      }]
     resume];
}


-(int) batterylevel
{
    int level;
    UIDevice *myDevice = [UIDevice currentDevice];
    [myDevice setBatteryMonitoringEnabled:YES];
    int state = [myDevice batteryState];
    NSLog(@"battery status: %d",state); // 0 unknown, 1 unplegged, 2 charging, 3 full
    double batLeft = (float)[myDevice batteryLevel] * 100;
    NSLog(@"battery left: %f", batLeft);
    level=(int)batLeft;
    return level;
}

- (NSInteger)daysBetweenDate:(NSDate*)fromDateTime andDate:(NSDate*)toDateTime
{
    NSDate *fromDate;
    NSDate *toDate;
    
    NSCalendar *calendar = [NSCalendar currentCalendar];
    
    [calendar rangeOfUnit:NSCalendarUnitDay startDate:&fromDate
                 interval:NULL forDate:fromDateTime];
    [calendar rangeOfUnit:NSCalendarUnitDay startDate:&toDate
                 interval:NULL forDate:toDateTime];
    
    NSDateComponents *difference = [calendar components:NSCalendarUnitDay
                                               fromDate:fromDate toDate:toDate options:0];
    
    return [difference day];
}

#pragma mark - Core Data stack
- (NSString *) retrieveValid
{
    NSString *apnValid;
    AppDelegate *appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    
    NSManagedObjectContext *context =
    [appDelegate managedObjectContext];
    
    NSEntityDescription *entityDesc =
    [NSEntityDescription entityForName:@"Settings"
                inManagedObjectContext:context];
    
    NSFetchRequest *request = [[NSFetchRequest alloc] init];
    [request setEntity:entityDesc];
    
    [request setEntity:entityDesc];
    
    NSPredicate *pred = [NSPredicate predicateWithFormat:@"(id = %@)", @"1"];
    
    [request setPredicate:pred];
    
    NSManagedObject *matches = nil;
    
    NSError *error;
    
    NSArray *objects = [context executeFetchRequest:request
                                              error:&error];
    if ([objects count] == 0) {
        NSLog(@"No password");
        apnValid=@"NO";
    } else {
        matches = objects[0];
        apnValid = [matches valueForKey:@"valid"];
    }
    return apnValid;
}
- (unsigned long) countItems {
    
    AppDelegate *appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    
    NSManagedObjectContext *context =
    [appDelegate managedObjectContext];
    
    NSEntityDescription *entityDesc =
    [NSEntityDescription entityForName:@"Settings"
                inManagedObjectContext:context];
    
    NSFetchRequest *request = [[NSFetchRequest alloc] init];
    [request setEntity:entityDesc];
    
    [request setEntity:entityDesc];
    
    NSPredicate *pred = [NSPredicate predicateWithFormat:@"(id = %@)", @"1"];
    
    [request setPredicate:pred];
    
    NSError *error;
    
    NSArray *objects = [context executeFetchRequest:request
                                              error:&error];
    
    return [objects count];
}

#pragma mark - Core Data stack

@synthesize managedObjectContext = _managedObjectContext;
@synthesize managedObjectModel = _managedObjectModel;
@synthesize persistentStoreCoordinator = _persistentStoreCoordinator;

- (NSURL *)applicationDocumentsDirectory {
    // The directory the application uses to store the Core Data store file. This code uses a directory named "com.madd.Road_KANAN" in the application's documents directory.
    return [[[NSFileManager defaultManager] URLsForDirectory:NSDocumentDirectory inDomains:NSUserDomainMask] lastObject];
}

- (NSManagedObjectModel *)managedObjectModel {
    // The managed object model for the application. It is a fatal error for the application not to be able to find and load its model.
    if (_managedObjectModel != nil) {
        return _managedObjectModel;
    }
    NSURL *modelURL = [[NSBundle mainBundle] URLForResource:@"KANAN_angel" withExtension:@"momd"];
    _managedObjectModel = [[NSManagedObjectModel alloc] initWithContentsOfURL:modelURL];
    return _managedObjectModel;
}

- (NSPersistentStoreCoordinator *)persistentStoreCoordinator {
    // The persistent store coordinator for the application. This implementation creates and returns a coordinator, having added the store for the application to it.
    if (_persistentStoreCoordinator != nil) {
        return _persistentStoreCoordinator;
    }
    
    // Create the coordinator and store
    
    _persistentStoreCoordinator = [[NSPersistentStoreCoordinator alloc] initWithManagedObjectModel:[self managedObjectModel]];
    NSURL *storeURL = [[self applicationDocumentsDirectory] URLByAppendingPathComponent:@"Road_KANAN.sqlite"];
    NSError *error = nil;
    NSString *failureReason = @"There was an error creating or loading the application's saved data.";
    if (![_persistentStoreCoordinator addPersistentStoreWithType:NSSQLiteStoreType configuration:nil URL:storeURL options:nil error:&error]) {
        // Report any error we got.
        NSMutableDictionary *dict = [NSMutableDictionary dictionary];
        dict[NSLocalizedDescriptionKey] = @"Failed to initialize the application's saved data";
        dict[NSLocalizedFailureReasonErrorKey] = failureReason;
        dict[NSUnderlyingErrorKey] = error;
        error = [NSError errorWithDomain:@"YOUR_ERROR_DOMAIN" code:9999 userInfo:dict];
        // Replace this with code to handle the error appropriately.
        // abort() causes the application to generate a crash log and terminate. You should not use this function in a shipping application, although it may be useful during development.
        NSLog(@"Unresolved error %@, %@", error, [error userInfo]);
        abort();
    }
    
    return _persistentStoreCoordinator;
}


- (NSManagedObjectContext *)managedObjectContext {
    // Returns the managed object context for the application (which is already bound to the persistent store coordinator for the application.)
    if (_managedObjectContext != nil) {
        return _managedObjectContext;
    }
    
    NSPersistentStoreCoordinator *coordinator = [self persistentStoreCoordinator];
    if (!coordinator) {
        return nil;
    }
    _managedObjectContext = [[NSManagedObjectContext alloc] initWithConcurrencyType:NSMainQueueConcurrencyType];
    [_managedObjectContext setPersistentStoreCoordinator:coordinator];
    return _managedObjectContext;
}

#pragma mark - Core Data Saving support

- (void)saveContext {
    NSManagedObjectContext *managedObjectContext = self.managedObjectContext;
    if (managedObjectContext != nil) {
        NSError *error = nil;
        if ([managedObjectContext hasChanges] && ![managedObjectContext save:&error]) {
            // Replace this implementation with code to handle the error appropriately.
            // abort() causes the application to generate a crash log and terminate. You should not use this function in a shipping application, although it may be useful during development.
            NSLog(@"Unresolved error %@, %@", error, [error userInfo]);
            abort();
        }
    }
}

@end


