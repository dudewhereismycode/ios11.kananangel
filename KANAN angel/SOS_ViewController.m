//
//  SOS_ViewController.m
//  KANAN angel
//
//  Created by Jorge Macias on 11/3/17.
//  Copyright © 2017 Jorge Macias. All rights reserved.
//

#import "SOS_ViewController.h"
#import "AppDelegate.h"
#import "Reachability.h"
#import "CoreData.h"

@interface SOS_ViewController ()

@property (weak, nonatomic) IBOutlet UILabel *indexLabel;

@property (weak, nonatomic) IBOutlet UISwitch *blepanicswitch;
@property (weak, nonatomic) IBOutlet UIButton *resetButton;
@property (nonatomic, retain) NSString * languageMessage;
@property (nonatomic, retain) NSNumber * last;
@property (weak, nonatomic) IBOutlet UIButton *sosButton;
@property (weak, nonatomic) IBOutlet UIButton *a911Button;
@property (weak, nonatomic) IBOutlet UIButton *medicButton;
@property (weak, nonatomic) IBOutlet UIButton *policeButton;
@property (weak, nonatomic) IBOutlet UIButton *fireButton;
@property (weak, nonatomic) NSString *alarmtype;
@end
int counter;
int buttonCounter;
int pointer;
int switch_state;
int last_stamp,current_stamp;
static int arrayMajor[5] = {0,0,0,0,0};
static int arrayMinor[5] = {0,0,0,0,0};
@implementation SOS_ViewController
{
    CLLocationManager *locationManager;
    NSMutableArray *tableData;
}
@synthesize languageMessage;
@synthesize locationManagerble;
@synthesize ccount;
@synthesize a911Button;
@synthesize medicButton;
@synthesize sosButton;
@synthesize policeButton;
@synthesize fireButton;
- (void)viewDidLoad {
    [super viewDidLoad];
    buttonCounter=0;
    sosButton.clipsToBounds=YES;
    last_stamp=-1;

    // Do any additional setup after loading the view.
}
- (void)viewDidAppear:(BOOL)animated{
    CoreData *orm=[[CoreData alloc]init];
    UIImage *a911img = [UIImage imageNamed:[orm retrieve:@"button1"]];
    [a911Button setImage:a911img  forState:UIControlStateNormal];
    UIImage *medicalimg = [UIImage imageNamed:[orm retrieve:@"button2"]];
    [medicButton setImage:medicalimg  forState:UIControlStateNormal];
    UIImage *sosimg = [UIImage imageNamed:[orm retrieve:@"button3"]];
    [sosButton setImage:sosimg  forState:UIControlStateNormal];
    UIImage *policeimg = [UIImage imageNamed:[orm retrieve:@"button4"]];
    [policeButton setImage:policeimg  forState:UIControlStateNormal];
    UIImage *fireimg = [UIImage imageNamed:[orm retrieve:@"button5"]];
    [fireButton setImage:fireimg  forState:UIControlStateNormal];

    [self.resetButton setTitle: NSLocalizedString(@"Reset",nil) forState:UIControlStateNormal];
    [[self.tabBarController.tabBar.items objectAtIndex:1] setTitle:NSLocalizedString(@"Settings", @"comment")];
    last_stamp=-1;
    
    tableData =  [[NSMutableArray alloc] init];
    // Configure Core Location
    locationManager = [[CLLocationManager alloc] init];
    locationManager = [[CLLocationManager alloc] init];
    locationManager.delegate = self;
    locationManager.distanceFilter = kCLDistanceFilterNone;
    locationManager.desiredAccuracy = kCLLocationAccuracyBest;
    [locationManager requestWhenInUseAuthorization];
    // Do any additional setup after loading the view.
    sendLocationCounter=-1;
    self.indexLabel.text=@"";
    if ([[self isvalid] isEqualToString:@"YES"]) {
        NSLog(@"valid account");
        if ([[self isbleenable] isEqualToString:@"YES"])
        {
            [self.blepanicswitch setOn:YES animated:YES];
            [self blecredentials];
            NSLog(@"Pass");
            locationManagerble = [[CLLocationManager alloc]init];
            if([self.locationManagerble respondsToSelector:@selector(requestAlwaysAuthorization)]) {
                [self.locationManagerble requestAlwaysAuthorization];
            }
            self.locationManagerble.pausesLocationUpdatesAutomatically = NO;
            self.locationManagerble.allowsBackgroundLocationUpdates = YES;
            locationManagerble.delegate = self;
            
            CLBeaconRegion *region = [[CLBeaconRegion alloc] initWithProximityUUID:[[NSUUID alloc] initWithUUIDString:@"FDA50693-A4E2-4FB1-AFCF-C6EB07647828"] major: arrayMajor[0] minor: arrayMinor[0] identifier: @"region1"];
            region.notifyEntryStateOnDisplay = YES;
            
            
            CLBeaconRegion *region2 = [[CLBeaconRegion alloc] initWithProximityUUID:[[NSUUID alloc] initWithUUIDString:@"FDA50693-A4E2-4FB1-AFCF-C6EB07640477"] major: arrayMajor[0] minor: arrayMinor[0] identifier: @"region2"];
            region.notifyEntryStateOnDisplay = YES;
            
            CLBeaconRegion *region3 = [[CLBeaconRegion alloc] initWithProximityUUID:[[NSUUID alloc] initWithUUIDString:@"FDA50693-A4E2-4FB1-AFCF-C6EB07647828"] major: arrayMajor[2] minor: arrayMinor[2] identifier: @"region3"];
            region.notifyEntryStateOnDisplay = YES;
            
            CLBeaconRegion *region4 = [[CLBeaconRegion alloc] initWithProximityUUID:[[NSUUID alloc] initWithUUIDString:@"FDA50693-A4E2-4FB1-AFCF-C6EB07640477"] major: arrayMajor[3] minor: arrayMinor[3] identifier: @"region4"];
            
            CLBeaconRegion *region5 = [[CLBeaconRegion alloc] initWithProximityUUID:[[NSUUID alloc] initWithUUIDString:@"FDA50693-A4E2-4FB1-AFCF-C6EB07647828"] major: arrayMajor[4] minor: arrayMinor[4] identifier: @"region4"];
            
            region.notifyEntryStateOnDisplay = YES;
            
            [self.locationManagerble startMonitoringForRegion:region];
            [self.locationManagerble startRangingBeaconsInRegion:region];
            
            [self.locationManagerble startMonitoringForRegion:region2];
            [self.locationManagerble startRangingBeaconsInRegion:region2];
            
            [self.locationManagerble startMonitoringForRegion:region3];
            [self.locationManagerble startRangingBeaconsInRegion:region3];
            
            [self.locationManagerble startMonitoringForRegion:region4];
            [self.locationManagerble startRangingBeaconsInRegion:region4];
            
            [self.locationManagerble startMonitoringForRegion:region5];
            [self.locationManagerble startRangingBeaconsInRegion:region5];
            
            [self.locationManagerble startUpdatingLocation];
            ccount=0;
        }
        else
        {
            [self.blepanicswitch setOn:NO animated:YES];
            
        }
    }
    else{
        NSLog(@"invalid account");
        languageMessage=NSLocalizedString(@"You have to go to settings and configure an account",nil);
        [self showAlert:@"Alert!" :languageMessage];
    }
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
- (IBAction)sosTapped:(UIButton *)sender {
    sendLocationCounter=0;
    CoreData *orm=[[CoreData alloc]init];
    if([[orm retrieve:@"valid"] isEqualToString:@"YES"] ){
        Reachability *reachability = [Reachability reachabilityForInternetConnection];
        [reachability startNotifier];
        
        NetworkStatus status = [reachability currentReachabilityStatus];
        
        if(status == NotReachable)
        {
            [self showAlert:@"Alert" :@"No internet availiable"];
        }
        else
        {
            buttonCounter++;
            self.indexLabel.text=[NSString stringWithFormat:@"%d",buttonCounter];
            if (buttonCounter==3)
            {
                NSLog(@"911 alarm pressed!!");
                self.alarmtype=@"3";
                [locationManager startUpdatingLocation];
                
                languageMessage=@"An alarm event has been sent to the platform";
                [self showAlert:@"Message" :languageMessage];
                buttonCounter=0;
                self.indexLabel.text=@"";
            }

        }
    }
    else
    {
        languageMessage=@"No valid account, go to settings and configure account";
        [self showAlert:@"Message" :languageMessage];
        buttonCounter=0;
        self.indexLabel.text=@"";
    }
}

- (IBAction)a911ButtonTapped:(UIButton *)sender {
    sendLocationCounter=0;
    CoreData *orm=[[CoreData alloc]init];
    if([[orm retrieve:@"valid"] isEqualToString:@"YES"] ){
        Reachability *reachability = [Reachability reachabilityForInternetConnection];
        [reachability startNotifier];
        
        NetworkStatus status = [reachability currentReachabilityStatus];
        
        if(status == NotReachable)
        {
            [self showAlert:@"Alert" :@"No internet availiable"];
        }
        else
        {
            buttonCounter++;
            self.indexLabel.text=[NSString stringWithFormat:@"%d",buttonCounter];
            if (buttonCounter==3)
            {
                NSLog(@"button 1!!");
                self.alarmtype=@"1";
                [locationManager startUpdatingLocation];
                
                languageMessage=@"An alarm event has been sent to the platform";
                [self showAlert:@"Message" :languageMessage];
                buttonCounter=0;
                self.indexLabel.text=@"";
            }
            
        }
    }
    else
    {
        languageMessage=@"No valid account, go to settings and configure account";
        [self showAlert:@"Message" :languageMessage];
        buttonCounter=0;
        self.indexLabel.text=@"";
    }
}
- (IBAction)medicalButtonTapped:(UIButton *)sender {
    sendLocationCounter=0;
    CoreData *orm=[[CoreData alloc]init];
    if([[orm retrieve:@"valid"] isEqualToString:@"YES"] ){
        Reachability *reachability = [Reachability reachabilityForInternetConnection];
        [reachability startNotifier];
        
        NetworkStatus status = [reachability currentReachabilityStatus];
        
        if(status == NotReachable)
        {
            [self showAlert:@"Alert" :@"No internet availiable"];
        }
        else
        {
            buttonCounter++;
            self.indexLabel.text=[NSString stringWithFormat:@"%d",buttonCounter];
            if (buttonCounter==3)
            {
                NSLog(@"Button 2!!");
                self.alarmtype=@"2";
                [locationManager startUpdatingLocation];
                
                languageMessage=@"An alarm event has been sent to the platform";
                [self showAlert:@"Message" :languageMessage];
                buttonCounter=0;
                self.indexLabel.text=@"";
            }
            
        }
    }
    else
    {
        languageMessage=@"No valid account, go to settings and configure account";
        [self showAlert:@"Message" :languageMessage];
        buttonCounter=0;
        self.indexLabel.text=@"";
    }
}
- (IBAction)policeButtonTapped:(UIButton *)sender {
    sendLocationCounter=0;
    CoreData *orm=[[CoreData alloc]init];
    if([[orm retrieve:@"valid"] isEqualToString:@"YES"] ){
        Reachability *reachability = [Reachability reachabilityForInternetConnection];
        [reachability startNotifier];
        
        NetworkStatus status = [reachability currentReachabilityStatus];
        
        if(status == NotReachable)
        {
            [self showAlert:@"Alert" :@"No internet availiable"];
        }
        else
        {
            buttonCounter++;
            self.indexLabel.text=[NSString stringWithFormat:@"%d",buttonCounter];
            if (buttonCounter==3)
            {
                NSLog(@"button 4d!!");
                self.alarmtype=@"4";
                [locationManager startUpdatingLocation];
                
                languageMessage=@"An alarm event has been sent to the platform";
                [self showAlert:@"Message" :languageMessage];
                buttonCounter=0;
                self.indexLabel.text=@"";
            }
            
        }
    }
    else
    {
        languageMessage=@"No valid account, go to settings and configure account";
        [self showAlert:@"Message" :languageMessage];
        buttonCounter=0;
        self.indexLabel.text=@"";
    }
}
- (IBAction)fireButtonTapped:(UIButton *)sender {
    sendLocationCounter=0;
    CoreData *orm=[[CoreData alloc]init];
    if([[orm retrieve:@"valid"] isEqualToString:@"YES"] ){
        Reachability *reachability = [Reachability reachabilityForInternetConnection];
        [reachability startNotifier];
        
        NetworkStatus status = [reachability currentReachabilityStatus];
        
        if(status == NotReachable)
        {
            [self showAlert:@"Alert" :@"No internet availiable"];
        }
        else
        {
            buttonCounter++;
            self.indexLabel.text=[NSString stringWithFormat:@"%d",buttonCounter];
            if (buttonCounter==3)
            {
                NSLog(@"button 5!!");
                self.alarmtype=@"5";
                [locationManager startUpdatingLocation];
                
                languageMessage=@"An alarm event has been sent to the platform";
                [self showAlert:@"Message" :languageMessage];
                buttonCounter=0;
                self.indexLabel.text=@"";
            }
            
        }
    }
    else
    {
        languageMessage=@"No valid account, go to settings and configure account";
        [self showAlert:@"Message" :languageMessage];
        buttonCounter=0;
        self.indexLabel.text=@"";
    }
}

- (IBAction)resetButtonTapped:(UIButton *)sender {
    counter=0;
    self.indexLabel.text=@"";
    
}
- (IBAction)bleSwitchButtonTapped:(UISwitch *)sender {
     UISwitch *switchObject = (UISwitch *)sender;
    if ([[self isvalid] isEqualToString:@"YES"]) {
        NSLog(@"valid account");
        if(switchObject.isOn){
            NSLog(@"Switch enable");
            
            [self blepanicstate:@"YES"];
            [self blecredentials];
            
            locationManagerble = [[CLLocationManager alloc]init];
            if([self.locationManagerble respondsToSelector:@selector(requestAlwaysAuthorization)]) {
                [self.locationManagerble requestAlwaysAuthorization];
            }
            
            
            counter=0;
            for (NSString *yourVar in tableData) {
                NSArray *array = [yourVar componentsSeparatedByString:@"-"];
                arrayMajor[counter]=[array[0] intValue];;
                arrayMinor[counter]=[array[1] intValue];;
                NSLog (@"Your Array elements are = %@", yourVar);
                counter++;
                if (counter>4)
                    break;
            }
            
            self.locationManagerble.pausesLocationUpdatesAutomatically = NO;
            self.locationManagerble.allowsBackgroundLocationUpdates = YES;
            locationManagerble.delegate = self;
            
            CLBeaconRegion *region = [[CLBeaconRegion alloc] initWithProximityUUID:[[NSUUID alloc] initWithUUIDString:@"FDA50693-A4E2-4FB1-AFCF-C6EB07647828"] major: arrayMajor[0] minor: arrayMinor[0] identifier: @"region1"];
            region.notifyEntryStateOnDisplay = YES;
            
            CLBeaconRegion *region2 = [[CLBeaconRegion alloc] initWithProximityUUID:[[NSUUID alloc] initWithUUIDString:@"FDA50693-A4E2-4FB1-AFCF-C6EB07640477"] major: arrayMajor[0] minor: arrayMinor[0] identifier: @"region2"];
            region.notifyEntryStateOnDisplay = YES;
            
            CLBeaconRegion *region3 = [[CLBeaconRegion alloc] initWithProximityUUID:[[NSUUID alloc] initWithUUIDString:@"FDA50693-A4E2-4FB1-AFCF-C6EB07647828"] major: arrayMajor[2] minor: arrayMinor[2] identifier: @"region3"];
            region.notifyEntryStateOnDisplay = YES;
            
            CLBeaconRegion *region4 = [[CLBeaconRegion alloc] initWithProximityUUID:[[NSUUID alloc] initWithUUIDString:@"FDA50693-A4E2-4FB1-AFCF-C6EB07640477"] major: arrayMajor[3] minor: arrayMinor[3] identifier: @"region4"];
            
            CLBeaconRegion *region5 = [[CLBeaconRegion alloc] initWithProximityUUID:[[NSUUID alloc] initWithUUIDString:@"FDA50693-A4E2-4FB1-AFCF-C6EB07647828"] major: arrayMajor[4] minor: arrayMinor[4] identifier: @"region4"];
            
            
            [self.locationManagerble startMonitoringForRegion:region];
            [self.locationManagerble startRangingBeaconsInRegion:region];
            
            [self.locationManagerble startMonitoringForRegion:region2];
            [self.locationManagerble startRangingBeaconsInRegion:region2];
            
            [self.locationManagerble startMonitoringForRegion:region3];
            [self.locationManagerble startRangingBeaconsInRegion:region3];
            
            [self.locationManagerble startMonitoringForRegion:region4];
            [self.locationManagerble startRangingBeaconsInRegion:region4];
            
            [self.locationManagerble startMonitoringForRegion:region5];
            [self.locationManagerble startRangingBeaconsInRegion:region5];
            
            [self.locationManagerble startUpdatingLocation];
            
            
            ccount=0;
            languageMessage=NSLocalizedString(@"Using bluetooth panic buttons can decrease the battery very quickly. Use this feature only when you are in transit crossing crime zones",nil);
            [self showAlert:NSLocalizedString(@"Warning",nil) :languageMessage];
            
        }else{
            NSLog(@"Switch disable");
            ccount=0;
            [self blepanicstate:@"NO"];
            [self blecredentials];
            
            CLBeaconRegion *region = [[CLBeaconRegion alloc] initWithProximityUUID:[[NSUUID alloc] initWithUUIDString:@"FDA50693-A4E2-4FB1-AFCF-C6EB07647828"] major: arrayMajor[0] minor: arrayMinor[0] identifier: @"region1"];
            
            CLBeaconRegion *region2 = [[CLBeaconRegion alloc] initWithProximityUUID:[[NSUUID alloc] initWithUUIDString:@"FDA50693-A4E2-4FB1-AFCF-C6EB07640477"] major: arrayMajor[0] minor: arrayMinor[0] identifier: @"region2"];
            
            CLBeaconRegion *region3 = [[CLBeaconRegion alloc] initWithProximityUUID:[[NSUUID alloc] initWithUUIDString:@"FDA50693-A4E2-4FB1-AFCF-C6EB07647828"] major: arrayMajor[2] minor: arrayMinor[2] identifier: @"region3"];
            
            CLBeaconRegion *region4 = [[CLBeaconRegion alloc] initWithProximityUUID:[[NSUUID alloc] initWithUUIDString:@"FDA50693-A4E2-4FB1-AFCF-C6EB07640477"] major: arrayMajor[3] minor: arrayMinor[3] identifier: @"region4"];
            
            CLBeaconRegion *region5 = [[CLBeaconRegion alloc] initWithProximityUUID:[[NSUUID alloc] initWithUUIDString:@"FDA50693-A4E2-4FB1-AFCF-C6EB07647828"] major: arrayMajor[4] minor: arrayMinor[4] identifier: @"region5"];
            
            
            [self.locationManagerble stopMonitoringForRegion:region];
            [self.locationManagerble stopRangingBeaconsInRegion: region];
            
            [self.locationManagerble stopMonitoringForRegion:region2];
            [self.locationManagerble stopRangingBeaconsInRegion: region2];
            
            [self.locationManagerble stopMonitoringForRegion:region3];
            [self.locationManagerble stopRangingBeaconsInRegion: region3];
            
            [self.locationManagerble stopMonitoringForRegion:region4];
            [self.locationManagerble stopRangingBeaconsInRegion: region4];
            
            [self.locationManagerble stopMonitoringForRegion:region5];
            [self.locationManagerble stopRangingBeaconsInRegion: region5];
            
            
            [self.locationManagerble stopUpdatingLocation];
            
        }
    }
    else{
        languageMessage=NSLocalizedString(@"You have to go to settings and configure an account",nil);
        [self.blepanicswitch setOn:NO animated:YES];
        [self showAlert:@"Alert!" :languageMessage];
    }
}

//Location Manager
/*
- (void)locationManager:(CLLocationManager *)manager didUpdateToLocation:(CLLocation *)newLocation fromLocation:(CLLocation *)oldLocation {
*/
- (void)locationManager:(CLLocationManager *)manager
     didUpdateLocations:(NSArray *)locations{
    CLLocation *newLocation = [locations lastObject];
    if (sendLocationCounter==0)
    {
        sendLocationCounter++;
        //NSLog(@"OldLocation %f %f", oldLocation.coordinate.latitude, oldLocation.coordinate.longitude);
        NSLog(@"Location %f %f", newLocation.coordinate.latitude, newLocation.coordinate.longitude);
        // Get data
        
        AppDelegate *appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
        
        NSManagedObjectContext *context =
        [appDelegate managedObjectContext];
        
        NSEntityDescription *entityDesc =
        [NSEntityDescription entityForName:@"Settings"
                    inManagedObjectContext:context];
        
        NSFetchRequest *request = [[NSFetchRequest alloc] init];
        [request setEntity:entityDesc];
        
        [request setEntity:entityDesc];
        
        NSPredicate *pred = [NSPredicate predicateWithFormat:@"(id = %@)", @"1"];
        
        [request setPredicate:pred];
        
        NSManagedObject *matches = nil;
        
        NSError *error;
        
        NSArray *objects = [context executeFetchRequest:request
                                                  error:&error];
        if ([objects count] == 0) {
            NSLog(@"No Data");
        }
        else
        {
            NSString *latitude = [[NSNumber numberWithDouble:newLocation.coordinate.latitude] stringValue];
            NSString *longitude = [[NSNumber numberWithDouble:newLocation.coordinate.longitude] stringValue];
            double  speed=newLocation.speed;
            NSLog(@"latitude: %@",latitude);
            NSLog(@"longitude: %@",longitude);
            matches = objects[0];
            NSString *phone_number = [matches valueForKey:@"phone_number"];
            NSString *email = [matches valueForKey:@"email"];
            NSString *group = [matches valueForKey:@"group"];
            NSString *valid = [matches valueForKey:@"valid"];
            if ([valid isEqualToString:@"YES"]) {
                NSLog(@"Valid");
                int calc,bl,sp;
                bl=[self batterylevel];
                calc=(bl*1024)/6;
                NSString *myspeed;
                sp=(int) speed;
                if (sp<0)
                    sp=0;
                myspeed=[NSString stringWithFormat:@"%d", sp];
                [self sendpanic2server:latitude longitude:longitude speed:myspeed imei:phone_number group:group email:email];
            }
            else{
                NSLog(@"Not valid");
                
            }
            //notificationInProgress=0;
        }
        [locationManager stopUpdatingLocation];
    }
    
}
// BLE Routines
-(void)locationManager:(CLLocationManager *)manager didEnterRegion:(CLRegion *)region
{
    /*
     ccount++;
     NSLog(@"Count enter:%d",ccount);
     if (ccount>=1){
     //NSLog(@"*****didEnterRegion******");
     NSLog(@"Did enter Region: %@",region.identifier);
     
     [self activateAlarm];
     }
     */
}

-(void)locationManager:(CLLocationManager *)manager didExitRegion:(CLRegion *)region
{
    //NSLog(@"***********didExitRegion***********");
    NSLog(@"Did Exit Region: %@",region.identifier);
    
}

-(void)locationManager:(CLLocationManager *)manager didStartMonitoringForRegion:(CLRegion *)region
{
    NSLog(@"didStartMonitoringForRegion");
    
}

-(void)locationManager:(CLLocationManager *)manager didFailWithError:(NSError *)error
{
    NSLog(@"didFailWithError");
}

-(void)locationManager:(CLLocationManager *)manager didRangeBeacons:(NSArray *)beacons inRegion:(CLBeaconRegion *)region
{
    if ([beacons count] > 0) {
        NSTimeInterval timeStamp = [[NSDate date] timeIntervalSince1970];
        // NSTimeInterval is defined as double
        NSNumber *timeStampObj = [NSNumber numberWithDouble: timeStamp];
        current_stamp=[timeStampObj intValue];
        NSLog(@"Time Stamp: %d",current_stamp);
        if (last_stamp<current_stamp){
            // Add 10 seconds to the next alarm
            last_stamp = [timeStampObj intValue]+10;
            NSLog(@"Last Stamp: %d",last_stamp);
            [self activateAlarm];
        }
        //UILocalNotification* localNotification = [[UILocalNotification alloc] init];
        //localNotification.fireDate = [NSDate dateWithTimeIntervalSinceNow:0];
        //localNotification.alertBody = @"Boton de panico";
        //localNotification.timeZone = [NSTimeZone defaultTimeZone];
        //localNotification.soundName = @"synth.mp3";
        //[[UIApplication sharedApplication] scheduleLocalNotification:localNotification];
        /*
         ccount++;
         NSLog(@"Count:%d",ccount);
         for(pointer = 0; pointer < [beacons count]; pointer++)
         {
         CLBeacon *beacon = [beacons objectAtIndex:pointer];
         NSString *mask=[NSString stringWithFormat:@"%@", @"Beacons: %lu UUID: %@ Major: %@ Minor: %@"];
         NSString *urlString = [NSString stringWithFormat:mask,(unsigned long)[beacons count],         beacon.proximityUUID.UUIDString, beacon.major, beacon.minor];
         NSLog(@"%@",urlString);
         }
         
         if (ccount==1){
         UILocalNotification* localNotification = [[UILocalNotification alloc] init];
         localNotification.fireDate = [NSDate dateWithTimeIntervalSinceNow:0];
         localNotification.alertBody = @"Panic Button";
         localNotification.timeZone = [NSTimeZone defaultTimeZone];
         localNotification.soundName = @"synth.mp3";
         [[UIApplication sharedApplication] scheduleLocalNotification:localNotification];
         [self activateAlarm];
         }
         */
        
    }
}

-(void) activateAlarm{
     self.alarmtype=@"3";
    sendLocationCounter=0;
    Reachability *reachability = [Reachability reachabilityForInternetConnection];
    [reachability startNotifier];
    NetworkStatus status = [reachability currentReachabilityStatus];
    if(status != NotReachable)
        [locationManager startUpdatingLocation];
    
}

// End BLE
-(NSMutableArray*) retrieveArray {
    NSUserDefaults *userDefaults = [NSUserDefaults standardUserDefaults];
    NSMutableArray *beaconlist = [userDefaults objectForKey:@"beaconlist"];
    NSMutableArray *new = [[NSMutableArray alloc] init];
    for (int i=0;i<beaconlist.count;i++){
        [new addObject:beaconlist[i]];
    }
    NSLog(@"Retrieve Len of array: %lu",(sizeof beaconlist) / (sizeof beaconlist[0]));
    return new;
}
// Data Model Bluetooth Routines
- (void) blecredentials{
    NSString *myret;
    AppDelegate *appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    
    NSManagedObjectContext *context =
    [appDelegate managedObjectContext];
    
    NSEntityDescription *entityDesc =
    [NSEntityDescription entityForName:@"Settings"
                inManagedObjectContext:context];
    
    NSFetchRequest *request = [[NSFetchRequest alloc] init];
    [request setEntity:entityDesc];
    
    [request setEntity:entityDesc];
    
    NSPredicate *pred = [NSPredicate predicateWithFormat:@"(id = %@)", @"1"];
    
    [request setPredicate:pred];
    
    NSManagedObject *matches = nil;
    
    NSError *error;
    
    NSArray *objects = [context executeFetchRequest:request
                                              error:&error];
    
    if ([objects count] == 0) {
        NSLog(@"No Data");
        
    }
    else
    {
        matches = objects[0];
        myret=[matches valueForKey:@"valid"];
        
        //NSArray *entries = [NSKeyedUnarchiver unarchiveObjectWithData:[matches valueForKey:@"beaconlist"]];
        //tableData= [NSMutableArray arrayWithArray:entries];
        tableData =  [self retrieveArray];
        for (int i = 0; i <= 4; i++)
        {
            arrayMajor[i]=0;
            arrayMinor[i]=0;
        }
        counter=0;
        for (NSString *yourVar in tableData) {
            NSArray *array = [yourVar componentsSeparatedByString:@"-"];
            arrayMajor[counter]=[array[0] intValue];;
            arrayMinor[counter]=[array[1] intValue];;
            NSLog (@"Your Array elements are = %@", yourVar);
            counter++;
            if (counter>4)
                break;
        }
    }
}

- (NSString *) isbleenable{
    NSString *myret;
    AppDelegate *appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    
    NSManagedObjectContext *context =
    [appDelegate managedObjectContext];
    
    NSEntityDescription *entityDesc =
    [NSEntityDescription entityForName:@"Settings"
                inManagedObjectContext:context];
    
    NSFetchRequest *request = [[NSFetchRequest alloc] init];
    [request setEntity:entityDesc];
    
    [request setEntity:entityDesc];
    
    NSPredicate *pred = [NSPredicate predicateWithFormat:@"(id = %@)", @"1"];
    
    [request setPredicate:pred];
    
    NSManagedObject *matches = nil;
    
    NSError *error;
    
    NSArray *objects = [context executeFetchRequest:request
                                              error:&error];
    if ([objects count] == 0) {
        NSLog(@"No Data");
        myret=@"NO";
    }
    else
    {
        matches = objects[0];
        myret=[matches valueForKey:@"blepanicstate"];
        
    }
    return myret;
}
- (void) blepanicstate: (NSString *) blepanicstate
{
    unsigned long items;
    items=[self countItems];
    if (items==0)
    {
        AppDelegate *appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
        NSManagedObjectContext *context =
        [appDelegate managedObjectContext];
        NSManagedObject *settings;
        settings = [NSEntityDescription
                    insertNewObjectForEntityForName:@"Settings"
                    inManagedObjectContext:context];
        [settings setValue:@"1" forKey:@"id"];
        [settings setValue:blepanicstate forKey:@"blepanicstate"];
        NSError *error;
        [context save:&error];
        
    }
    else
    {
        AppDelegate *appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
        
        NSManagedObjectContext *context =
        [appDelegate managedObjectContext];
        
        NSEntityDescription *entityDesc =
        [NSEntityDescription entityForName:@"Settings"
                    inManagedObjectContext:context];
        
        NSFetchRequest *request = [[NSFetchRequest alloc] init];
        [request setEntity:entityDesc];
        
        [request setEntity:entityDesc];
        
        NSPredicate *pred = [NSPredicate predicateWithFormat:@"(id = %@)", @"1"];
        
        [request setPredicate:pred];
        NSError *error;
        
        NSArray *results = [context executeFetchRequest:request
                                                  error:&error];
        
        NSManagedObject* favoritsGrabbed = [results objectAtIndex:0];
        [favoritsGrabbed setValue:blepanicstate forKey:@"blepanicstate"];
        [context save:&error];
        NSLog(@"updated core data %@",blepanicstate);
    }
    
}
// General Routines
- (unsigned long) countItems {
    
    AppDelegate *appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    
    NSManagedObjectContext *context =
    [appDelegate managedObjectContext];
    
    NSEntityDescription *entityDesc =
    [NSEntityDescription entityForName:@"Settings"
                inManagedObjectContext:context];
    
    NSFetchRequest *request = [[NSFetchRequest alloc] init];
    [request setEntity:entityDesc];
    
    [request setEntity:entityDesc];
    
    NSPredicate *pred = [NSPredicate predicateWithFormat:@"(id = %@)", @"1"];
    
    [request setPredicate:pred];
    
    NSError *error;
    
    NSArray *objects = [context executeFetchRequest:request
                                              error:&error];
    
    return [objects count];
}

- (NSString *) isvalid{
    NSString *myret;
    AppDelegate *appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    
    NSManagedObjectContext *context =
    [appDelegate managedObjectContext];
    
    NSEntityDescription *entityDesc =
    [NSEntityDescription entityForName:@"Settings"
                inManagedObjectContext:context];
    
    NSFetchRequest *request = [[NSFetchRequest alloc] init];
    [request setEntity:entityDesc];
    
    [request setEntity:entityDesc];
    
    NSPredicate *pred = [NSPredicate predicateWithFormat:@"(id = %@)", @"1"];
    
    [request setPredicate:pred];
    
    NSManagedObject *matches = nil;
    
    NSError *error;
    
    NSArray *objects = [context executeFetchRequest:request
                                              error:&error];
    if ([objects count] == 0) {
        NSLog(@"No Data");
        myret=@"NO";
    }
    else
    {
        matches = objects[0];
        myret=[matches valueForKey:@"valid"];
    }
    
    //
    
    return myret;
}

-(int) batterylevel
{
    int level;
    UIDevice *myDevice = [UIDevice currentDevice];
    [myDevice setBatteryMonitoringEnabled:YES];
    int state = [myDevice batteryState];
    NSLog(@"battery status: %d",state); // 0 unknown, 1 unplegged, 2 charging, 3 full
    double batLeft = (float)[myDevice batteryLevel] * 100;
    NSLog(@"battery left: %f", batLeft);
    level=(int)batLeft;
    return level;
}

- (void) sendpanic2server: (NSString *) latitude longitude:(NSString *) longitude speed: (NSString *) speed imei: (NSString *) imei group: (NSString *) group email: (NSString *) email
{
    
    NSString *battery = [[NSNumber numberWithDouble:[self batterylevel]] stringValue];
    NSString *femail = [email stringByAddingPercentEncodingWithAllowedCharacters:[NSCharacterSet URLHostAllowedCharacterSet]];
    NSString *fgroup = [group stringByAddingPercentEncodingWithAllowedCharacters:[NSCharacterSet URLHostAllowedCharacterSet]];
    
    NSString *mask=[NSString stringWithFormat:@"%@%@%@", @"http://", KANAN_url, @"/api/noa/panic/?imei=%@&email=%@&group=%@&latitude=%@&longitude=%@&speed=%@&battery=%@&type=%@"];
    NSString *urlString = [NSString stringWithFormat:mask,
                           imei,
                           femail,
                           fgroup,
                           latitude,
                           longitude,
                           speed,
                           battery,self.alarmtype];
    
    NSMutableURLRequest *request = [[NSMutableURLRequest alloc] initWithURL:[NSURL URLWithString:urlString]];
    
    [request setHTTPMethod:@"GET"];
    [request setValue:@"application/json" forHTTPHeaderField:@"Accept"];
    [request setValue:@"application/json" forHTTPHeaderField:@"Content-Type"];
    // Send your request asynchronously
    NSLog(@"%@",urlString);
    NSLog(@"Just before send asyncronous ");
    NSUserDefaults *preferences = [NSUserDefaults standardUserDefaults];
    
    NSString *currentLevelKey = @"currentlevel";
    
    if ([preferences objectForKey:currentLevelKey] == nil)
    {
        //  Doesn't exist.
        switch_state=0;
        NSUserDefaults *preferences = [NSUserDefaults standardUserDefaults];
        
        NSString *currentLevelKey = @"currentlevel";
        [preferences setInteger:switch_state forKey:currentLevelKey];
        //  Save to disk
        const BOOL didSave = [preferences synchronize];
        
        if (!didSave)
        {
            //  Couldn't save (I've never seen this happen in real world testing)
        }
    }
    else
    {
        switch_state = (int)[preferences integerForKey:currentLevelKey];
        if (switch_state==1){
            NSDictionary* userInfo= @{@"title": NSLocalizedString(@"Alert",nil),@"body":NSLocalizedString(@"Panic button pressed",nil)};
            [[NSNotificationCenter defaultCenter] postNotificationName:@"localNotification" object:self userInfo:userInfo];
        }
    }
    NSURLSession *session = [NSURLSession sharedSession];
    [[session dataTaskWithURL:[NSURL URLWithString:urlString]
            completionHandler:^(NSData *responseData,
                                NSURLResponse *responseCode,
                                NSError *responseError)
      {
          // This will get the NSURLResponse into NSHTTPURLResponse format
          NSHTTPURLResponse* httpResponse = (NSHTTPURLResponse*)responseCode;
          // This will Fetch the status code from NSHTTPURLResponse object
          int responseStatusCode = (int)[httpResponse statusCode];
          //Just to make sure, it works or not
          NSLog(@"Status Code :: %d", responseStatusCode);
          NSDictionary *userInfo = [responseError userInfo];
          NSString *errorString = [[userInfo objectForKey:NSUnderlyingErrorKey] localizedDescription];
          NSLog(@"Error string: %@",errorString);
          if ([responseData length] > 0 && responseError == nil)
          {
              id jsonData = [NSJSONSerialization JSONObjectWithData:responseData options:NSJSONReadingAllowFragments error:&responseError];
              NSDictionary *responseDict = jsonData;
              NSString *kananError = @"";
              NSString *kananMessage = @"";
              NSString *title=@"Error";
              NSLog(@"Success panic");
              if ([[responseDict objectForKey:@"Error"] isEqual:@"none"])
              {
                  NSLog(@"Success");
                  kananError=[responseDict objectForKey:@"Error"];
                  NSLog(@"Error: %@",kananError);
              }
              else if ([[responseDict objectForKey:@"Error"] isEqual:@"500"])
              {
                  [self showAlert:title : @"Can't assign account due lack of licenses"];
              }
              else if ([[responseDict objectForKey:@"Error"] isEqual:@"400"])
              {
                  [self showAlert:title : @"The account does not exist"];
              }
              else if ([[responseDict objectForKey:@"Error"] isEqual:@"401"])
              {
                  [self showAlert:title :@"The account and group does not exist"];
              }
              
              else
              {
                  [self showAlert:title : kananMessage];
              }
              
          }
          
          else if ([responseData length] == 0 && responseError == nil)
          {
              [self senderror:@"Data Error" message:@"Please check credentials or connection"];
              NSLog(@"data error: %@", responseError);
          }
          else if (responseError != nil && responseError.code == NSURLErrorTimedOut)
          {
              [self senderror:@"Data Time Out" message:@"Please check internet is available"];
              NSLog(@"data timeout: %ld", (long)NSURLErrorTimedOut);
          }
          else if (responseError != nil)
          {
              [self senderror:@"Access is denied" message:@"Please check credentials or connection"];
              NSLog(@"data download error: %@",responseError);
          }
      }]
     resume];
    
}
- (void) senderror: (NSString *) error message: (NSString *) message
{
    [self showAlert:error :message];
}
-(void)showAlert:(NSString *)title :(NSString *)body {
    UIAlertController * alert=   [UIAlertController
                                  alertControllerWithTitle:title
                                  message:body
                                  preferredStyle:UIAlertControllerStyleAlert];
    
    UIAlertAction* ok = [UIAlertAction
                         actionWithTitle:@"OK"
                         style:UIAlertActionStyleDefault
                         handler:^(UIAlertAction * action)
                         {
                             [alert dismissViewControllerAnimated:YES completion:nil];
                             
                         }];
    
    [alert addAction:ok];
    [self presentViewController:alert animated:YES completion:nil];
}
@end
