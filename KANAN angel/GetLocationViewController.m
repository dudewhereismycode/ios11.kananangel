//
//  GetLocationViewController.m
//  KANAN angel
//
//  Created by Jorge Macias on 11/1/17.
//  Copyright © 2017 Jorge Macias. All rights reserved.
//

#import "GetLocationViewController.h"
#import "LoginViewController.h"

#import "AppDelegate.h"
#import "CoreData.h"

@interface GetLocationViewController ()
@property (weak, nonatomic) IBOutlet UILabel *coordinates;
@property (weak, nonatomic) IBOutlet UITextView *contract;

@property (weak, nonatomic) IBOutlet UIButton *agreeButton;


@property (strong, nonatomic) NSTimer *timer;

@end
int seconds2;
int remainingCounts2;

@implementation GetLocationViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    self.contract.text=NSLocalizedString(@"This Nondisclosure Agreement the Agreement is entered into by and between MADD Systems with its principal offices at Mariano Abasolo located at Tlalnepantla for the purpose of preventing the unauthorized disclosure of Confidential Information as defined below. The parties agree to enter into a confidential relationship with respect to the disclosure of certain proprietary and confidential information Confidential Information. 1. Definition of Confidential Information. For purposes of this Agreement, Confidential Information shall include all information or material that has or could have commercial value or other utility in the business in which Disclosing Party is engaged. If Confidential Information is in written form, the Disclosing Party shall label or stamp the materials with the word Confidential or some similar warning. If Confidential Information is transmitted orally, the Disclosing Party shall promptly provide a writing indicating that such oral communication constituted Confidential Information. 2. Exclusions from Confidential Information. Receiving Party's obligations under this Agreement do not extend to information that is: a publicly known at the time of disclosure or subsequently becomes publicly known through no fault of the Receiving Party; b discovered or created by the Receiving Party before disclosure by Disclosing Party; c learned by the Receiving Party through legitimate means other than from the Disclosing Party or Disclosing Party's representatives; or d is disclosed by Receiving Party with Disclosing Party's prior written approval. 3. Obligations of Receiving Party. Receiving Party shall hold and maintain the Confidential Information in strictest confidence for the sole and exclusive benefit of the Disclosing Party. Receiving Party shall carefully restrict access to Confidential Information to employees, contractors, and third parties as is reasonably required and shall require those persons to sign nondisclosure restrictions at least as protective as those in this Agreement. Receiving Party shall not, without prior written approval of Disclosing Party, use for Receiving Party's own benefit, publish, copy, or otherwise disclose to others, or permit the use by others for their benefit or to the detriment of Disclosing Party, any Confidential Information. Receiving Party shall return to Disclosing Party any and all records, notes, and other written, printed, or tangible materials in its possession pertaining to Confidential Information immediately if Disclosing Party requests it in writing. 4. Time Periods. The nondisclosure provisions of this Agreement shall survive the termination of this Agreement and Receiving Party's duty to hold Confidential Information in confidence shall remain in effect until the Confidential Information no longer qualifies as a trade secret or until Disclosing Party sends Receiving Party written notice releasing Receiving Party from this Agreement, whichever occurs first. 5. Relationships. Nothing contained in this Agreement shall be deemed to constitute either party a partner, joint venturer or employee of the other party for any purpose. 6. Severability. If a court finds any provision of this Agreement invalid or unenforceable, the remainder of this Agreement shall be interpreted so as best to effect the intent of the parties. 7. Integration. This Agreement expresses the complete understanding of the parties with respect to the subject matter and supersedes all prior proposals, agreements, representations, and understandings. This Agreement may not be amended except in a writing signed by both parties. 8. Waiver. The failure to exercise any right provided in this Agreement shall not be a waiver of prior or subsequent rights. This Agreement and each party's obligations shall be binding on the representatives, assigns, and successors of such party. Each party has signed this Agreement through its authorized representative.",nil);
     // Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void) viewDidAppear:(BOOL)animated{
    
}
- (IBAction)startButtonTapped:(UIButton *)sender {

    
    if ([CLLocationManager locationServicesEnabled]){
        
        NSLog(@"Location Services Enabled");
        
        if ([CLLocationManager authorizationStatus]==kCLAuthorizationStatusDenied){
            [self showAlert:@"App Permission Denied" :@"To re-enable, please go to Settings and turn on Location Service for this app."];

        }
        else{
            [self showLoginScreen2];
            self.agreeButton.enabled=NO;
        }
    }
   

}
/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/
-(void) saveData2:(NSString*)country :(NSString*)latitude :(NSString*)longitude{
    AppDelegate *appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    
    NSManagedObjectContext *context =
    [appDelegate managedObjectContext];
    
    NSEntityDescription *entityDesc =
    [NSEntityDescription entityForName:@"Settings"
                inManagedObjectContext:context];
    
    NSFetchRequest *request = [[NSFetchRequest alloc] init];
    [request setEntity:entityDesc];
    
    [request setEntity:entityDesc];
    
    NSPredicate *pred = [NSPredicate predicateWithFormat:@"(id = %@)", @"1"];
    
    [request setPredicate:pred];
    NSError *error;
    
    NSArray *results = [context executeFetchRequest:request
                                              error:&error];
    
    NSManagedObject* favoritsGrabbed = [results objectAtIndex:0];
    [favoritsGrabbed setValue:country forKey:@"country"];
    [context save:&error];
    NSLog(@"updated core data");
}
-(void) showLoginScreen2
{
   [self saveData2:@"MX" :@"19.5":@"-99.5"];
    CLAuthorizationStatus authorizationStatus= [CLLocationManager authorizationStatus];
    if (authorizationStatus == kCLAuthorizationStatusAuthorizedAlways ||
        authorizationStatus == kCLAuthorizationStatusAuthorizedWhenInUse) {
        //stop GPS after 5 seconds and then send string to kanan server
        
        CLLocationManager * locationManager = [[CLLocationManager alloc] init];
        CLLocation * lastKnownLocation = [locationManager location];
        CLLocationCoordinate2D coordinate = [lastKnownLocation coordinate];
        
        
        NSString *latitude = [NSString stringWithFormat:@"%.06f", coordinate.latitude];
        NSString *longitude = [NSString stringWithFormat:@"%.06f", coordinate.longitude];
        NSString *newString=[latitude stringByAppendingString:@","];
        NSString *lastString= [newString stringByAppendingString:longitude];
        self.coordinates.text=lastString;
        CoreData *orm=[[CoreData alloc] init];
        [orm save:@"ilatitude" :latitude];
        [orm save:@"ilongitude" :longitude];
        
        
        NSLog(@"Latitude : %@",latitude);
        NSLog(@"Longitude: %@",longitude);
        
        CLGeocoder *ceo = [[CLGeocoder alloc]init];
        CLLocation *loc = [[CLLocation alloc]initWithLatitude:coordinate.latitude longitude:coordinate.longitude]; //insert your coordinates
        
        [ceo reverseGeocodeLocation:loc
                  completionHandler:^(NSArray *placemarks, NSError *error) {
                      CLPlacemark *placemark = [placemarks objectAtIndex:0];
                      if (placemark) {
                          
                          
                          NSLog(@"placemark %@",placemark);
                          //String to hold address
                          NSLog(@"placemark %@",placemark.region);
                          NSLog(@"placemark %@",placemark.country);  // Give Country Name
                          NSLog(@"placemark %@",placemark.locality); // Extract the city name
                          NSLog(@"location %@",placemark.name);
                          NSLog(@"location %@",placemark.ocean);
                          NSLog(@"location %@",placemark.postalCode);
                          NSLog(@"location %@",placemark.subLocality);
                          NSString *countryCode = placemark.ISOcountryCode;
                          
                          NSLog(@"ISO country code:%@",countryCode);
                          NSLog(@"location %@",placemark.location);

                          NSLog(@"ISO COUNTRY CODE: %@",countryCode);
                          [self saveData2:countryCode :latitude :longitude];
                          self.locationViewController=[self.storyboard instantiateViewControllerWithIdentifier:@"loginScreen"];
                          
                          
                          [self presentViewController:self.locationViewController animated:NO completion:nil];
                          
                          
                      }
                      else {
                           [self showAlert:@"Alert" :@"Location not enabled"];

                          NSLog(@"Could not locate");
                          self.locationViewController=[self.storyboard instantiateViewControllerWithIdentifier:@"loginScreen"];
                          
                          
                          [self presentViewController:self.locationViewController animated:NO completion:nil];
                      }
                  }
         ];
        
    }
    else{
        [self showAlert:@"Alert" :@"Location not enabled"];
        NSLog(@"Error no location");
        self.locationViewController=[self.storyboard instantiateViewControllerWithIdentifier:@"loginScreen"];
        
        
        [self presentViewController:self.locationViewController animated:NO completion:nil];
        
    }
    //[locationManager stopUpdatingLocation];
    
    // Send Data to kanan server
    


}
-(void)showAlert:(NSString *)title :(NSString *)body {
    UIAlertController * alert=   [UIAlertController
                                  alertControllerWithTitle:title
                                  message:body
                                  preferredStyle:UIAlertControllerStyleAlert];
    
    UIAlertAction* ok = [UIAlertAction
                         actionWithTitle:@"OK"
                         style:UIAlertActionStyleDefault
                         handler:^(UIAlertAction * action)
                         {
                             [alert dismissViewControllerAnimated:YES completion:nil];
                             
                         }];
    
    [alert addAction:ok];
    [self presentViewController:alert animated:YES completion:nil];
}
@end
