//
//  EnableAccountViewController.m
//  KANAN angel
//
//  Created by Jorge Macias on 11/8/17.
//  Copyright © 2017 Jorge Macias. All rights reserved.
//

#import "EnableAccountViewController.h"
#import "AppDelegate.h"
#import "CoreData.h"
@interface EnableAccountViewController ()
@property (weak, nonatomic) IBOutlet UILabel *accountName;
@property (weak, nonatomic) IBOutlet UILabel *accountPhone;
@property (weak, nonatomic) IBOutlet UILabel *enableLabel;
@property (weak, nonatomic) IBOutlet UISwitch *enableSwitch;
@property (weak, nonatomic) IBOutlet UIButton *saveButton;
@property (weak, nonatomic) IBOutlet NSString *kananError;
@property (weak, nonatomic) IBOutlet NSString *kananMessage;
@property (weak, nonatomic) IBOutlet NSString *countrycode;
@property (weak, nonatomic) IBOutlet NSString *shareLocationState;
@property (weak, nonatomic) IBOutlet NSString *enableState;
@property (weak, nonatomic) IBOutlet NSString *routeTracingState;
@property (weak, nonatomic) IBOutlet NSString *receivedTime;
@end

@implementation EnableAccountViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    self.title = NSLocalizedString(@"Contact",nil);
    self.navigationController.delegate = self;
    self.navigationController.navigationBar.backgroundColor = [UIColor whiteColor];
    self.navigationController.navigationBar.translucent = YES;
    self.navigationController.navigationBar.backItem.title=NSLocalizedString(@"Settings",nil);;
    [[self navigationController] setNavigationBarHidden:NO animated:YES];
    // Do any additional setup after loading the view.
}
-(void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    self.countrycode=[self retrieve:@"telephony_cc"];
    NSString *str = @"52";
    str = [str stringByAppendingString:self.phoneNumber];;
    
    self.phoneNumber=str;
    self.accountName.text=self.userName;
    self.accountPhone.text=self.phoneNumber;
    self.shareLocationState=@"False";
    
    self.routeTracingState=@"False";
    [self isThisAnAngel];
}
- (void) viewDidAppear:(BOOL)animated{

}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
- (IBAction)backButtonTapped:(UIButton *)sender {
    NSLog(@"Back Pressed");
    [self dismissViewControllerAnimated:YES completion:nil];
}

-(void) isThisAnAngel{
    NSString *mask=[NSString stringWithFormat:@"%@%@%@", @"http://", KANAN_url, @"/api/noa/isThisanAngel?myphone=%@&angel_phone=%@&button=%@"];
    NSString *urlString = [NSString stringWithFormat:mask,
                           [self getphonenumber],
                           self.phoneNumber,
                           self.button
                           ];
    
    NSLog(@"URL: %@",urlString);
    
    NSMutableURLRequest *request = [[NSMutableURLRequest alloc] initWithURL:[NSURL URLWithString:urlString]];
    [request setHTTPMethod:@"GET"];
    [request setValue:@"application/json" forHTTPHeaderField:@"Accept"];
    [request setValue:@"application/json" forHTTPHeaderField:@"Content-Type"];
    NSURLSession *session = [NSURLSession sharedSession];
    [[session dataTaskWithURL:[NSURL URLWithString:urlString]
            completionHandler:^(NSData *responseData,
                                NSURLResponse *responseCode,
                                NSError *responseError)
      {
          // This will get the NSURLResponse into NSHTTPURLResponse format
          NSHTTPURLResponse* httpResponse = (NSHTTPURLResponse*)responseCode;
          // This will Fetch the status code from NSHTTPURLResponse object
          int responseStatusCode = (int)[httpResponse statusCode];
          //Just to make sure, it works or not
          NSLog(@"Status Code :: %d", responseStatusCode);
          NSDictionary *userInfo = [responseError userInfo];
          NSString *errorString = [[userInfo objectForKey:NSUnderlyingErrorKey] localizedDescription];
          NSLog(@"Error string: %@",errorString);
          if ([responseData length] > 0 && responseError == nil)
          {
              id jsonData = [NSJSONSerialization JSONObjectWithData:responseData options:NSJSONReadingAllowFragments error:&responseError];
              NSDictionary *responseDict = jsonData;
              //NSString *kananError = @"";
              //NSString *kananMessage = @"Distributor";
              NSString *title=@"Alert!";
              NSLog(@"sucess reading");
              dispatch_async(dispatch_get_main_queue(), ^{
                  // UI Update
                  if ([responseDict objectForKey:@"Error"])
                  {
                      self.kananError = [responseDict objectForKey:@"Error"];

                      if ([[responseDict objectForKey:@"Error"] isEqual:@"none"])
                      {
                          NSString *receivedEnabled=[responseDict objectForKey:@"enable"];
                          NSLog(@"ENABLED: %@",receivedEnabled);
                          self.receivedTime =[responseDict objectForKey:@"time"];
                          if ([receivedEnabled isEqualToString:@"True"]){
                              self.enableState=@"True";
                              [self.enableSwitch setOn:YES animated:NO];
                          }
                          else{
                              self.enableState=@"False";
                              [self.enableSwitch setOn:NO animated:NO];
                          }
                          
                          self.enableLabel.hidden=NO;
                          self.enableSwitch.hidden=NO;
                          self.saveButton.hidden=NO;
                         
                      }
                      
                      else
                      {
                          [self showAlert:title : [responseDict objectForKey:@"message"]];
                      }
                      
                  }
              });
              
          }
          
          else if ([responseData length] == 0 && responseError == nil)
          {
              
              [self showAlert: @"Data Error" :@"Please check credentials or connection"];
              NSLog(@"data error: %@", responseError);
          }
          else if (responseError != nil && responseError.code == NSURLErrorTimedOut)
          {
              [self showAlert:@"Data Time Out" :@"Please check internet is available"];
              NSLog(@"data timeout: %ld", (long)NSURLErrorTimedOut);
          }
          else if (responseError != nil)
          {
              [self showAlert:@"Access is denied" :@"Please check credentials or connection"];
              NSLog(@"data download error: %@",responseError);
          }
          
          
          
      }]
     resume];
}
- (IBAction)shareLocationToggled:(UISwitch *)sender {
    UISwitch *mySwitch = (UISwitch *)sender;
    if ([mySwitch isOn]) {
        self.shareLocationState=@"True";
    } else {
        self.shareLocationState=@"False";
    }
}
- (IBAction)enableToggled:(UISwitch *)sender {
    UISwitch *mySwitch = (UISwitch *)sender;
    if ([mySwitch isOn]) {
        self.enableState=@"True";
    } else {
        self.enableState=@"False";
    }
}
- (IBAction)routeTracingToggled:(UISwitch *)sender {
    UISwitch *mySwitch = (UISwitch *)sender;
    if ([mySwitch isOn]) {
        self.routeTracingState=@"True";
    } else {
        self.routeTracingState=@"False";
    }
}


- (IBAction)saveButtonTapped:(UIButton *)sender {
    //http://app.dwim.mx/api/noa/setAngel/?myphone=525543513556&angel_phone=525543584656&enable=True&message=me%20alerte&share_location=True&route_tracing=True&time=5
    NSString *mask=[NSString stringWithFormat:@"%@%@%@", @"http://", KANAN_url, @"/api/noa/setAngel?myphone=%@&angel_phone=%@&enable=%@&button=%@"];
    NSString *urlString = [NSString stringWithFormat:mask,
                           [self getphonenumber],
                           self.phoneNumber,
                           self.enableState,
                           self.button
                           ];
    
    NSLog(@"URL: %@",urlString);
                    
    
    NSMutableURLRequest *request = [[NSMutableURLRequest alloc] initWithURL:[NSURL URLWithString:urlString]];
    [request setHTTPMethod:@"GET"];
    [request setValue:@"application/json" forHTTPHeaderField:@"Accept"];
    [request setValue:@"application/json" forHTTPHeaderField:@"Content-Type"];
    NSURLSession *session = [NSURLSession sharedSession];
    [[session dataTaskWithURL:[NSURL URLWithString:urlString]
            completionHandler:^(NSData *responseData,
                                NSURLResponse *responseCode,
                                NSError *responseError)
      {
          // This will get the NSURLResponse into NSHTTPURLResponse format
          NSHTTPURLResponse* httpResponse = (NSHTTPURLResponse*)responseCode;
          // This will Fetch the status code from NSHTTPURLResponse object
          int responseStatusCode = (int)[httpResponse statusCode];
          //Just to make sure, it works or not
          NSLog(@"Status Code :: %d", responseStatusCode);
          NSDictionary *userInfo = [responseError userInfo];
          NSString *errorString = [[userInfo objectForKey:NSUnderlyingErrorKey] localizedDescription];
          NSLog(@"Error string: %@",errorString);
          if ([responseData length] > 0 && responseError == nil)
          {
              id jsonData = [NSJSONSerialization JSONObjectWithData:responseData options:NSJSONReadingAllowFragments error:&responseError];
              NSDictionary *responseDict = jsonData;
              //NSString *kananError = @"";
              //NSString *kananMessage = @"Distributor";
              NSString *title=@"Error1";
              NSLog(@"sucess reading");
              dispatch_async(dispatch_get_main_queue(), ^{
                  // UI Update
                  if ([responseDict objectForKey:@"Error"])
                  {
                      self.kananError = [responseDict objectForKey:@"Error"];
                      if ([responseDict objectForKey:@"message"])
                      {
                          self.kananMessage = [responseDict objectForKey:@"message"];
                      }
                      if ([[responseDict objectForKey:@"Error"] isEqual:@"none"])
                      {
                          [self showAlert:@"Message" : @"Data saved"];
                          
                      }
                      
                      else
                      {
                          [self showAlert:title :self.kananMessage];
                      }
                      
                  }
              });
              
          }
          
          else if ([responseData length] == 0 && responseError == nil)
          {
              
              [self showAlert: @"Data Error" :@"Please check credentials or connection"];
              NSLog(@"data error: %@", responseError);
          }
          else if (responseError != nil && responseError.code == NSURLErrorTimedOut)
          {
              [self showAlert:@"Data Time Out" :@"Please check internet is available"];
              NSLog(@"data timeout: %ld", (long)NSURLErrorTimedOut);
          }
          else if (responseError != nil)
          {
              [self showAlert:@"Access is denied" :@"Please check credentials or connection"];
              NSLog(@"data download error: %@",responseError);
          }
          
          
          
      }]
     resume];
}


-(void)showAlert:(NSString *)title :(NSString *)body {
    UIAlertController * alert=   [UIAlertController
                                  alertControllerWithTitle:title
                                  message:body
                                  preferredStyle:UIAlertControllerStyleAlert];
    
    UIAlertAction* ok = [UIAlertAction
                         actionWithTitle:@"OK"
                         style:UIAlertActionStyleDefault
                         handler:^(UIAlertAction * action)
                         {
                             [alert dismissViewControllerAnimated:YES completion:nil];
                             
                         }];
    
    [alert addAction:ok];
    [self presentViewController:alert animated:YES completion:nil];
}
-(NSString *) getphonenumber{
    CoreData *orm=[[CoreData alloc] init];
    return [orm retrieve:@"phone_number"];
}
-(void) save:(NSString*)key :(NSString*)value{
    AppDelegate *appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    
    NSManagedObjectContext *context =
    [appDelegate managedObjectContext];
    
    NSEntityDescription *entityDesc =
    [NSEntityDescription entityForName:@"Settings"
                inManagedObjectContext:context];
    
    NSFetchRequest *request = [[NSFetchRequest alloc] init];
    [request setEntity:entityDesc];
    
    [request setEntity:entityDesc];
    
    NSPredicate *pred = [NSPredicate predicateWithFormat:@"(id = %@)", @"1"];
    
    [request setPredicate:pred];
    NSError *error;
    
    NSArray *results = [context executeFetchRequest:request
                                              error:&error];
    
    NSManagedObject* favoritsGrabbed = [results objectAtIndex:0];
    [favoritsGrabbed setValue:value forKey:key];
    [context save:&error];
    NSLog(@"updated core data");
}


- (NSString *) retrieve:(NSString*) key
{
    NSString *apnToken;
    AppDelegate *appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    
    NSManagedObjectContext *context =
    [appDelegate managedObjectContext];
    
    NSEntityDescription *entityDesc =
    [NSEntityDescription entityForName:@"Settings"
                inManagedObjectContext:context];
    
    NSFetchRequest *request = [[NSFetchRequest alloc] init];
    [request setEntity:entityDesc];
    
    [request setEntity:entityDesc];
    
    NSPredicate *pred = [NSPredicate predicateWithFormat:@"(id = %@)", @"1"];
    
    [request setPredicate:pred];
    
    NSManagedObject *matches = nil;
    
    NSError *error;
    
    NSArray *objects = [context executeFetchRequest:request
                                              error:&error];
    if ([objects count] == 0) {
        NSLog(@"No token");
        apnToken=@"";
    } else {
        matches = objects[0];
        apnToken = [matches valueForKey:key];
    }
    return apnToken;
}
/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/
- (IBAction)didExitAlarm:(id)sender {
}
- (IBAction)didExitonTime:(UITextField *)sender {
}


@end
