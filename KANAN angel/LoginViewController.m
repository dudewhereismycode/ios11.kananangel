//
//  LoginViewController.m
//  KANAN angel
//
//  Created by Jorge Macias on 10/30/17.
//  Copyright © 2017 Jorge Macias. All rights reserved.
//

#import "LoginViewController.h"
#import "AppDelegate.h"
#import <UIKit/UIKit.h>
#import "CoreData.h"

//#import "Account.h"

#define allTrim( object ) [object stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceCharacterSet] ]

@interface LoginViewController (){
    int res;
}

@property (weak, nonatomic) IBOutlet UIPickerView *pickerView;
@property (weak, nonatomic) IBOutlet UITextField *countryCode;
@property (weak, nonatomic) IBOutlet UITextField *phoneNumber;
@property (weak, nonatomic) IBOutlet UITextField *name;
@property  BOOL responseState;
@property (weak, nonatomic) IBOutlet UITextField *lastname;
@property (strong, nonatomic) IBOutlet NSString *kananError;
@property (strong, nonatomic) IBOutlet NSString *kananMessage;
@property (strong, nonatomic) IBOutlet NSString *group;
@end

@implementation LoginViewController{
    UIAlertController * pinalert;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    self.responseState=YES;
    self.phoneNumber.delegate=self;

    /*
    Account *create=[[Account alloc] init];
    create.number1=0;
    NSLog(@"Return Error: %d",[create account:@"Jorge " :@"Macias"]);
    */
     // Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
- (BOOL)textFieldShouldReturn:(UITextField *)textField //resign first responder for textfield
{
    [textField resignFirstResponder];
    return YES;
}
- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string
{
    NSCharacterSet *numbersOnly = [NSCharacterSet characterSetWithCharactersInString:@"0123456789"];
    NSCharacterSet *characterSetFromTextField = [NSCharacterSet characterSetWithCharactersInString:textField.text];
    
    BOOL stringIsValid = [numbersOnly isSupersetOfSet:characterSetFromTextField];
    return stringIsValid;
}

-(void) saveData:(NSString*)phone :(NSString*)cc{
    AppDelegate *appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    
    NSManagedObjectContext *context =
    [appDelegate managedObjectContext];
    
    NSEntityDescription *entityDesc =
    [NSEntityDescription entityForName:@"Settings"
                inManagedObjectContext:context];
    
    NSFetchRequest *request = [[NSFetchRequest alloc] init];
    [request setEntity:entityDesc];
    
    [request setEntity:entityDesc];
    
    NSPredicate *pred = [NSPredicate predicateWithFormat:@"(id = %@)", @"1"];
    
    [request setPredicate:pred];
    NSError *error;
    
    NSArray *results = [context executeFetchRequest:request
                                              error:&error];

    NSString *t=[self.countryCode.text stringByAppendingString:self.phoneNumber.text];
    NSString *npn2=[t substringFromIndex:1];

    NSLog(@"NEW PHONE: %@",npn2);
    NSManagedObject* favoritsGrabbed = [results objectAtIndex:0];
    [favoritsGrabbed setValue:npn2 forKey:@"phone_number"];
    [favoritsGrabbed setValue:@"angel@dwim.mx" forKey:@"email"];
    [favoritsGrabbed setValue: @"angel" forKey:@"group"];
    [favoritsGrabbed setValue: self.name.text forKey:@"name"];
    [favoritsGrabbed setValue: self.lastname.text forKey:@"lastname"];
    [favoritsGrabbed setValue:@"YES" forKey:@"valid"];
    [favoritsGrabbed setValue:cc forKey:@"telephony_cc"];
    [context save:&error];
    NSLog(@"updated core data");
}
-(NSString *) getCountry{
    AppDelegate *appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    NSManagedObjectContext *context =
    [appDelegate managedObjectContext];
    NSEntityDescription *entityDesc =
    [NSEntityDescription entityForName:@"Settings"
                inManagedObjectContext:context];
    NSFetchRequest *request = [[NSFetchRequest alloc] init];
    [request setEntity:entityDesc];
    NSPredicate *pred = [NSPredicate predicateWithFormat:@"(id = %@)", @"1"];
    [request setPredicate:pred];
    NSManagedObject *matches = nil;
    NSError *error;
    NSArray *objects = [context executeFetchRequest:request
                                              error:&error];
    NSString *cc=@"";
    if ([objects count] == 0) {
        NSLog(@"vacio");
        
    } else {
        matches = objects[0];
        
        cc=[matches valueForKey:@"country"];
        return cc;
    }
    return @"";
}
-(NSString *) getLatitude{
    AppDelegate *appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    NSManagedObjectContext *context =
    [appDelegate managedObjectContext];
    NSEntityDescription *entityDesc =
    [NSEntityDescription entityForName:@"Settings"
                inManagedObjectContext:context];
    NSFetchRequest *request = [[NSFetchRequest alloc] init];
    [request setEntity:entityDesc];
    NSPredicate *pred = [NSPredicate predicateWithFormat:@"(id = %@)", @"1"];
    [request setPredicate:pred];
    NSManagedObject *matches = nil;
    NSError *error;
    NSArray *objects = [context executeFetchRequest:request
                                              error:&error];
    NSString *latitude=@"";
    if ([objects count] == 0) {
        NSLog(@"vacio");
        
    } else {
        matches = objects[0];
        
        latitude=[matches valueForKey:@"ilatitude"];
        return latitude;
    }
    return @"";
}
-(NSString *) getLongitude{
    AppDelegate *appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    NSManagedObjectContext *context =
    [appDelegate managedObjectContext];
    NSEntityDescription *entityDesc =
    [NSEntityDescription entityForName:@"Settings"
                inManagedObjectContext:context];
    NSFetchRequest *request = [[NSFetchRequest alloc] init];
    [request setEntity:entityDesc];
    NSPredicate *pred = [NSPredicate predicateWithFormat:@"(id = %@)", @"1"];
    [request setPredicate:pred];
    NSManagedObject *matches = nil;
    NSError *error;
    NSArray *objects = [context executeFetchRequest:request
                                              error:&error];
    NSString *longitude=@"";
    if ([objects count] == 0) {
        NSLog(@"vacio");
        
    } else {
        matches = objects[0];
        
        longitude=[matches valueForKey:@"ilongitude"];
        return longitude;
    }
    return @"";
}

-(NSString *) getphonenumber{
    AppDelegate *appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    NSManagedObjectContext *context =
    [appDelegate managedObjectContext];
    NSEntityDescription *entityDesc =
    [NSEntityDescription entityForName:@"Settings"
                inManagedObjectContext:context];
    NSFetchRequest *request = [[NSFetchRequest alloc] init];
    [request setEntity:entityDesc];
    NSPredicate *pred = [NSPredicate predicateWithFormat:@"(id = %@)", @"1"];
    [request setPredicate:pred];
    NSManagedObject *matches = nil;
    NSError *error;
    NSArray *objects = [context executeFetchRequest:request
                                              error:&error];
    NSString *pn=@"";
    NSString *tcc=@"";
    if ([objects count] == 0) {
        NSLog(@"vacio");
        
    } else {
        matches = objects[0];
        pn=[matches valueForKey:@"phone_number"];
        tcc=[matches valueForKey:@"telephony_cc"];
        return [tcc stringByAppendingString:pn];
    }
    return @"";
}
- (void) viewDidAppear:(BOOL)animated{
    dictCodes = [NSDictionary dictionaryWithObjectsAndKeys:@"681",@"WF",@"81",@"JP",@"1876",@"JM",@"962",@"JO",@"685",@"WS",@"441534",@"JE",@"245",@"GW",@"1671",@"GU",@"502",@"GT",@"",@"GS",@"30",@"GR",@"240",@"GQ",@"590",@"GP",@"592",@"GY",@"441481",@"GG",@"594",@"GF",@"995",@"GE",@"1473",@"GD",@"44",@"GB",@"241",@"GA",@"224",@"GN",@"220",@"GM",@"299",@"GL",@"350",@"GI",@"233",@"GH",@"1787",@"PR",@"970",@"PS",@"680",@"PW",@"351",@"PT",@"595",@"PY",@"507",@"PA",@"689",@"PF",@"675",@"PG",@"51",@"PE",@"92",@"PK",@"63",@"PH",@"870",@"PN",@"48",@"PL",@"508",@"PM",@"260",@"ZM",@"27",@"ZA",@"263",@"ZW",@"382",@"ME",@"373",@"MD",@"261",@"MG",@"590",@"MF",@"212",@"MA",@"377",@"MC",@"95",@"MM",@"223",@"ML",@"853",@"MO",@"976",@"MN",@"692",@"MH",@"389",@"MK",@"230",@"MU",@"356",@"MT",@"265",@"MW",@"960",@"MV",@"596",@"MQ",@"1670",@"MP",@"1664",@"MS",@"222",@"MR",@"60",@"MY",@"52",@"MX",@"258",@"MZ",@"33",@"FR",@"358",@"FI",@"679",@"FJ",@"500",@"FK",@"691",@"FM",@"298",@"FO",@"682",@"CK",@"225",@"CI",@"41",@"CH",@"57",@"CO",@"86",@"CN",@"237",@"CM",@"56",@"CL",@"61",@"CC",@"242",@"CG",@"236",@"CF",@"243",@"CD",@"420",@"CZ",@"357",@"CY",@"61",@"CX",@"506",@"CR",@"599",@"CW",@"238",@"CV",@"53",@"CU",@"268",@"SZ",@"963",@"SY",@"599",@"SX",@"211",@"SS",@"597",@"SR",@"503",@"SV",@"239",@"ST",@"421",@"SK",@"47",@"SJ",@"386",@"SI",@"290",@"SH",@"252",@"SO",@"221",@"SN",@"378",@"SM",@"232",@"SL",@"248",@"SC",@"677",@"SB",@"966",@"SA",@"65",@"SG",@"46",@"SE",@"249",@"SD",@"967",@"YE",@"262",@"YT",@"961",@"LB",@"1758",@"LC",@"856",@"LA",@"94",@"LK",@"423",@"LI",@"371",@"LV",@"370",@"LT",@"352",@"LU",@"231",@"LR",@"266",@"LS",@"218",@"LY",@"379",@"VA",@"1784",@"VC",@"58",@"VE",@"1284",@"VG",@"964",@"IQ",@"1340",@"VI",@"354",@"IS",@"98",@"IR",@"39",@"IT",@"84",@"VN",@"441624",@"IM",@"678",@"VU",@"246",@"IO",@"91",@"IN",@"353",@"IE",@"62",@"ID",@"880",@"BD",@"32",@"BE",@"226",@"BF",@"359",@"BG",@"387",@"BA",@"1246",@"BB",@"590",@"BL",@"1441",@"BM",@"673",@"BN",@"591",@"BO",@"973",@"BH",@"257",@"BI",@"229",@"BJ",@"975",@"BT",@"",@"BV",@"267",@"BW",@"599",@"BQ",@"55",@"BR",@"1242",@"BS",@"375",@"BY",@"501",@"BZ",@"7",@"RU",@"250",@"RW",@"381",@"RS",@"262",@"RE",@"40",@"RO",@"968",@"OM",@"385",@"HR",@"509",@"HT",@"36",@"HU",@"852",@"HK",@"504",@"HN",@"999",@"HM",@"212",@"EH",@"372",@"EE",@"20",@"EG",@"593",@"EC",@"251",@"ET",@"34",@"ES",@"291",@"ER",@"598",@"UY",@"998",@"UZ",@"1",@"US",@"999",@"UM",@"256",@"UG",@"380",@"UA",@"972",@"IL",@"505",@"NI",@"31",@"NL",@"47",@"NO",@"264",@"NA",@"687",@"NC",@"227",@"NE",@"672",@"NF",@"234",@"NG",@"64",@"NZ",@"977",@"NP",@"674",@"NR",@"683",@"NU",@"",@"XK",@"996",@"KG",@"254",@"KE",@"686",@"KI",@"855",@"KH",@"1869",@"KN",@"269",@"KM",@"82",@"KR",@"850",@"KP",@"965",@"KW",@"7",@"KZ",@"1345",@"KY",@"1809",@"DO",@"1767",@"DM",@"253",@"DJ",@"45",@"DK",@"49",@"DE",@"213",@"DZ",@"255",@"TZ",@"688",@"TV",@"886",@"TW",@"1868",@"TT",@"90",@"TR",@"216",@"TN",@"676",@"TO",@"670",@"TL",@"993",@"TM",@"992",@"TJ",@"690",@"TK",@"66",@"TH",@"",@"TF",@"228",@"TG",@"235",@"TD",@"1649",@"TC",@"971",@"AE",@"376",@"AD",@"1268",@"AG",@"93",@"AF",@"1264",@"AI",@"374",@"AM",@"355",@"AL",@"244",@"AO",@"0",@"AQ",@"1684",@"AS",@"54",@"AR",@"61",@"AU",@"43",@"AT",@"297",@"AW",@"35818",@"AX",@"994",@"AZ",@"974",@"QA",@"2",@"CA",
                 
                 nil];
    
    self.pickerView.dataSource=self;
    self.pickerView.delegate=self;
    
    NSLog(@"Number of countries: %ld",(long)[[[self class] countryCodes] count]);

    // Do any additional setup after loading the view.
    //
    NSString *country = [self getCountry];


    
    NSString *telephonyCountryCode = [dictCodes objectForKey:country];
    if (telephonyCountryCode==nil){
        telephonyCountryCode=@"";
    }
    
    NSLog(@"Telephony Country code %@",telephonyCountryCode);
    self.countryCode.text=[@"+" stringByAppendingString:telephonyCountryCode];
    NSString *itemcc;
    
    int item;
    
    for (item=0; item<= [[[self class] countryCodes] count]-1;item++){
        itemcc=[[self class] countryCodes][item];
        if ([itemcc isEqualToString:country]){
            NSLog(@"Country found at %d",item);
            break;
        }
        
    }
    
    [self.pickerView setUserInteractionEnabled:YES];
    [self.pickerView selectRow:item inComponent:0 animated:YES];
    
}
- (IBAction)createAccountButtonTapped:(UIButton *)sender {
    self.phoneNumber.text = [[self.phoneNumber.text componentsSeparatedByCharactersInSet:
                            [[NSCharacterSet decimalDigitCharacterSet] invertedSet]]
                           componentsJoinedByString:@""];
    unsigned long namelen = [self.name.text length];
    unsigned long lastnamelen = [self.lastname.text length];
    unsigned long phonelen = [self.phoneNumber.text length];
   

    if (namelen<1 || lastnamelen<1 || phonelen<10 || phonelen>10){
        [self showAlert:@"Alert!!" :@"You must capture Name,last name and a telephone number with at least a 10 digit lenght."];
    }
    else{
        NSString *completePhone=[self.countryCode.text stringByAppendingString:self.phoneNumber.text];
        NSString *mask=[NSString stringWithFormat:@"%@%@%@", @"http://", KANAN_url, @"/api/noa/twilio_create?phone=%@"];
        NSString *urlString = [NSString stringWithFormat:mask, [completePhone lowercaseString]];
        
        NSMutableURLRequest *request = [[NSMutableURLRequest alloc] initWithURL:[NSURL URLWithString:urlString]];
        [request setHTTPMethod:@"GET"];
        [request setValue:@"application/json" forHTTPHeaderField:@"Accept"];
        [request setValue:@"application/json" forHTTPHeaderField:@"Content-Type"];
        // Send your request asynchronously
        NSLog(@"Just before send asyncronous ");
        NSURLSession *session = [NSURLSession sharedSession];
        [[session dataTaskWithURL:[NSURL URLWithString:urlString]
                completionHandler:^(NSData *responseData,
                                    NSURLResponse *responseCode,
                                    NSError *responseError)
          {
              // This will get the NSURLResponse into NSHTTPURLResponse format
              NSHTTPURLResponse* httpResponse = (NSHTTPURLResponse*)responseCode;
              // This will Fetch the status code from NSHTTPURLResponse object
              int responseStatusCode = (int)[httpResponse statusCode];
              //Just to make sure, it works or not
              NSLog(@"Status Code :: %d", responseStatusCode);
              NSDictionary *userInfo = [responseError userInfo];
              NSString *errorString = [[userInfo objectForKey:NSUnderlyingErrorKey] localizedDescription];
              NSLog(@"Error string: %@",errorString);
              if ([responseData length] > 0 && responseError == nil)
              {
                  id jsonData = [NSJSONSerialization JSONObjectWithData:responseData options:NSJSONReadingAllowFragments error:&responseError];
                  NSDictionary *responseDict = jsonData;
                  //NSString *kananError = @"";
                  //NSString *kananMessage = @"Distributor";
                  NSString *title=@"Error1";
                  NSLog(@"sucess reading");
                
                  dispatch_async(dispatch_get_main_queue(), ^{
                      // UI Update
                    
                          NSLog(@"Enter");
                      if ([responseDict objectForKey:@"Error"])
                      {
                          self.kananError = [responseDict objectForKey:@"Error"];
                          if ([responseDict objectForKey:@"message"])
                          {
                              self.kananMessage = [responseDict objectForKey:@"message"];
                          }
                          if ([[responseDict objectForKey:@"Error"] isEqual:@"none"])
                          {
                              
                              
                              
                                  NSString *languageMessage=NSLocalizedString(@"Please enter password SMS password",nil);
                                  NSString *secondMessage=NSLocalizedString(@"to create account",nil);
                                  
                              self->pinalert=   [UIAlertController
                                                                   alertControllerWithTitle:languageMessage message:secondMessage
                                                                   preferredStyle:UIAlertControllerStyleAlert];
                                  
                                  UIAlertAction* ok = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault
                                                                             handler:^(UIAlertAction * action) {
                                                                                 //Do Some action here
                                                                                 NSString *captured_pin = ((UITextField *)[self->pinalert.textFields objectAtIndex:0]).text;
                                                                                 NSLog(@"Group 2:%@",captured_pin);
                                                                                 self.group=captured_pin;
                                                                                 if ([[responseDict objectForKey:@"password"] isEqual:captured_pin])
                                                                                 {
                                                                                     self.responseState=NO;
                                                                                     [self saveData:self.countryCode.text :self.phoneNumber.text];
                                                                                     [self createAccount];
                                                                                     self.locationViewController=[self.storyboard instantiateViewControllerWithIdentifier:@"tabbar"];
                                                                                     
                                                                                     [self presentViewController:self.locationViewController animated:NO completion:nil];
                                                                                     
                                                                                     
                                                                                     
                                                                                 }else
                                                                                 {
                                                                                     //[self showAlert:@"Password" :  @"Error!"];
                                                                                     self->pinalert.title=NSLocalizedString(@"Wrong password",nil);
                                                                                     self->pinalert.message=NSLocalizedString(@"Try again",nil);
                                                                                     [self presentViewController:self->pinalert animated:YES completion:nil];
                                                                                 }
                                                                                 
                                                                             }];
                                  
                                  languageMessage=@"Cancel";
                                  UIAlertAction* cancel = [UIAlertAction actionWithTitle:languageMessage style:UIAlertActionStyleDefault
                                                                                 handler:^(UIAlertAction * action) {self.group=@"\\";[self showAlert:NSLocalizedString(@"Aborting",nil) :NSLocalizedString(@"Press create account again",nil)];self.responseState=NO;}];
                              [self->pinalert addAction:ok];
                              [self->pinalert addAction:cancel];
                              [self->pinalert addTextFieldWithConfigurationHandler:^(UITextField *textField) {
                                      textField.placeholder = NSLocalizedString(@"Pin",nil);
                                      textField.keyboardType = UIKeyboardTypeAlphabet;
                                  }];
                                  NSLog(@"Display");
                              [self presentViewController:self->pinalert animated:YES completion:nil];
                                  
                              
                              }
                              
                          
                          
                          else
                          {
                              [self showAlert:title :self.kananMessage];
                          }
                          
                      }
                          
                      
                  });
                  
              }
              
              else if ([responseData length] == 0 && responseError == nil)
              {
                  [self senderror:@"Data Error" message:@"Please check credentials or connection"];
                  NSLog(@"data error: %@", responseError);
              }
              else if (responseError != nil && responseError.code == NSURLErrorTimedOut)
              {
                  [self senderror:@"Data Time Out" message:@"Please check internet is available"];
                  NSLog(@"data timeout: %ld", (long)NSURLErrorTimedOut);
              }
              else if (responseError != nil)
              {
                  [self senderror:@"Access is denied" message:@"Please check credentials or connection"];
                  NSLog(@"data download error: %@",responseError);
              }
              
              
              
          }]
         resume];
    }
    CoreData *orm=[[CoreData alloc] init ];
    NSString *pn=[orm retrieve:@"phone_number"];
    NSLog(@"IMEI: %@",pn);

}


-(int) batterylevel
{
    int level;
    UIDevice *myDevice = [UIDevice currentDevice];
    [myDevice setBatteryMonitoringEnabled:YES];
    int state = [myDevice batteryState];
    NSLog(@"battery status: %d",state); // 0 unknown, 1 unplegged, 2 charging, 3 full
    double batLeft = (float)[myDevice batteryLevel] * 100;
    NSLog(@"battery left: %f", batLeft);
    level=(int)batLeft;
    return level;
}
- (void) send2server: (NSString *) phone_number latitude:(NSString *) latitude longitude:(NSString *) longitude speed: (NSString *) speed imei: (NSString *) imei group: (NSString *) group email: (NSString *) email
{
    
    NSString *battery = [[NSNumber numberWithDouble:[self batterylevel]] stringValue];
    NSString *femail = [email stringByAddingPercentEncodingWithAllowedCharacters:[NSCharacterSet URLHostAllowedCharacterSet]];
    NSString *fgroup = [group stringByAddingPercentEncodingWithAllowedCharacters:[NSCharacterSet URLHostAllowedCharacterSet]];
    NSString *mask=[NSString stringWithFormat:@"%@%@%@", @"http://", KANAN_url, @"/api/noa/location/?imei=%@&email=%@&group=%@&latitude=%@&longitude=%@&speed=%@&battery=%@"];
    NSString *urlString = [NSString stringWithFormat:mask,
                           imei,
                           femail,
                           fgroup,
                           latitude,
                           longitude,
                           speed,
                           battery];
    
    
    NSMutableURLRequest *request = [[NSMutableURLRequest alloc] initWithURL:[NSURL URLWithString:urlString]];
    
    [request setHTTPMethod:@"GET"];
    [request setValue:@"application/json" forHTTPHeaderField:@"Accept"];
    [request setValue:@"application/json" forHTTPHeaderField:@"Content-Type"];
    // Send your request asynchronously
    NSLog(@"%@",urlString);
    NSLog(@"Just before send asyncronous ");
    NSURLSession *session = [NSURLSession sharedSession];
    [[session dataTaskWithURL:[NSURL URLWithString:urlString]
            completionHandler:^(NSData *responseData,
                                NSURLResponse *responseCode,
                                NSError *responseError)
      {
          // This will get the NSURLResponse into NSHTTPURLResponse format
          NSHTTPURLResponse* httpResponse = (NSHTTPURLResponse*)responseCode;
          // This will Fetch the status code from NSHTTPURLResponse object
          int responseStatusCode = (int)[httpResponse statusCode];
          //Just to make sure, it works or not
          NSLog(@"Status Code :: %d", responseStatusCode);
          NSDictionary *userInfo = [responseError userInfo];
          NSString *errorString = [[userInfo objectForKey:NSUnderlyingErrorKey] localizedDescription];
          NSLog(@"Error string: %@",errorString);
          if ([responseData length] > 0 && responseError == nil)
          {
              id jsonData = [NSJSONSerialization JSONObjectWithData:responseData options:NSJSONReadingAllowFragments error:&responseError];
              NSDictionary *responseDict = jsonData;
              NSString *kananError = @"";
              NSLog(@"Success location");
              if ([[responseDict objectForKey:@"Error"] isEqual:@"none"])
              {
                  NSLog(@"Success");
                  kananError=[responseDict objectForKey:@"Error"];
                  NSLog(@"Error: %@",kananError);
              }
              
              
          }
          
          else if ([responseData length] == 0 && responseError == nil)
          {
              [self senderror:@"Data Error" message:@"Please check credentials or connection"];
              NSLog(@"data error: %@", responseError);
          }
          else if (responseError != nil && responseError.code == NSURLErrorTimedOut)
          {
              [self senderror:@"Data Time Out" message:@"Please check internet is available"];
              NSLog(@"data timeout: %ld", (long)NSURLErrorTimedOut);
          }
          else if (responseError != nil)
          {
              [self senderror:@"Access is denied" message:@"Please check credentials or connection"];
              NSLog(@"data download error: %@",responseError);
          }
          
      }]
     resume];
    
}
-(void)updateChannel: (NSString *) imei group: (NSString *) group email: (NSString *) email date: (NSString *) date token: (NSString *) token  {
    NSString *mask=[NSString stringWithFormat:@"%@%@%@", @"http://", KANAN_url, @"/api/noa/update_channel/?imei=%@&group=%@&email=%@&date=%@&channel=%@"];
    NSString *urlString = [NSString stringWithFormat:mask,
                           imei,
                           group,
                           email,
                           date,
                           token];
    
    //HTTP Basic Authentication
    //NSString *authenticationString = [NSString stringWithFormat:@"%@:%@",@"",@""];
    //NSData *authenticationData = [authenticationString dataUsingEncoding:NSASCIIStringEncoding];
    //NSString *authenticationValue = [authenticationData base64EncodedStringWithOptions:0];
    //NSLog(@"url string: %@",urlString);
    //Set up your request
    /*
     NSMutableURLRequest *request = [[NSMutableURLRequest alloc] initWithURL:[NSURL URLWithString:@"http://192.168.1.9:5000/api/v1.0/posts/?latitude=19.523689&longitude=-99.239017&email=somosmoviesmexico%40gmail.com&uuid=999999999&device_name=iphone"]];
     */
    NSMutableURLRequest *request = [[NSMutableURLRequest alloc] initWithURL:[NSURL URLWithString:urlString]];
    // Set time out
    //[request setTimeoutInterval:10];
    // Set your user login credentials
    //[request setValue:[NSString stringWithFormat:@"Basic %@", authenticationValue] forHTTPHeaderField:@"Authorization"];
    [request setHTTPMethod:@"GET"];
    [request setValue:@"application/json" forHTTPHeaderField:@"Accept"];
    [request setValue:@"application/json" forHTTPHeaderField:@"Content-Type"];
    // Send your request asynchronously
    NSLog(@"Just before send asyncronous ");
    NSURLSession *session = [NSURLSession sharedSession];
    [[session dataTaskWithURL:[NSURL URLWithString:urlString]
            completionHandler:^(NSData *responseData,
                                NSURLResponse *responseCode,
                                NSError *responseError)
      {
          // This will get the NSURLResponse into NSHTTPURLResponse format
          NSHTTPURLResponse* httpResponse = (NSHTTPURLResponse*)responseCode;
          // This will Fetch the status code from NSHTTPURLResponse object
          int responseStatusCode = (int)[httpResponse statusCode];
          //Just to make sure, it works or not
          NSLog(@"Status Code :: %d", responseStatusCode);
          NSDictionary *userInfo = [responseError userInfo];
          NSString *errorString = [[userInfo objectForKey:NSUnderlyingErrorKey] localizedDescription];
          NSLog(@"Error string: %@",errorString);
          if ([responseData length] > 0 && responseError == nil)
          {
              id jsonData = [NSJSONSerialization JSONObjectWithData:responseData options:NSJSONReadingAllowFragments error:&responseError];
              NSDictionary *responseDict = jsonData;
              NSString *kananError = @"";
              NSString *kananMessage = @"";
              NSLog(@"sucess reading");
              if ([responseDict objectForKey:@"Error"])
              {
                  kananError = [responseDict objectForKey:@"Error"];
                  if ([responseDict objectForKey:@"message"])
                  {
                      kananMessage = [responseDict objectForKey:@"message"];
                  }
                  if ([[responseDict objectForKey:@"Error"] isEqual:@"none"])
                  {
                      NSLog(@"Success update channel");
                  }
                  
                  
              }
          }
          
          else if ([responseData length] == 0 && responseError == nil)
          {
              [self senderror:@"Data Error" message:@"Please check credentials or connection"];
              NSLog(@"data error: %@", responseError);
          }
          else if (responseError != nil && responseError.code == NSURLErrorTimedOut)
          {
              [self senderror:@"Data Time Out" message:@"Please check internet is available"];
              NSLog(@"data timeout: %ld", (long)NSURLErrorTimedOut);
          }
          else if (responseError != nil)
          {
              [self senderror:@"Access is denied" message:@"Please check credentials or connection"];
              NSLog(@"data download error: %@",responseError);
          }
          
      }]
     resume];
    
    
}

- (void) createAccount{
    NSString *latitude =  [self getLatitude];
    NSString *longitude =  [self getLongitude];
    NSString *speed = @"0";
    NSLog(@"latitude: %@",latitude);
    NSLog(@"longitude: %@",longitude);
    NSString *token=[self retrieveToken];
    NSString *t=[self.countryCode.text stringByAppendingString:self.phoneNumber.text];
    NSString *npn2=[t substringFromIndex:1];
    NSLog(@"NEW PHONE:%@",npn2);
    NSString *imei=npn2;
    NSString *escapedemailNameString = [@"angel@dwim.mx" stringByAddingPercentEncodingWithAllowedCharacters:[NSCharacterSet URLHostAllowedCharacterSet]];
    NSString *escapedgroupString = [@"angel" stringByAddingPercentEncodingWithAllowedCharacters:[NSCharacterSet URLHostAllowedCharacterSet]];
    NSString *escapedname=[self.name.text stringByAddingPercentEncodingWithAllowedCharacters:[NSCharacterSet URLHostAllowedCharacterSet]];
    NSString *escapedlastname=[self.lastname.text stringByAddingPercentEncodingWithAllowedCharacters:[NSCharacterSet URLHostAllowedCharacterSet]];
    NSString *platform=@"I";
    
    // "api/noa/redirector/?imei="+final_imei+"&email="+enc_email+"&group="+enc_group+"&channel=Android";
    NSLog(@"Token %@",token);
    NSString *mask=[NSString stringWithFormat:@"%@%@%@", @"http://", KANAN_url, @"/api/noa/redirector/?imei=%@&email=%@&group=%@&channel=%@&platform=%@&name=%@&lastname=%@"];
    NSString *urlString = [NSString stringWithFormat:mask,
                           imei,
                           escapedemailNameString,
                           escapedgroupString,
                           token,
                           platform,
                           escapedname,
                           escapedlastname];
    
    //HTTP Basic Authentication
    //NSString *authenticationString = [NSString stringWithFormat:@"%@:%@",@"",@""];
    //NSData *authenticationData = [authenticationString dataUsingEncoding:NSASCIIStringEncoding];
    //NSString *authenticationValue = [authenticationData base64EncodedStringWithOptions:0];
    //Set up your request
    /*
     NSMutableURLRequest *request = [[NSMutableURLRequest alloc] initWithURL:[NSURL URLWithString:@"http://192.168.1.9:5000/api/v1.0/posts/?latitude=19.523689&longitude=-99.239017&email=somosmoviesmexico%40gmail.com&uuid=999999999&device_name=iphone"]];
     */
    NSMutableURLRequest *request = [[NSMutableURLRequest alloc] initWithURL:[NSURL URLWithString:urlString]];
    // Set time out
    //[request setTimeoutInterval:10];
    // Set your user login credentials
    //[request setValue:[NSString stringWithFormat:@"Basic %@", authenticationValue] forHTTPHeaderField:@"Authorization"];
    [request setHTTPMethod:@"GET"];
    [request setValue:@"application/json" forHTTPHeaderField:@"Accept"];
    [request setValue:@"application/json" forHTTPHeaderField:@"Content-Type"];
    // Send your request asynchronously
    NSLog(@"%@",urlString);
    NSLog(@"Just before send asyncronous ");
    NSURLSession *session = [NSURLSession sharedSession];
    [[session dataTaskWithURL:[NSURL URLWithString:urlString]
            completionHandler:^(NSData *responseData,
                                NSURLResponse *responseCode,
                                NSError *responseError)
      {
          // This will get the NSURLResponse into NSHTTPURLResponse format
          NSHTTPURLResponse* httpResponse = (NSHTTPURLResponse*)responseCode;
          // This will Fetch the status code from NSHTTPURLResponse object
          int responseStatusCode = (int)[httpResponse statusCode];
          //Just to make sure, it works or not
          NSLog(@"Status Code :: %d", responseStatusCode);
          NSDictionary *userInfo = [responseError userInfo];
          NSString *errorString = [[userInfo objectForKey:NSUnderlyingErrorKey] localizedDescription];
          NSLog(@"Error string: %@",errorString);
          if ([responseData length] > 0 && responseError == nil)
          {
              id jsonData = [NSJSONSerialization JSONObjectWithData:responseData options:NSJSONReadingAllowFragments error:&responseError];
              NSDictionary *responseDict = jsonData;
              //NSString *kananError = @"";
              NSString *server=@"Location error";
              NSString *port=@"";
              NSString *password=@"";
              NSString *token=[self retrieveToken];
         
              NSDate *currentDate = [[NSDate alloc] init];
              NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
              [dateFormatter setDateFormat:@"yyMMddHHmmss"];
              NSString *utcDateString = [dateFormatter stringFromDate:currentDate];
              NSString *imei;
              NSLog(@"Success redirector");
              if ([[responseDict objectForKey:@"Error"] isEqual:@"none"])
              {
                  NSLog(@"Success");
                  server=@"";
                  port=@"";
                  password=@"1234";
                  
                  NSLog(@"IMEI: %@",imei);
                  NSLog(@"server: %@",server);
                  NSLog(@"port: %@",port);
                  NSString *t=[self.countryCode.text stringByAppendingString:self.phoneNumber.text];
                  NSString *npn2=[t substringFromIndex:1];
                  NSLog(@"NEW PHONE:%@",npn2);
                  CoreData *orm=[[CoreData alloc] init];
                  [orm save:@"valid" : @"YES"];
                  [orm save:@"response1":[responseDict objectForKey:@"response1"]];
                  [orm save:@"response2":[responseDict objectForKey:@"response2"]];
                  [orm save:@"response3":[responseDict objectForKey:@"response3"]];
                  [orm save:@"response4":[responseDict objectForKey:@"response4"]];
                  [orm save:@"response5":[responseDict objectForKey:@"response5"]];
                  [orm save:@"time1":[responseDict objectForKey:@"time1"]];
                  [orm save:@"time2":[responseDict objectForKey:@"time2"]];
                  [orm save:@"time3":[responseDict objectForKey:@"time3"]];
                  [orm save:@"time4":[responseDict objectForKey:@"time4"]];
                  [orm save:@"time5":[responseDict objectForKey:@"time5"]];
                  [orm save:@"follow1":[responseDict objectForKey:@"follow1"]];
                  [orm save:@"follow2":[responseDict objectForKey:@"follow2"]];
                  [orm save:@"follow3":[responseDict objectForKey:@"follow3"]];
                  [orm save:@"follow4":[responseDict objectForKey:@"follow4"]];
                  [orm save:@"follow5":[responseDict objectForKey:@"follow5"]];


                  [self send2server:npn2 latitude:[orm retrieve:@"ilatitude"] longitude:[orm retrieve:@"ilongitude"] speed:speed imei:npn2 group:@"angel" email:[@"angel@dwim.mx" stringByAddingPercentEncodingWithAllowedCharacters:[NSCharacterSet URLHostAllowedCharacterSet]]];
                  [self updateChannel:npn2 group:@"angel" email:[@"angel@dwim.mx" stringByAddingPercentEncodingWithAllowedCharacters:[NSCharacterSet URLHostAllowedCharacterSet]] date:utcDateString token:token];
                  dispatch_async(dispatch_get_main_queue(), ^{
                      // UI Update
                  });
                  
              }
              
          }
          
          else if ([responseData length] == 0 && responseError == nil)
          {
              [self senderror:@"Data Error" message:@"Please check credentials or connection"];
              NSLog(@"data error: %@", responseError);
          }
          else if (responseError != nil && responseError.code == NSURLErrorTimedOut)
          {
              [self senderror:@"Data Time Out" message:@"Please check internet is available"];
              NSLog(@"data timeout: %ld", (long)NSURLErrorTimedOut);
          }
          else if (responseError != nil)
          {
              [self senderror:@"Access is denied" message:@"Please check credentials or connection"];
              NSLog(@"data download error: %@",responseError);
          }
      }]
     resume];
    
}
- (NSString *) retrieveToken
{
    NSString *apnToken;
    AppDelegate *appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    
    NSManagedObjectContext *context =
    [appDelegate managedObjectContext];
    
    NSEntityDescription *entityDesc =
    [NSEntityDescription entityForName:@"Settings"
                inManagedObjectContext:context];
    
    NSFetchRequest *request = [[NSFetchRequest alloc] init];
    [request setEntity:entityDesc];
    
    [request setEntity:entityDesc];
    
    NSPredicate *pred = [NSPredicate predicateWithFormat:@"(id = %@)", @"1"];
    
    [request setPredicate:pred];
    
    NSManagedObject *matches = nil;
    
    NSError *error;
    
    NSArray *objects = [context executeFetchRequest:request
                                              error:&error];
    if ([objects count] == 0) {
        NSLog(@"No token");
        apnToken=@"";
    } else {
        matches = objects[0];
        apnToken = [matches valueForKey:@"token"];
    }
    return apnToken;
}

- (void) senderror: (NSString *) error message: (NSString *) message
{
    [self showAlert:error :message];
}

-(void)showAlert:(NSString *)title :(NSString *)body {
    UIAlertController * alert=   [UIAlertController
                                  alertControllerWithTitle:title
                                  message:body
                                  preferredStyle:UIAlertControllerStyleAlert];
    
    UIAlertAction* ok = [UIAlertAction
                         actionWithTitle:@"OK"
                         style:UIAlertActionStyleDefault
                         handler:^(UIAlertAction * action)
                         {
                             [alert dismissViewControllerAnimated:YES completion:nil];
                             
                         }];
    
    [alert addAction:ok];
    [self presentViewController:alert animated:YES completion:nil];
}
// Picker section

- (NSInteger)numberOfComponentsInPickerView:
(UIPickerView *)pickerView
{
    return 1;
}
- (NSInteger)pickerView:(__unused UIPickerView *)pickerView numberOfRowsInComponent:(__unused NSInteger)component
{
    return (NSInteger)[[[self class] countryCodes] count];
}

- (UIView *)pickerView:(__unused UIPickerView *)pickerView viewForRow:(NSInteger)row
          forComponent:(__unused NSInteger)component reusingView:(UIView *)view
{
    if (!view)
    {
        view = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 280, 30)];
        
        UILabel *label = [[UILabel alloc] initWithFrame:CGRectMake(35, 3, 245, 24)];
        label.backgroundColor = [UIColor clearColor];
        label.tag = 1;
        [view addSubview:label];
        
        UIImageView *flagView = [[UIImageView alloc] initWithFrame:CGRectMake(3, 3, 24, 24)];
        flagView.contentMode = UIViewContentModeScaleAspectFit;
        flagView.tag = 2;
        [view addSubview:flagView];
    }
    
    ((UILabel *)[view viewWithTag:1]).text = [[self class] countryNames][(NSUInteger)row];
    NSString *imagePath = [NSString stringWithFormat:@"CountryPicker.bundle/%@", [[self class] countryCodes][(NSUInteger) row]];
    ((UIImageView *)[view viewWithTag:2]).image = [UIImage imageNamed:imagePath];
    
    
    return view;
}

- (void)pickerView:(__unused UIPickerView *)pickerView
      didSelectRow:(__unused NSInteger)row
       inComponent:(__unused NSInteger)component
{
    NSLog(@"row: %ld",(long)row);
    NSString *search=[[self class] countryCodes][row];
    NSLog(@"Search string: %@",search);
    self.countryCode.text=[@"+" stringByAppendingString:[dictCodes objectForKey:search]];
}


+ (NSArray *)countryCodes
{
    static NSArray *_countryCodes = nil;
    if (!_countryCodes)
    {
        _countryCodes = [[[self countryCodesByName] objectsForKeys:[self countryNames] notFoundMarker:@""] copy];
    }
    return _countryCodes;
}

+ (NSArray *)countryNames
{
    static NSArray *_countryNames = nil;
    if (!_countryNames)
    {
        _countryNames = [[[[self countryNamesByCode] allValues] sortedArrayUsingSelector:@selector(caseInsensitiveCompare:)] copy];
    }
    return _countryNames;
}

+ (NSDictionary *)countryNamesByCode
{
    static NSDictionary *_countryNamesByCode = nil;
    if (!_countryNamesByCode)
    {
        NSMutableDictionary *namesByCode = [NSMutableDictionary dictionary];
        for (NSString *code in [NSLocale ISOCountryCodes])
        {
            NSString *countryName = [[NSLocale currentLocale] displayNameForKey:NSLocaleCountryCode value:code];
            
            //workaround for simulator bug
            if (!countryName)
            {
                countryName = [[NSLocale localeWithLocaleIdentifier:@"en_US"] displayNameForKey:NSLocaleCountryCode value:code];
            }
            
            namesByCode[code] = countryName ?: code;
        }
        _countryNamesByCode = [namesByCode copy];
    }
    return _countryNamesByCode;
}

+ (NSDictionary *)countryCodesByName
{
    static NSDictionary *_countryCodesByName = nil;
    if (!_countryCodesByName)
    {
        NSDictionary *countryNamesByCode = [self countryNamesByCode];
        NSMutableDictionary *codesByName = [NSMutableDictionary dictionary];
        for (NSString *code in countryNamesByCode)
        {
            codesByName[countryNamesByCode[code]] = code;
        }
        _countryCodesByName = [codesByName copy];
    }
    return _countryCodesByName;
}
- (IBAction)didExit:(UIButton *)sender {
}
- (IBAction)didExitName:(UITextField *)sender {
}
- (IBAction)didExitLastName:(id)sender {
}
- (IBAction)didExotPhoneNumber:(UITextField *)sender {
}


@end
