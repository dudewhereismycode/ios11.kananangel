//
//  TellFriendViewController.h
//  KANAN angel
//
//  Created by Jorge Macias on 11/8/17.
//  Copyright © 2017 Jorge Macias. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <MessageUI/MessageUI.h>

@interface TellFriendViewController : UIViewController <UITableViewDataSource,UITableViewDelegate,MFMailComposeViewControllerDelegate,MFMessageComposeViewControllerDelegate>

@end
