//
//  Account.m
//  KANAN angel
//
//  Created by Jorge Macias on 10/31/17.
//  Copyright © 2017 Jorge Macias. All rights reserved.
//

#import "Account.h"
#import "Reachability.h"
#import "AppDelegate.h"

@implementation Account{
    int instance_variable;
    NSString* phoneNumber;
}
-(int) suma{
    instance_variable=1;
    return [self number1]+[self number2];
}
- (NSString *) retrieveToken
{
    NSString *apnToken;
    AppDelegate *appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    
    NSManagedObjectContext *context =
    [appDelegate managedObjectContext];
    
    NSEntityDescription *entityDesc =
    [NSEntityDescription entityForName:@"Settings"
                inManagedObjectContext:context];
    
    NSFetchRequest *request = [[NSFetchRequest alloc] init];
    [request setEntity:entityDesc];
    
    [request setEntity:entityDesc];
    
    NSPredicate *pred = [NSPredicate predicateWithFormat:@"(id = %@)", @"1"];
    
    [request setPredicate:pred];
    
    NSManagedObject *matches = nil;
    
    NSError *error;
    
    NSArray *objects = [context executeFetchRequest:request
                                              error:&error];
    if ([objects count] == 0) {
        NSLog(@"No token");
        apnToken=@"";
    } else {
        matches = objects[0];
        apnToken = [matches valueForKey:@"token"];
    }
    return apnToken;
}

- (unsigned long) countItems {
    AppDelegate *appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    
    NSManagedObjectContext *context =
    [appDelegate managedObjectContext];
    
    NSEntityDescription *entityDesc =
    [NSEntityDescription entityForName:@"Settings"
                inManagedObjectContext:context];
    
    NSFetchRequest *request = [[NSFetchRequest alloc] init];
    [request setEntity:entityDesc];
    
    [request setEntity:entityDesc];
    
    NSPredicate *pred = [NSPredicate predicateWithFormat:@"(id = %@)", @"1"];
    
    [request setPredicate:pred];
    
    NSError *error;
    
    NSArray *objects = [context executeFetchRequest:request
                                              error:&error];
    return [objects count];
}

- (void) storeData: (NSString *) email phone: (NSString *) phone_number group: (NSString *) group server: (NSString *) server port: (NSString *) port password: (NSString *) password valid: (NSString *) valid cc: (NSString *) cc pn: (NSString *) pn
{
    unsigned long items;
    items=[self countItems];
    if (items==0)
    {
        AppDelegate *appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
        NSManagedObjectContext *context =
        [appDelegate managedObjectContext];
        NSManagedObject *settings;
        settings = [NSEntityDescription
                    insertNewObjectForEntityForName:@"Settings"
                    inManagedObjectContext:context];
        [settings setValue:@"1" forKey:@"id"];
        [settings setValue:email forKey:@"email"];
        [settings setValue:phone_number forKey:@"phone_number"];
        [settings setValue:cc forKey:@"field1"];
        [settings setValue:pn forKey:@"field2"];
        [settings setValue:group forKey:@"group"];
        [settings setValue:server forKey:@"server"];
        [settings setValue:port forKey:@"port"];
        [settings setValue:password forKey:@"password"];
        [settings setValue:valid forKey:@"valid"];
        NSError *error;
        [context save:&error];
        
    }
    else
    {
        AppDelegate *appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
        
        NSManagedObjectContext *context =
        [appDelegate managedObjectContext];
        
        NSEntityDescription *entityDesc =
        [NSEntityDescription entityForName:@"Settings"
                    inManagedObjectContext:context];
        
        NSFetchRequest *request = [[NSFetchRequest alloc] init];
        [request setEntity:entityDesc];
        
        [request setEntity:entityDesc];
        
        NSPredicate *pred = [NSPredicate predicateWithFormat:@"(id = %@)", @"1"];
        
        [request setPredicate:pred];
        NSError *error;
        
        NSArray *results = [context executeFetchRequest:request
                                                  error:&error];
        
        NSManagedObject* favoritsGrabbed = [results objectAtIndex:0];
        [favoritsGrabbed setValue:email forKey:@"email"];
        [favoritsGrabbed setValue:cc forKey:@"field1"];
        [favoritsGrabbed setValue:pn forKey:@"field2"];
        [favoritsGrabbed setValue:phone_number forKey:@"phone_number"];
        [favoritsGrabbed setValue:group forKey:@"group"];
        [favoritsGrabbed setValue:server forKey:@"server"];
        [favoritsGrabbed setValue:port forKey:@"port"];
        [favoritsGrabbed setValue:password forKey:@"password"];
        [favoritsGrabbed setValue:valid forKey:@"valid"];
        [context save:&error];
        NSLog(@"updated core data");
    }
}


-(int)account:(NSString*)countryCode :(NSString*)phone{
    phoneNumber=[countryCode stringByAppendingString:phone];
    NSLog(@"Phone Number: %@", phoneNumber);
    NSString *token=[self retrieveToken];
    NSLog(@"Token: %@",token);
    // Do any additional setup after loading the view, typically from a nib.
    locationManager = [[CLLocationManager alloc] init];
    locationManager = [[CLLocationManager alloc] init];
    locationManager.delegate = self;
    locationManager.distanceFilter = kCLDistanceFilterNone;
    locationManager.desiredAccuracy = kCLLocationAccuracyBest;
    [locationManager requestWhenInUseAuthorization];
    if ([locationManager respondsToSelector:@selector(requestWhenInUseAuthorization)]) {
        [locationManager requestWhenInUseAuthorization];
    }
    else
    {
        NSLog(@"Location services app permision denied!");
    }

    //[locationManager startUpdatingLocation];
    Reachability *reachability = [Reachability reachabilityForInternetConnection];
    [reachability startNotifier];
    
    NetworkStatus status = [reachability currentReachabilityStatus];
    
    if(status == NotReachable)
    {
        NSLog(@"no internet available!");
        return 1;
    }
    else
    {
        
    }
    return 0;
}
- (void)locationManager:(CLLocationManager *)manager didFailWithError:(NSError *)error
{
    NSLog(@"%@",error);
}
/*
- (void)locationManager:(CLLocationManager *)manager didUpdateToLocation:(CLLocation *)newLocation fromLocation:(CLLocation *)oldLocation {
    */
- (void)locationManager:(CLLocationManager *)manager
     didUpdateLocations:(NSArray *)locations{
    CLLocation *newLocation = [locations lastObject];
    NSLog(@"Location %f %f", newLocation.coordinate.latitude, newLocation.coordinate.longitude);
    //NSString *latitude = [[NSNumber numberWithDouble:newLocation.coordinate.latitude] stringValue];
    ///NSString *longitude = [[NSNumber numberWithDouble:newLocation.coordinate.longitude] stringValue];
    NSString *latitude = [NSString stringWithFormat:@"%.06f", newLocation.coordinate.latitude];
    NSString *longitude = [NSString stringWithFormat:@"%.06f", newLocation.coordinate.longitude];
    NSString *speed = [[NSNumber numberWithDouble:newLocation.speed] stringValue];
    NSLog(@"latitude: %@",latitude);
    NSLog(@"longitude: %@",longitude);
    NSLog(@"speed: %@",speed);

    [locationManager stopUpdatingLocation];
    
    CLGeocoder *reverseGeocoder = [[CLGeocoder alloc] init];
    
    [reverseGeocoder reverseGeocodeLocation:newLocation completionHandler:^(NSArray *placemarks, NSError *error) {
        NSLog(@"Received placemarks: %@", placemarks);
        
        CLPlacemark *myPlacemark = [placemarks objectAtIndex:0];
        NSString *countryCode = myPlacemark.ISOcountryCode;
        NSString *countryName = myPlacemark.country;
        NSString *cityName= myPlacemark.subAdministrativeArea;
        NSLog(@"My country code: %@ and countryName: %@ MyCity: %@", countryCode, countryName, cityName);
    }];
    [locationManager stopUpdatingLocation];
}
@end
