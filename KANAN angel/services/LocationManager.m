//
//  LocationManager.m
//  KANAN angel
//
//  Created by Jorge Macias on 10/30/17.
//  Copyright © 2017 Jorge Macias. All rights reserved.
//

#import "LocationManager.h"
#import <UIKit/UIKit.h>
#import "AppDelegate.h"
#import <UserNotifications/UserNotifications.h>
#import "CoreData.h"

@interface LocationManager () <CLLocationManagerDelegate,NSStreamDelegate>

@end


@implementation LocationManager
@synthesize inputStream, outputStream;

//Class method to make sure the share model is synch across the app
+ (id)sharedManager {
    static id sharedMyModel = nil;
    static dispatch_once_t onceToken;
    
    dispatch_once(&onceToken, ^{
        sharedMyModel = [[self alloc] init];
    });
    
    return sharedMyModel;
}


#pragma mark - CLLocationManager

- (void)startMonitoringLocation {
    if (_anotherLocationManager)
        [_anotherLocationManager stopMonitoringSignificantLocationChanges];
    
    self.anotherLocationManager = [[CLLocationManager alloc]init];
    _anotherLocationManager.delegate = self;
    _anotherLocationManager.desiredAccuracy = kCLLocationAccuracyBestForNavigation;
    _anotherLocationManager.activityType = CLActivityTypeOtherNavigation;
    
    if(IS_OS_8_OR_LATER) {
        [_anotherLocationManager requestAlwaysAuthorization];
    }
    [_anotherLocationManager startMonitoringSignificantLocationChanges];
}

- (void)restartMonitoringLocation {
    [_anotherLocationManager stopMonitoringSignificantLocationChanges];
    
    if (IS_OS_8_OR_LATER) {
        [_anotherLocationManager requestAlwaysAuthorization];
    }
    [_anotherLocationManager startMonitoringSignificantLocationChanges];
}


#pragma mark - CLLocationManager Delegate

- (void)locationManager:(CLLocationManager *)manager didUpdateLocations:(NSArray *)locations{
    
    NSLog(@"locationManager didUpdateLocations: %@",locations);
    
    for (int i = 0; i < locations.count; i++) {
        
        CLLocation * newLocation = [locations objectAtIndex:i];
        CLLocationCoordinate2D theLocation = newLocation.coordinate;
        CLLocationAccuracy theAccuracy = newLocation.horizontalAccuracy;
        
        self.myLocation = theLocation;
        self.myLocationAccuracy = theAccuracy;
        self.speed=newLocation.speed;
    }
    
    [self addLocationToPList:_afterResume];
}



#pragma mark - Plist helper methods

// Below are 3 functions that add location and Application status to PList
// The purpose is to collect location information locally

- (NSString *)appState {
    UIApplication* application = [UIApplication sharedApplication];
    
    NSString * appState;
    if([application applicationState]==UIApplicationStateActive)
        appState = @"UIApplicationStateActive";
    if([application applicationState]==UIApplicationStateBackground)
        appState = @"UIApplicationStateBackground";
    if([application applicationState]==UIApplicationStateInactive)
        appState = @"UIApplicationStateInactive";
    
    return appState;
}

- (void)addResumeLocationToPList {
    
    NSLog(@"addResumeLocationToPList");
    
    NSString * appState = [self appState];
    
    self.myLocationDictInPlist = [[NSMutableDictionary alloc]init];
    [_myLocationDictInPlist setObject:@"UIApplicationLaunchOptionsLocationKey" forKey:@"Resume"];
    [_myLocationDictInPlist setObject:appState forKey:@"AppState"];
    [_myLocationDictInPlist setObject:[NSDate date] forKey:@"Time"];
    
    [self saveLocationsToPlist];
    
}



- (void)addLocationToPList:(BOOL)fromResume {
    NSLog(@"addLocationToPList");
    
    NSString * appState = [self appState];
    
    self.myLocationDictInPlist = [[NSMutableDictionary alloc]init];
    [_myLocationDictInPlist setObject:[NSNumber numberWithDouble:self.myLocation.latitude]  forKey:@"Latitude"];
    [_myLocationDictInPlist setObject:[NSNumber numberWithDouble:self.myLocation.longitude] forKey:@"Longitude"];
    [_myLocationDictInPlist setObject:[NSNumber numberWithDouble:self.myLocationAccuracy] forKey:@"Accuracy"];
    
    [_myLocationDictInPlist setObject:appState forKey:@"AppState"];
    
    if (fromResume) {
        [_myLocationDictInPlist setObject:@"YES" forKey:@"AddFromResume"];
        
    } else {
        [_myLocationDictInPlist setObject:@"NO" forKey:@"AddFromResume"];
    }
    
    [_myLocationDictInPlist setObject:[NSDate date] forKey:@"Time"];
    
    [self saveLocationsToPlist];
    [self send2platform];
    
}

- (void)addApplicationStatusToPList:(NSString*)applicationStatus {
    
    NSLog(@"addApplicationStatusToPList");
    
    NSString * appState = [self appState];
    
    self.myLocationDictInPlist = [[NSMutableDictionary alloc]init];
    [_myLocationDictInPlist setObject:applicationStatus forKey:@"applicationStatus"];
    [_myLocationDictInPlist setObject:appState forKey:@"AppState"];
    [_myLocationDictInPlist setObject:[NSDate date] forKey:@"Time"];
    
    [self saveLocationsToPlist];
    
}

- (void)saveLocationsToPlist {
    NSString *plistName = [NSString stringWithFormat:@"LocationArray.plist"];
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString *docDir = [paths objectAtIndex:0];
    NSString *fullPath = [NSString stringWithFormat:@"%@/%@", docDir, plistName];
    
    NSMutableDictionary *savedProfile = [[NSMutableDictionary alloc] initWithContentsOfFile:fullPath];
    
    if (!savedProfile) {
        savedProfile = [[NSMutableDictionary alloc] init];
        self.myLocationArrayInPlist = [[NSMutableArray alloc]init];
    } else {
        self.myLocationArrayInPlist = [savedProfile objectForKey:@"LocationArray"];
    }
    
    if(_myLocationDictInPlist) {
        [_myLocationArrayInPlist addObject:_myLocationDictInPlist];
        [savedProfile setObject:_myLocationArrayInPlist forKey:@"LocationArray"];
    }
    
    if (![savedProfile writeToFile:fullPath atomically:FALSE]) {
        NSLog(@"Couldn't save LocationArray.plist" );
    }
}

-(void)deleteLocationsToPlist{
    NSString *plistName = [NSString stringWithFormat:@"LocationArray.plist"];
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString *docDir = [paths objectAtIndex:0];
    NSString *fullPath = [NSString stringWithFormat:@"%@/%@", docDir, plistName];
    NSError *error;
    if(![[NSFileManager defaultManager] removeItemAtPath:fullPath error:&error])
    {
        //TODO: Handle/Log error
    }
}

- (void) send2platform
{
    // Send Data to kanan server
    NSString *latitude = [NSString stringWithFormat:@"%f", self.myLocation.latitude];
    NSString *longitude = [NSString stringWithFormat:@"%f", self.myLocation.longitude];
    NSLog(@"latitude: %@",latitude);
    NSLog(@"longitude: %@",longitude);
    NSString *valid = @"";
    NSString *phone_number = @"";
    NSString *port = @"";
    NSString *server = @"";
    NSString *group = @"";
    NSString *email = @"";
    
    AppDelegate *appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    NSManagedObjectContext *context =
    [appDelegate managedObjectContext];
    NSEntityDescription *entityDesc =
    [NSEntityDescription entityForName:@"Settings" inManagedObjectContext:context];
    NSFetchRequest *request = [[NSFetchRequest alloc] init];
    [request setEntity:entityDesc];
    [request setEntity:entityDesc];
    NSPredicate *pred = [NSPredicate predicateWithFormat:@"(id = %@)", @"1"];
    [request setPredicate:pred];
    NSManagedObject *matches = nil;
    NSError *error;
    NSArray *objects = [context executeFetchRequest:request error:&error];
    if ([objects count] != 0) {
        matches = objects[0];
        phone_number = [matches valueForKey:@"phone_number"];
        port = [matches valueForKey:@"port"];
        server = [matches valueForKey:@"server"];
        group = [matches valueForKey:@"group"];
        email = [matches valueForKey:@"email"];
        valid = [matches valueForKey:@"valid"];
        NSString *myspeed;
        int sp;
        sp=(int) self.speed;
        if (sp<0)
            sp=0;
        myspeed=[NSString stringWithFormat:@"%d", sp];
        if ([valid isEqualToString:@"YES"]) {
            [self send2server:latitude longitude:longitude speed:myspeed imei:phone_number group:group email:email];
            NSLog(@"Valid");
            
        }
        else{
            NSLog(@"Not valid");
        }
    }
}

- (void) send2server: (NSString *) latitude longitude:(NSString *) longitude speed: (NSString *) speed imei: (NSString *) imei group: (NSString *) group email: (NSString *) email
{
    CoreData *orm=[[CoreData alloc]init];
    NSString *token=[orm retrieve:@"token"];
    NSString *battery = [[NSNumber numberWithDouble:[self batterylevel]] stringValue];
    NSString *femail = [email stringByAddingPercentEncodingWithAllowedCharacters:[NSCharacterSet URLHostAllowedCharacterSet]];
    NSString *fgroup = [group stringByAddingPercentEncodingWithAllowedCharacters:[NSCharacterSet URLHostAllowedCharacterSet]];
    //NSLog(@"GROUP!!:%@",group);
    NSString *mask=[NSString stringWithFormat:@"%@%@%@", @"http://", KANAN_url, @"/api/noa/location/?imei=%@&email=%@&group=%@&latitude=%@&longitude=%@&speed=%@&battery=%@&channel=%@"];
    NSString *urlString = [NSString stringWithFormat:mask,
                           imei,
                           femail,
                           fgroup,
                           latitude,
                           longitude,
                           speed,
                           battery,
                           token];
    
    NSMutableURLRequest *request = [[NSMutableURLRequest alloc] initWithURL:[NSURL URLWithString:urlString]];
    
    [request setHTTPMethod:@"GET"];
    [request setValue:@"application/json" forHTTPHeaderField:@"Accept"];
    [request setValue:@"application/json" forHTTPHeaderField:@"Content-Type"];
    // Send your request asynchronously
    NSLog(@"%@",urlString);
    NSLog(@"Just before send asyncronous loca manager");
    NSURLSession *session = [NSURLSession sharedSession];
    [[session dataTaskWithURL:[NSURL URLWithString:urlString]
            completionHandler:^(NSData *responseData,
                                NSURLResponse *responseCode,
                                NSError *responseError)
      {
          // handle response
          // This will get the NSURLResponse into NSHTTPURLResponse format
          NSHTTPURLResponse* httpResponse = (NSHTTPURLResponse*)responseCode;
          // This will Fetch the status code from NSHTTPURLResponse object
          int responseStatusCode = (int)[httpResponse statusCode];
          //Just to make sure, it works or not
          NSLog(@"Status Code :: %d", responseStatusCode);
          NSDictionary *userInfo = [responseError userInfo];
          NSString *errorString = [[userInfo objectForKey:NSUnderlyingErrorKey] localizedDescription];
          NSLog(@"Error string: %@",errorString);
          if ([responseData length] > 0 && responseError == nil)
          {
              id jsonData = [NSJSONSerialization JSONObjectWithData:responseData options:NSJSONReadingAllowFragments error:&responseError];
              NSDictionary *responseDict = jsonData;
              NSString *kananError = @"";
              NSLog(@"Success location");
              if ([[responseDict objectForKey:@"Error"] isEqual:@"none"])
              {
                  NSLog(@"Success");
                  kananError=[responseDict objectForKey:@"Error"];
                  NSLog(@"Error: %@",kananError);
              }
              
              else
              {
                  [self storelostlocation4imei:imei email:femail group:fgroup latitude:latitude longitude:longitude speed:speed battery:battery];
                  // Show Local Notification
                  NSDictionary* userInfo= @{@"title": @"Cant reach server(1)",@"body":@"Saving position"};
                  [[NSNotificationCenter defaultCenter] postNotificationName:@"localNotification" object:self userInfo:userInfo];
              }
              
          }
          
          else
          {
              [self storelostlocation4imei:imei email:femail group:fgroup latitude:latitude longitude:longitude speed:speed battery:battery];
              // Show Local Notification
              NSDictionary* userInfo= @{@"title": @"Can't reach server",@"body":@"Saving position"};
              [[NSNotificationCenter defaultCenter] postNotificationName:@"localNotification" object:self userInfo:userInfo];
          }
          
      }] resume];}


-(int) batterylevel
{
    int level;
    UIDevice *myDevice = [UIDevice currentDevice];
    [myDevice setBatteryMonitoringEnabled:YES];
    int state = [myDevice batteryState];
    NSLog(@"battery status: %d",state); // 0 unknown, 1 unplegged, 2 charging, 3 full
    double batLeft = (float)[myDevice batteryLevel] * 100;
    NSLog(@"battery left: %f", batLeft);
    level=(int)batLeft;
    return level;
}



- (void) storelostlocation4imei: (NSString*) imei email: (NSString *) email group: (NSString *) group latitude: (NSString *) latitude longitude: (NSString *) longitude speed: (NSString *) speed battery: (NSString *) battery
{
    NSDate *currentDate = [[NSDate alloc] init];
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    [dateFormatter setTimeZone:[NSTimeZone timeZoneWithAbbreviation:@"UTC"]];
    [dateFormatter setDateFormat:@"yyMMddHHmmss"];
    NSString *utcDateString = [dateFormatter stringFromDate:currentDate];
    AppDelegate *appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    NSManagedObjectContext *context =
    [appDelegate managedObjectContext];
    NSManagedObject *settings;
    settings = [NSEntityDescription
                insertNewObjectForEntityForName:@"Lost"
                inManagedObjectContext:context];
    [settings setValue:@"NO" forKey:@"processed"];
    [settings setValue:imei forKey:@"imei"];
    [settings setValue:email forKey:@"email"];
    [settings setValue:group forKey:@"group"];
    [settings setValue:utcDateString forKey:@"date"];
    [settings setValue:latitude forKey:@"latitude"];
    [settings setValue:longitude forKey:@"longitude"];
    [settings setValue:speed forKey:@"speed"];
    [settings setValue:battery forKey:@"battery"];
    NSError *error;
    [context save:&error];
}
@end

