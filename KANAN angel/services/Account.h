//
//  Account.h
//  KANAN angel
//
//  Created by Jorge Macias on 10/31/17.
//  Copyright © 2017 Jorge Macias. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreLocation/CoreLocation.h>

@interface Account : NSObject <CLLocationManagerDelegate>
{
    CLLocationManager *locationManager;
}
@property int number1;
@property int number2;
-(int) suma;
-(int)account:(NSString*)countryCode :(NSString*)phone;

@end
