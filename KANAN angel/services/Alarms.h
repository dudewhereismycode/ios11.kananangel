//
//  Alarms.h
//  KANAN angel
//
//  Created by Jorge Macias on 11/28/17.
//  Copyright © 2017 Jorge Macias. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface Alarms : NSObject
@property (nonatomic, copy) NSString *time;
@property (nonatomic, copy) NSString *battery;
@property (nonatomic, copy) NSString *latitude;
@property (nonatomic, copy) NSString *longitude;
@end
