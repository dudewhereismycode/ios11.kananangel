//
//  Contacts.h
//  KANAN angel
//
//  Created by Jorge Macias on 11/22/17.
//  Copyright © 2017 Jorge Macias. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface Contacts : NSObject
@property (nonatomic, copy) NSString *name;
@property (nonatomic, copy) NSString *lastName;
@property (nonatomic, copy) NSString *portrait;
@property (nonatomic, copy) NSString *phone;
@end
