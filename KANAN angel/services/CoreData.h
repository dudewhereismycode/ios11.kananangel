//
//  CoreData.h
//  KANAN angel
//
//  Created by Jorge Macias on 11/13/17.
//  Copyright © 2017 Jorge Macias. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface CoreData : NSObject
-(void) save:(NSString*)key :(NSString*)value;
-(NSString *) retrieve:(NSString*) key;
@end
