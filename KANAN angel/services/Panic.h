//
//  Panic.h
//  KANAN angel
//
//  Created by Jorge Macias on 11/29/17.
//  Copyright © 2017 Jorge Macias. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface Panic : NSObject
@property (nonatomic, copy) NSString *battery;
@property (nonatomic, copy) NSString *latitude;
@property (nonatomic, copy) NSString *longitude;
@property (nonatomic, copy) NSString *panic_type;
@property (nonatomic, copy) NSString *time;
@property (nonatomic, copy) NSString *response;
@end
