//
//  LocationServiceSat.h
//  KANAN angel
//
//  Created by Jorge Macias on 10/30/17.
//  Copyright © 2017 Jorge Macias. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreLocation/CoreLocation.h>

@interface LocationServiceSat : NSObject <CLLocationManagerDelegate>

+(LocationServiceSat *) sharedInstance;

@property (strong, nonatomic) CLLocationManager *locationManager;
@property (strong, nonatomic) CLLocation *currentLocation;

- (void)startUpdatingLocation;
- (void)stopUpdatingLocation;

@end
