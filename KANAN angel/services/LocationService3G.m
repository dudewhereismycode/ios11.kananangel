//
//  LocationService3G.m
//  KANAN angel
//
//  Created by Jorge Macias on 10/30/17.
//  Copyright © 2017 Jorge Macias. All rights reserved.
//

#import "LocationService3G.h"
@implementation LocationService3G

+(LocationService3G *) sharedInstance
{
    static LocationService3G *instance = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        instance = [[self alloc]init];
    });
    return instance;
}

- (id)init {
    self = [super init];
    if(self != nil) {
        self.locationManager = [[CLLocationManager alloc] init];
        self.locationManager.desiredAccuracy = kCLLocationAccuracyKilometer;
        // self.locationManager.distanceFilter = 100; // meters
        /*
         GPS - kCLLocationAccuracyBestForNavigation; gyroscope correction
         GPS - kCLLocationAccuracyBest;
         GPS - kCLLocationAccuracyNearestTenMeters;
         WiFi (or GPS in rural area) - kCLLocationAccuracyHundredMeters;
         Cell Tower - kCLLocationAccuracyKilometer;
         Cell Tower - kCLLocationAccuracyThreeKilometers;
         */
        self.locationManager.delegate = self;
        [self.locationManager requestAlwaysAuthorization];
        [self.locationManager startUpdatingLocation];
    }
    return self;
}

- (void)startUpdatingLocation
{
    NSLog(@"Starting location updates");
    [self.locationManager startUpdatingLocation];
}
- (void)stopUpdatingLocation
{
    NSLog(@"Stopping location updates");
    [self.locationManager stopUpdatingLocation];
}
- (void)locationManager:(CLLocationManager *)manager
       didFailWithError:(NSError *)error
{
    NSLog(@"Location service failed with error %@", error);
}

- (void)locationManager:(CLLocationManager *)manager
     didUpdateLocations:(NSArray*)locations
{
    CLLocation *location = [locations lastObject];
    //NSLog(@"3G Latitude %+.6f, Longitude %+.6f\n",          location.coordinate.latitude,          location.coordinate.longitude);
    self.currentLocation = location;
}
@end


