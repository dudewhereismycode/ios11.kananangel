//
//  PositionsViewController.h
//  KANAN angel
//
//  Created by Jorge Macias on 11/28/17.
//  Copyright © 2017 Jorge Macias. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface PositionsViewController : UIViewController <UINavigationControllerDelegate>
@property (nonatomic, assign) NSString *latitude;
@property (nonatomic, assign) NSString *longitude;
@property (nonatomic, assign) NSString *maptitle;
@end
