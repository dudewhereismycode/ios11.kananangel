//
//  EnableAccountViewController.h
//  KANAN angel
//
//  Created by Jorge Macias on 11/8/17.
//  Copyright © 2017 Jorge Macias. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface EnableAccountViewController : UIViewController <UINavigationControllerDelegate>
@property (nonatomic, assign) NSString *phoneNumber;
@property (nonatomic, assign) NSString *userName;
@property (nonatomic, assign) NSString *button;
@end
