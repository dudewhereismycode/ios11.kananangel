//
//  BeaconScanViewController.h
//  KANAN angel
//
//  Created by Jorge Macias on 11/3/17.
//  Copyright © 2017 Jorge Macias. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <BeaconSDK/BeaconSDK.h>

@interface BeaconScanViewController : UIViewController <UITableViewDataSource,UITableViewDelegate,CBCentralManagerDelegate>
@end
