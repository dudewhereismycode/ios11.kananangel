//
//  AccountViewController.m
//  KANAN angel
//
//  Created by Jorge Macias on 11/8/17.
//  Copyright © 2017 Jorge Macias. All rights reserved.
//

#import "AccountViewController.h"
#import "AppDelegate.h"
#import "CoreData.h"

@interface AccountViewController ()
@property (weak, nonatomic) IBOutlet UITextField *accountName;
@property (weak, nonatomic) IBOutlet UITextField *accountLastName;
@property (weak, nonatomic) IBOutlet UISwitch *alarmSwitch;
@property (weak, nonatomic) IBOutlet UILabel *phoneNumber;

@property (weak, nonatomic) IBOutlet NSString *kananError;
@property (weak, nonatomic) IBOutlet NSString *kananMessage;


@end

@implementation AccountViewController
{
    int switch_state;
}


- (void)viewDidLoad {
    [super viewDidLoad];
    NSUserDefaults *preferences = [NSUserDefaults standardUserDefaults];
    
    NSString *currentLevelKey = @"currentlevel";
    
    if ([preferences objectForKey:currentLevelKey] == nil)
    {
        //  Doesn't exist.
        [self.alarmSwitch setOn:NO animated:YES];
        switch_state=0;
        NSUserDefaults *preferences = [NSUserDefaults standardUserDefaults];
        
        NSString *currentLevelKey = @"currentlevel";
        [preferences setInteger:switch_state forKey:currentLevelKey];
        //  Save to disk
        const BOOL didSave = [preferences synchronize];
        
        if (!didSave)
        {
            //  Couldn't save (I've never seen this happen in real world testing)
        }
    }
    else
    {
        //  Get current level
        switch_state = (int)[preferences integerForKey:currentLevelKey];
        if (switch_state==1){
            [self.alarmSwitch setOn:YES animated:YES];
        }
        else{
            [self.alarmSwitch setOn:NO animated:YES];
        }
        
    }
    // Do any additional setup after loading the view.
}
- (void)viewWillAppear:(BOOL)animated{
    self.phoneNumber.text=[self getphonenumber];
    self.accountName.placeholder = [self retrieve:@"name"];
    self.accountLastName.placeholder = [self retrieve:@"lastname"];
    self.accountName.text = [self retrieve:@"name"];
    self.accountLastName.text = [self retrieve:@"lastname"];

}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
- (IBAction)updateButtonTapped:(UIButton *)sender {
    [self updateAccount];
    [self showAlert:@"Message" :@"Account updated successfully"];
}
-(void)eraseData{
    AppDelegate *appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    
    NSManagedObjectContext *context =
    [appDelegate managedObjectContext];
    
    NSEntityDescription *entityDesc =
    [NSEntityDescription entityForName:@"Settings"
                inManagedObjectContext:context];
    
    NSFetchRequest *request = [[NSFetchRequest alloc] init];
    [request setEntity:entityDesc];
    
    [request setEntity:entityDesc];
    
    NSPredicate *pred = [NSPredicate predicateWithFormat:@"(id = %@)", @"1"];
    
    [request setPredicate:pred];
    NSError *error;
    
    NSArray *results = [context executeFetchRequest:request
                                              error:&error];

    NSManagedObject* favoritsGrabbed = [results objectAtIndex:0];
    [favoritsGrabbed setValue:@"" forKey:@"phone_number"];
    [favoritsGrabbed setValue:@"" forKey:@"email"];
    [favoritsGrabbed setValue:@"" forKey:@"group"];
    [favoritsGrabbed setValue:@"" forKey:@"name"];
    [favoritsGrabbed setValue:@"" forKey:@"lastname"];
    [favoritsGrabbed setValue:@"NO" forKey:@"valid"];
    [favoritsGrabbed setValue:@"" forKey:@"telephony_cc"];
    [favoritsGrabbed setValue:@"" forKey:@"token"];
    [favoritsGrabbed setValue:@"" forKey:@"iuuid"];
    [favoritsGrabbed setValue:@"" forKey:@"country"];
    [favoritsGrabbed setValue:@"" forKey:@"blepanicstate"];
    [context save:&error];
}
-(void) deleteAccount{
    //http://app.dwim.mx/api/noa/deleteAccount?myphone=525543513556

    NSString *mask=[NSString stringWithFormat:@"%@%@%@", @"http://", KANAN_url, @"/api/noa/deleteAccount?myphone=%@"];
    NSString *urlString = [NSString stringWithFormat:mask,
                           [self getphonenumber]
                           ];
    
    NSLog(@"Just before send asyncronous ");
    
    NSMutableURLRequest *request = [[NSMutableURLRequest alloc] initWithURL:[NSURL URLWithString:urlString]];
    [request setHTTPMethod:@"GET"];
    [request setValue:@"application/json" forHTTPHeaderField:@"Accept"];
    [request setValue:@"application/json" forHTTPHeaderField:@"Content-Type"];
    NSURLSession *session = [NSURLSession sharedSession];
    [[session dataTaskWithURL:[NSURL URLWithString:urlString]
            completionHandler:^(NSData *responseData,
                                NSURLResponse *responseCode,
                                NSError *responseError)
      {
          // This will get the NSURLResponse into NSHTTPURLResponse format
          NSHTTPURLResponse* httpResponse = (NSHTTPURLResponse*)responseCode;
          // This will Fetch the status code from NSHTTPURLResponse object
          int responseStatusCode = (int)[httpResponse statusCode];
          //Just to make sure, it works or not
          NSLog(@"Status Code :: %d", responseStatusCode);
          NSDictionary *userInfo = [responseError userInfo];
          NSString *errorString = [[userInfo objectForKey:NSUnderlyingErrorKey] localizedDescription];
          NSLog(@"Error string: %@",errorString);
          if ([responseData length] > 0 && responseError == nil)
          {
              id jsonData = [NSJSONSerialization JSONObjectWithData:responseData options:NSJSONReadingAllowFragments error:&responseError];
              NSDictionary *responseDict = jsonData;
              //NSString *kananError = @"";
              //NSString *kananMessage = @"Distributor";
              NSString *title=@"Error1";
              NSLog(@"sucess reading");
              dispatch_async(dispatch_get_main_queue(), ^{
                  // UI Update
                  if ([responseDict objectForKey:@"Error"])
                  {
                      self.kananError = [responseDict objectForKey:@"Error"];
                      if ([responseDict objectForKey:@"message"])
                      {
                          self.kananMessage = [responseDict objectForKey:@"message"];
                      }
                      if ([[responseDict objectForKey:@"Error"] isEqual:@"none"])
                      {
                          [self eraseData];
                          exit(0);
                      }
                      
                      else
                      {
                          [self showAlert:title :self.kananMessage];
                      }
                      
                  }
              });
              
          }
          
          else if ([responseData length] == 0 && responseError == nil)
          {
              
              [self showAlert: @"Data Error" :@"Please check credentials or connection"];
              NSLog(@"data error: %@", responseError);
          }
          else if (responseError != nil && responseError.code == NSURLErrorTimedOut)
          {
              [self showAlert:@"Data Time Out" :@"Please check internet is available"];
              NSLog(@"data timeout: %ld", (long)NSURLErrorTimedOut);
          }
          else if (responseError != nil)
          {
              [self showAlert:@"Access is denied" :@"Please check credentials or connection"];
              NSLog(@"data download error: %@",responseError);
          }
          
          
          
      }]
     resume];
}
- (IBAction)deleteMyAccountButtonTapped:(UIButton *)sender {

    UIAlertController * alert=[UIAlertController alertControllerWithTitle:@"Alert!!"
                                                                  message:@"Do you want to erase your account?"
                                                           preferredStyle:UIAlertControllerStyleAlert];
    
    UIAlertAction* yesButton = [UIAlertAction actionWithTitle:@"Yes"
                                                        style:UIAlertActionStyleDefault
                                                      handler:^(UIAlertAction * action)
                                {
                                    [self deleteAccount];
                                    NSLog(@"you pressed Yes button");
                                    
                                }];
    
    UIAlertAction* noButton = [UIAlertAction actionWithTitle:@"No"
                                                       style:UIAlertActionStyleDefault
                                                     handler:^(UIAlertAction * action)
                               {
                                   
                                   /** What we write here???????? **/
                                    NSLog(@"you pressed No button");
                               }];
    
    
    [alert addAction:yesButton];
    [alert addAction:noButton];
    
    [self presentViewController:alert animated:YES completion:nil];


   // [self showAlert:@"Alert" :@"Not implemented yet"];
}
- (IBAction)switchToggled:(UISwitch *)sender {
    UISwitch *mySwitch = (UISwitch *)sender;
    if ([mySwitch isOn]) {
        NSLog(@"its on!");
        NSUserDefaults *preferences = [NSUserDefaults standardUserDefaults];
        
        NSString *currentLevelKey = @"currentlevel";
        
        [preferences setInteger:1 forKey:currentLevelKey];
        
        //  Save to disk
        const BOOL didSave = [preferences synchronize];
        
        if (!didSave)
        {
            //  Couldn't save (I've never seen this happen in real world testing)
        }
    } else {
        NSUserDefaults *preferences = [NSUserDefaults standardUserDefaults];
        
        NSString *currentLevelKey = @"currentlevel";
        
        [preferences setInteger:0 forKey:currentLevelKey];
        
        //  Save to disk
        const BOOL didSave = [preferences synchronize];
        
        if (!didSave)
        {
            //  Couldn't save (I've never seen this happen in real world testing)
        }
        NSLog(@"its off!");
    }
}
-(void) updateAccount{
    //http://app.dwim.mx/api/noa/updateAccountName/?myphone=525543513556&name=Maria&lastname=Lacayo

    NSString *mask=[NSString stringWithFormat:@"%@%@%@", @"http://", KANAN_url, @"/api/noa/updateAccountName?myphone=%@&name=%@&lastname=%@"];
    
    NSString *urlString = [NSString stringWithFormat:mask,
                           [self getphonenumber],
                           [self.accountName.text stringByAddingPercentEncodingWithAllowedCharacters:[NSCharacterSet URLHostAllowedCharacterSet]],
                           [self.accountLastName.text stringByAddingPercentEncodingWithAllowedCharacters:[NSCharacterSet URLHostAllowedCharacterSet]]
                          ];
    
    NSLog(@"Just before send asyncronous ");

    NSMutableURLRequest *request = [[NSMutableURLRequest alloc] initWithURL:[NSURL URLWithString:urlString]];
    [request setHTTPMethod:@"GET"];
    [request setValue:@"application/json" forHTTPHeaderField:@"Accept"];
    [request setValue:@"application/json" forHTTPHeaderField:@"Content-Type"];
    NSURLSession *session = [NSURLSession sharedSession];
    [[session dataTaskWithURL:[NSURL URLWithString:urlString]
            completionHandler:^(NSData *responseData,
                                NSURLResponse *responseCode,
                                NSError *responseError)
      {
          // This will get the NSURLResponse into NSHTTPURLResponse format
          NSHTTPURLResponse* httpResponse = (NSHTTPURLResponse*)responseCode;
          // This will Fetch the status code from NSHTTPURLResponse object
          int responseStatusCode = (int)[httpResponse statusCode];
          //Just to make sure, it works or not
          NSLog(@"Status Code :: %d", responseStatusCode);
          NSDictionary *userInfo = [responseError userInfo];
          NSString *errorString = [[userInfo objectForKey:NSUnderlyingErrorKey] localizedDescription];
          NSLog(@"Error string: %@",errorString);
          if ([responseData length] > 0 && responseError == nil)
          {
              id jsonData = [NSJSONSerialization JSONObjectWithData:responseData options:NSJSONReadingAllowFragments error:&responseError];
              NSDictionary *responseDict = jsonData;
              //NSString *kananError = @"";
              //NSString *kananMessage = @"Distributor";
              NSString *title=@"Error1";
              NSLog(@"sucess reading");
              dispatch_async(dispatch_get_main_queue(), ^{
                  // UI Update
                  if ([responseDict objectForKey:@"Error"])
                  {
                      self.kananError = [responseDict objectForKey:@"Error"];
                      if ([responseDict objectForKey:@"message"])
                      {
                          self.kananMessage = [responseDict objectForKey:@"message"];
                      }
                      if ([[responseDict objectForKey:@"Error"] isEqual:@"none"])
                      {
                          
                          [self save:@"name" : self.accountName.text];
                          [self save:@"lastname" : self.accountLastName.text];
                      }
                      
                      else
                      {
                          [self showAlert:title :self.kananMessage];
                      }
                      
                  }
              });
              
          }
          
          else if ([responseData length] == 0 && responseError == nil)
          {
 
              [self showAlert: @"Data Error" :@"Please check credentials or connection"];
              NSLog(@"data error: %@", responseError);
          }
          else if (responseError != nil && responseError.code == NSURLErrorTimedOut)
          {
              [self showAlert:@"Data Time Out" :@"Please check internet is available"];
              NSLog(@"data timeout: %ld", (long)NSURLErrorTimedOut);
          }
          else if (responseError != nil)
          {
              [self showAlert:@"Access is denied" :@"Please check credentials or connection"];
              NSLog(@"data download error: %@",responseError);
          }
          
          
          
      }]
     resume];
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/
-(void)showAlert:(NSString *)title :(NSString *)body {
    UIAlertController * alert=   [UIAlertController
                                  alertControllerWithTitle:title
                                  message:body
                                  preferredStyle:UIAlertControllerStyleAlert];
    
    UIAlertAction* ok = [UIAlertAction
                         actionWithTitle:@"OK"
                         style:UIAlertActionStyleDefault
                         handler:^(UIAlertAction * action)
                         {
                             [alert dismissViewControllerAnimated:YES completion:nil];
                             
                         }];
    
    [alert addAction:ok];
    [self presentViewController:alert animated:YES completion:nil];
}
-(NSString *) getphonenumber{
    CoreData *orm=[[CoreData alloc] init];
    return     [orm retrieve:@"phone_number"];

}
-(void) save:(NSString*)key :(NSString*)value{
    AppDelegate *appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    
    NSManagedObjectContext *context =
    [appDelegate managedObjectContext];
    
    NSEntityDescription *entityDesc =
    [NSEntityDescription entityForName:@"Settings"
                inManagedObjectContext:context];
    
    NSFetchRequest *request = [[NSFetchRequest alloc] init];
    [request setEntity:entityDesc];
    
    [request setEntity:entityDesc];
    
    NSPredicate *pred = [NSPredicate predicateWithFormat:@"(id = %@)", @"1"];
    
    [request setPredicate:pred];
    NSError *error;
    
    NSArray *results = [context executeFetchRequest:request
                                              error:&error];

    NSManagedObject* favoritsGrabbed = [results objectAtIndex:0];
    [favoritsGrabbed setValue:value forKey:key];
    [context save:&error];
    NSLog(@"updated core data");
}


- (NSString *) retrieve:(NSString*) key
{
    NSString *apnToken;
    AppDelegate *appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    
    NSManagedObjectContext *context =
    [appDelegate managedObjectContext];
    
    NSEntityDescription *entityDesc =
    [NSEntityDescription entityForName:@"Settings"
                inManagedObjectContext:context];
    
    NSFetchRequest *request = [[NSFetchRequest alloc] init];
    [request setEntity:entityDesc];
    
    [request setEntity:entityDesc];
    
    NSPredicate *pred = [NSPredicate predicateWithFormat:@"(id = %@)", @"1"];
    
    [request setPredicate:pred];
    
    NSManagedObject *matches = nil;
    
    NSError *error;
    
    NSArray *objects = [context executeFetchRequest:request
                                              error:&error];
    if ([objects count] == 0) {
        NSLog(@"No token");
        apnToken=@"";
    } else {
        matches = objects[0];
        apnToken = [matches valueForKey:key];
    }
    return apnToken;
}
@end
