//
//  AppDelegate.h
//  KANAN angel
//
//  Created by Jorge Macias on 10/30/17.
//  Copyright © 2017 Jorge Macias. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <CoreData/CoreData.h>
#import "LocationManager.h"
#import <CoreLocation/CoreLocation.h>
#import <UserNotifications/UserNotifications.h>

typedef void(^BackgroundCompletionHandler)(UIBackgroundFetchResult);

@interface AppDelegate : UIResponder <UIApplicationDelegate,CLLocationManagerDelegate,UIAlertViewDelegate,NSStreamDelegate,UNUserNotificationCenterDelegate>
{
    BackgroundCompletionHandler _backgroundCompletionHandler;
    int notificationInProgress;
    CLLocationManager *locationManager;
    NSInputStream    *inputStream;
    NSOutputStream    *outputStream;
}

//base
@property (strong, nonatomic) UIWindow *window;
@property (readonly, strong) NSPersistentContainer *persistentContainer;
@property (strong, nonatomic) NSString *strDeviceToken;
//Added
@property (nonatomic) CLLocationCoordinate2D myWIFILocation;
@property (nonatomic) CLLocationCoordinate2D my3GLocation;
@property (nonatomic) CLLocationCoordinate2D mySatLocation;
@property (nonatomic) CLLocationCoordinate2D myLocation;
@property (nonatomic) double speed;
@property (nonatomic) CLLocationAccuracy myHorizontalLocationAccuracy;
@property (nonatomic) CLLocationAccuracy myVerticalLocationAccuracy;
@property (nonatomic) CLLocationAccuracy myWIFILocationAccuracy;
@property (nonatomic) CLLocationAccuracy my3GLocationAccuracy;
@property (nonatomic) CLLocationAccuracy mySatLocationAccuracy;
@property (nonatomic, retain) NSInputStream *inputStream;
@property (nonatomic, retain) NSOutputStream *outputStream;
@property (strong,nonatomic) LocationManager * shareModel;

@property (readonly, strong, nonatomic) NSManagedObjectContext *managedObjectContext;
@property (readonly, strong, nonatomic) NSManagedObjectModel *managedObjectModel;
@property (readonly, strong, nonatomic) NSPersistentStoreCoordinator *persistentStoreCoordinator;

extern NSString *const KANAN_url;
- (void)saveContext;
- (NSURL *)applicationDocumentsDirectory;

@end


