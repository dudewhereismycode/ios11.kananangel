//
//  ContactsViewController.h
//  KANAN angel
//
//  Created by Jorge Macias on 11/3/17.
//  Copyright © 2017 Jorge Macias. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "EnableAccountViewController.h"

@interface ContactsViewController : UIViewController <UITableViewDataSource,UITableViewDelegate>
@property (strong,nonatomic) EnableAccountViewController *enableViewController;
@end
