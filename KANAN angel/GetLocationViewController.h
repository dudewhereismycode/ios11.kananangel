//
//  GetLocationViewController.h
//  KANAN angel
//
//  Created by Jorge Macias on 11/1/17.
//  Copyright © 2017 Jorge Macias. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <CoreLocation/CoreLocation.h>
#import  "GetLocationViewController.h"

@interface GetLocationViewController : UIViewController
@property (strong,nonatomic) GetLocationViewController *locationViewController;

@end
