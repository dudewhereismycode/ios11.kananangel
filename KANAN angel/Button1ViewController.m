//
//  Button1ViewController.m
//  KANAN angel
//
//  Created by Jorge Macias on 11/22/17.
//  Copyright © 2017 Jorge Macias. All rights reserved.
//

#import "Button1ViewController.h"
#import "Contacts.h"
#import "AppDelegate.h"
#import "CoreData.h"

@interface Button1ViewController ()
@property (weak, nonatomic) IBOutlet UITableView *tableViewContactData;
@property (weak, nonatomic) IBOutlet UISegmentedControl *whichButton;
@property (weak, nonatomic) NSString *kananError;
@property (weak, nonatomic) NSString *kananMessage;
@end

@implementation Button1ViewController{
    NSMutableArray *tableData;
    long  index;
    NSString *selectedButton;

        NSString *phone;
        NSString *label;
        NSString *fullName;
        NSString *firstName;
        NSString *lastName;
 
}
@synthesize whichButton;

- (void)viewDidLoad {
    [super viewDidLoad];

   // tableData =  [[NSMutableArray alloc] init];

   
}
-(void) viewWillAppear:(BOOL)animated{
    //CoreData *orm=[[CoreData alloc]init];
    //[orm save:@"passbutton" :@"1"];
     [self retrieveContacts];
}
-(NSString *) getphonenumber{
    CoreData *orm=[[CoreData alloc] init];
    return [orm retrieve:@"phone_number"];
}
-(void)showAlert:(NSString *)title :(NSString *)body {
    UIAlertController * alert=   [UIAlertController
                                  alertControllerWithTitle:title
                                  message:body
                                  preferredStyle:UIAlertControllerStyleAlert];
    
    UIAlertAction* ok = [UIAlertAction
                         actionWithTitle:@"OK"
                         style:UIAlertActionStyleDefault
                         handler:^(UIAlertAction * action)
                         {
                             [alert dismissViewControllerAnimated:YES completion:nil];
                             
                         }];
    
    [alert addAction:ok];
    [self presentViewController:alert animated:YES completion:nil];
}
- (IBAction)buttonSelectorTapped:(UISegmentedControl *)sender {
    //CoreData *orm=[[CoreData alloc]init];
    //[orm save:@"passbutton" :[[NSString alloc] initWithFormat:@"%ld", whichButton.selectedSegmentIndex+1]];
      [self retrieveContacts];
}
-(void) retrieveContacts{
    tableData =  [[NSMutableArray alloc] init];
    NSString *button= [[NSString alloc] initWithFormat:@"%ld", whichButton.selectedSegmentIndex+1];
    
    NSString *mask=[NSString stringWithFormat:@"%@%@%@", @"http://", KANAN_url, @"/api/noa/listAngels?myphone=%@&button=%@"];
    NSString *urlString = [NSString stringWithFormat:mask,
                           [self getphonenumber],
                           button
                           ];
    
    NSLog(@"URL: %@",urlString);
    
    NSMutableURLRequest *request = [[NSMutableURLRequest alloc] initWithURL:[NSURL URLWithString:urlString]];
    [request setHTTPMethod:@"GET"];
    [request setValue:@"application/json" forHTTPHeaderField:@"Accept"];
    [request setValue:@"application/json" forHTTPHeaderField:@"Content-Type"];
    NSURLSession *session = [NSURLSession sharedSession];
    [[session dataTaskWithURL:[NSURL URLWithString:urlString]
            completionHandler:^(NSData *responseData,
                                NSURLResponse *responseCode,
                                NSError *responseError)
      {
          // This will get the NSURLResponse into NSHTTPURLResponse format
          NSHTTPURLResponse* httpResponse = (NSHTTPURLResponse*)responseCode;
          // This will Fetch the status code from NSHTTPURLResponse object
          int responseStatusCode = (int)[httpResponse statusCode];
          //Just to make sure, it works or not
          NSLog(@"Status Code :: %d", responseStatusCode);
          NSDictionary *userInfo = [responseError userInfo];
          NSString *errorString = [[userInfo objectForKey:NSUnderlyingErrorKey] localizedDescription];
          NSLog(@"Error string: %@",errorString);
          if ([responseData length] > 0 && responseError == nil)
          {
              id jsonData = [NSJSONSerialization JSONObjectWithData:responseData options:NSJSONReadingAllowFragments error:&responseError];
              NSDictionary *responseDict = jsonData;
              //NSString *kananError = @"";
              //NSString *kananMessage = @"Distributor";
              NSString *title=@"Alert!";
              NSLog(@"sucess reading");
              dispatch_async(dispatch_get_main_queue(), ^{
                  // UI Update
                  if ([responseDict objectForKey:@"Error"])
                  {
                      
                      self.kananError = [responseDict objectForKey:@"Error"];
                      
                      if ([[responseDict objectForKey:@"Error"] isEqual:@"none"])
                      {
                          NSMutableArray *resultArray = [responseDict objectForKeyedSubscript:@"myangels"];
                          Contacts *cell = [[Contacts alloc]init];
            

                          for(NSObject *obj in resultArray){
                              NSLog(@"Name: %@", [obj valueForKey:@"name"]);
                               NSLog(@"Last Name: %@", [obj valueForKey:@"lastname"]);
                              cell=[[Contacts alloc]init];
                              cell.name=[obj valueForKey:@"name"];
                              cell.lastName= [obj valueForKey:@"lastname"];;
                              cell.phone=[obj valueForKey:@"phone"];
                              cell.portrait=@"users.png";
                              [self->tableData addObject:cell];
                          }
                          
                         [self.tableViewContactData reloadData];
                      }
                      
                      else
                      {
                          [self showAlert:title : [responseDict objectForKey:@"message"]];
                      }
                      
                  }
              });
              
          }
          
          else if ([responseData length] == 0 && responseError == nil)
          {
              
              [self showAlert: @"Data Error" :@"Please check credentials or connection"];
              NSLog(@"data error: %@", responseError);
          }
          else if (responseError != nil && responseError.code == NSURLErrorTimedOut)
          {
              [self showAlert:@"Data Time Out" :@"Please check internet is available"];
              NSLog(@"data timeout: %ld", (long)NSURLErrorTimedOut);
          }
          else if (responseError != nil)
          {
              [self showAlert:@"Access is denied" :@"Please check credentials or connection"];
              NSLog(@"data download error: %@",responseError);
          }
          
          
          
      }]
     resume];
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
/*
- (IBAction)doneButtonTapped:(UIBarButtonItem *)sender {
    [self dismissViewControllerAnimated:NO completion:nil];
}
*/

// Table View

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    // Number of rows is the number of time zones in the region for the specified section.
    return [tableData count];
}
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    static NSString *simpleTableIdentifier = @"SimpleTableItem";
    
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:simpleTableIdentifier];
    
    if (cell == nil) {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleSubtitle reuseIdentifier:simpleTableIdentifier];
    }
    Contacts *mycell = [[Contacts alloc]init];
    mycell=[tableData objectAtIndex:indexPath.row];
    cell.textLabel.text = mycell.name;
    cell.detailTextLabel.text = mycell.lastName;
    cell.imageView.image = [UIImage imageNamed:mycell.portrait];
    cell.textLabel.textAlignment = NSTextAlignmentLeft;
    //cell.accessoryType = UITableViewCellAccessoryDisclosureIndicator;
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    
    /*
     UIButton *button = [UIButton buttonWithType:UIButtonTypeCustom];
     [button addTarget:self
     action:@selector(myCustomFunction:)
     forControlEvents:UIControlEventTouchUpInside];
     [button setTitle:@">" forState:UIControlStateNormal];
     button.frame = CGRectMake(80.0, 210.0, 160.0, 40.0);
     [button setTitleColor:[UIColor blueColor] forState:UIControlStateNormal];
     
     cell.accessoryView = button;
     */
    return cell;
}
- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (editingStyle == UITableViewCellEditingStyleDelete)
    {
        NSString *button= [[NSString alloc] initWithFormat:@"%ld", whichButton.selectedSegmentIndex+1];
        Contacts *mycell = [[Contacts alloc]init];
        mycell=[tableData objectAtIndex:indexPath.row];
        NSLog(@"phone: %@",mycell.phone);
        NSString *mask=[NSString stringWithFormat:@"%@%@%@", @"http://", KANAN_url, @"/api/noa/setAngel?myphone=%@&angel_phone=%@&button=%@&enable=%@"];
        NSString *urlString = [NSString stringWithFormat:mask,
                               [self getphonenumber],
                               mycell.phone,
                               button,
                               @"False"
                               ];
        
        NSLog(@"URL: %@",urlString);
        
        
        NSMutableURLRequest *request = [[NSMutableURLRequest alloc] initWithURL:[NSURL URLWithString:urlString]];
        [request setHTTPMethod:@"GET"];
        [request setValue:@"application/json" forHTTPHeaderField:@"Accept"];
        [request setValue:@"application/json" forHTTPHeaderField:@"Content-Type"];
        NSURLSession *session = [NSURLSession sharedSession];
        [[session dataTaskWithURL:[NSURL URLWithString:urlString]
                completionHandler:^(NSData *responseData,
                                    NSURLResponse *responseCode,
                                    NSError *responseError)
          {
              // This will get the NSURLResponse into NSHTTPURLResponse format
              NSHTTPURLResponse* httpResponse = (NSHTTPURLResponse*)responseCode;
              // This will Fetch the status code from NSHTTPURLResponse object
              int responseStatusCode = (int)[httpResponse statusCode];
              //Just to make sure, it works or not
              NSLog(@"Status Code :: %d", responseStatusCode);
              NSDictionary *userInfo = [responseError userInfo];
              NSString *errorString = [[userInfo objectForKey:NSUnderlyingErrorKey] localizedDescription];
              NSLog(@"Error string: %@",errorString);
              if ([responseData length] > 0 && responseError == nil)
              {
                  id jsonData = [NSJSONSerialization JSONObjectWithData:responseData options:NSJSONReadingAllowFragments error:&responseError];
                  NSDictionary *responseDict = jsonData;
                  //NSString *kananError = @"";
                  //NSString *kananMessage = @"Distributor";
                  NSString *title=@"Error1";
                  NSLog(@"sucess reading");
                  dispatch_async(dispatch_get_main_queue(), ^{
                      // UI Update
                      if ([responseDict objectForKey:@"Error"])
                      {
                          self.kananError = [responseDict objectForKey:@"Error"];
                          if ([responseDict objectForKey:@"message"])
                          {
                              self.kananMessage = [responseDict objectForKey:@"message"];
                          }
                          if ([[responseDict objectForKey:@"Error"] isEqual:@"none"])
                          {
                              [self->tableData removeObjectAtIndex:indexPath.row];
                              [self.tableViewContactData reloadData];
                              [self showAlert:@"Message" : @"Data saved"];
                              
                          }
                          
                          else
                          {
                              [self showAlert:title :self.kananMessage];
                          }
                          
                      }
                  });
                  
              }
              
              else if ([responseData length] == 0 && responseError == nil)
              {
                  
                  [self showAlert: @"Data Error" :@"Please check credentials or connection"];
                  NSLog(@"data error: %@", responseError);
              }
              else if (responseError != nil && responseError.code == NSURLErrorTimedOut)
              {
                  [self showAlert:@"Data Time Out" :@"Please check internet is available"];
                  NSLog(@"data timeout: %ld", (long)NSURLErrorTimedOut);
              }
              else if (responseError != nil)
              {
                  [self showAlert:@"Access is denied" :@"Please check credentials or connection"];
                  NSLog(@"data download error: %@",responseError);
              }
              
              
              
          }]
         resume];
    }
}
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath
                                                                    *)indexPath

{
    /*
    Contacts *mycell = [[Contacts alloc]init];
    mycell=[tableData objectAtIndex:indexPath.row];
    
    NSLog(@"Phone: %@",mycell.phone);
    
   
    self.alarmsViewController=[self.storyboard instantiateViewControllerWithIdentifier:@"alarms"];
    self.alarmsViewController.phoneNumber=mycell.phone;
    self.alarmsViewController.userName=mycell.name;
    self.alarmsViewController.lastName=mycell.lastName;
    NSString *buttonchoice= [[NSString alloc] initWithFormat:@"%ld", whichButton.selectedSegmentIndex+1];
    self.alarmsViewController.button=buttonchoice;
    

     [self.navigationController pushViewController:self.alarmsViewController animated:NO];

   */
    
}
- (IBAction)plusButtonTapped:(UIBarButtonItem *)sender {
    CNContactPickerViewController *contactPickerController=[[CNContactPickerViewController alloc]init];
    contactPickerController.delegate=self;
    contactPickerController.displayedPropertyKeys = @[CNContactGivenNameKey, CNContactImageDataAvailableKey, CNContactFamilyNameKey, CNContactPhoneNumbersKey, CNContactThumbnailImageDataKey, CNContactIdentifierKey];

    contactPickerController.predicateForEnablingContact = [NSPredicate predicateWithFormat:@"phoneNumbers.@count > 0"];
    contactPickerController.predicateForSelectionOfContact = [NSPredicate predicateWithFormat:@"phoneNumbers.@count == 1"];
    contactPickerController.predicateForSelectionOfProperty = [NSPredicate predicateWithFormat:@"key == 'phoneNumbers'"];
    [self presentViewController:contactPickerController animated:NO completion:nil];
}
-(void)contactPicker:(CNContactPickerViewController *)picker didSelectContactProperty:(CNContactProperty *)contactProperty {
    
    CNContact *contact = contactProperty.contact;
    firstName = contact.givenName;
    lastName = contact.familyName;
    if (lastName == nil) {
        fullName=[NSString stringWithFormat:@"%@",firstName];
    }else if (firstName == nil){
        fullName=[NSString stringWithFormat:@"%@",lastName];
    }
    else{
        fullName=[NSString stringWithFormat:@"%@ %@",firstName,lastName];
    }
    NSString *identify = contactProperty.identifier;//pick the number according to this id!!!
    NSString *lastDisplay = @"";
    for (CNLabeledValue<CNPhoneNumber*>* number in contact.phoneNumbers) {
        if ([number.identifier isEqualToString:identify]) {
            lastDisplay = ((CNPhoneNumber *)number.value).stringValue;
        }
    }
    NSLog(@"phone:%@",lastDisplay);
    phone=lastDisplay;
   
    phone = [phone stringByReplacingOccurrencesOfString:@" "
                                             withString:@""];
    phone = [phone stringByReplacingOccurrencesOfString:@"("
                                             withString:@""];
    phone = [phone stringByReplacingOccurrencesOfString:@")"
                                             withString:@""];
    phone = [phone stringByReplacingOccurrencesOfString:@" "
                                             withString:@""];
    phone = [phone stringByReplacingOccurrencesOfString:@"+"
                                             withString:@""];
    //phone = [phone stringByReplacingOccurrencesOfString:@"52+"                                                    withString:@""];
    //phone = [phone stringByReplacingOccurrencesOfString:@" +521"                                                     withString:@""];
    // Hay que agregar si debe de tomar 11 o 10 digitos
    phone = [[phone componentsSeparatedByCharactersInSet:
              [[NSCharacterSet decimalDigitCharacterSet] invertedSet]]
             componentsJoinedByString:@""];
    unsigned long le=[phone length];
    NSLog(@"Value: %lu",le);
    if (le>10){
        phone=[phone substringWithRange:NSMakeRange(le-10,10)];
    }


    self.enableViewController=[self.storyboard instantiateViewControllerWithIdentifier:@"enable"];
    self.enableViewController.phoneNumber=phone;
    self.enableViewController.userName=fullName;
    NSString *new_button=[[NSString alloc] initWithFormat:@"%ld", whichButton.selectedSegmentIndex+1];
    self.enableViewController.button=new_button;
    //self.navigationController.navigationBar.backItem.title=@"Settings";
    // [self presentViewController:self.enableViewController animated:NO completion:nil];
    [self.navigationController pushViewController:self.enableViewController animated:NO];
}
// Conform Protocol CNContacts
- (void)contactPicker:(CNContactPickerViewController *)picker didSelectContact:(CNContact *)contact
{
    NSLog(@"did select contact ");
    [self dismissViewControllerAnimated:NO completion:nil];
    NSString *fetchName = contact.givenName;
    NSArray *fetchPhoneNo = contact.phoneNumbers;
    firstName = contact.givenName;
    lastName = contact.familyName;
    if (lastName == nil) {
        fullName=[NSString stringWithFormat:@"%@",firstName];
    }else if (firstName == nil){
        fullName=[NSString stringWithFormat:@"%@",lastName];
    }
    else{
        fullName=[NSString stringWithFormat:@"%@ %@",firstName,lastName];
    }
    phone=@"";
    for (CNLabeledValue *telephones in contact.phoneNumbers) {
        label = telephones.label;
        if ([label isEqualToString:CNLabelPhoneNumberMobile] || [label isEqualToString:CNLabelPhoneNumberiPhone] ){
            
            phone = [telephones.value stringValue];
            
            phone = [phone stringByReplacingOccurrencesOfString:@" "
                                                     withString:@""];
            phone = [phone stringByReplacingOccurrencesOfString:@"("
                                                     withString:@""];
            phone = [phone stringByReplacingOccurrencesOfString:@")"
                                                     withString:@""];
            phone = [phone stringByReplacingOccurrencesOfString:@" "
                                                     withString:@""];
            phone = [phone stringByReplacingOccurrencesOfString:@"+"
                                                     withString:@""];
            //phone = [phone stringByReplacingOccurrencesOfString:@"52+"                                                    withString:@""];
            //phone = [phone stringByReplacingOccurrencesOfString:@" +521"                                                     withString:@""];
           // Hay que agregar si debe de tomar 11 o 10 digitos
            phone = [[phone componentsSeparatedByCharactersInSet:
                      [[NSCharacterSet decimalDigitCharacterSet] invertedSet]]
                     componentsJoinedByString:@""];
            unsigned long le=[phone length];
            NSLog(@"Value: %lu",le);
            if (le>10){
                phone=[phone substringWithRange:NSMakeRange(le-10,10)];
            }

            NSLog(@"Phone: %@",phone);
            NSLog(@"label: %@",label);
            break;
        }
    }
    if ([phone length] > 0) {
        NSLog(@"contact details are %@ %@ ",fetchPhoneNo,fetchName);
        self.enableViewController=[self.storyboard instantiateViewControllerWithIdentifier:@"enable"];
        self.enableViewController.phoneNumber=phone;
        self.enableViewController.userName=fullName;
        NSString *new_button=[[NSString alloc] initWithFormat:@"%ld", whichButton.selectedSegmentIndex+1];
        self.enableViewController.button=new_button;
        //self.navigationController.navigationBar.backItem.title=@"Settings";
       // [self presentViewController:self.enableViewController animated:NO completion:nil];
        [self.navigationController pushViewController:self.enableViewController animated:NO];
        /*
        
        self.positionsViewController=[self.storyboard instantiateViewControllerWithIdentifier:@"positions"];
        self.positionsViewController.latitude=mycell.latitude;
        self.positionsViewController.longitude=mycell.longitude;
        self.navigationController.navigationBar.backItem.title=@"Alarms";
        [self.navigationController pushViewController:self.positionsViewController animated:NO];
        */
    }
    else{
        [self showAlert:@"Alert" :@"There is no mobile phone available"];
    }
    
    
    
}

- (void)parseContactWithContact :(CNContact* )contact
{
    //givenName,phoneNumbers,emailAddresses and familyName are the inbuilt properties of CNContactPicker
    NSString * firstName =  contact.givenName;
    NSString * lastName =  contact.familyName;
    NSString * phone = [[contact.phoneNumbers valueForKey:@"value"] valueForKey:@"digits"];
    NSString * email = [contact.emailAddresses valueForKey:@"value"];
    
    NSLog(@"%@ %@ %@ %@",firstName,lastName,phone,email);
}
- (NSMutableArray *)parseAddressWithContac: (CNContact *)contact
{
    NSMutableArray * addressArray = [[NSMutableArray alloc]init];
    CNPostalAddressFormatter * formatter = [[CNPostalAddressFormatter alloc]init];
    NSArray * addresses = (NSArray*)[contact.postalAddresses valueForKey:@"value"];
    
    if (addresses.count > 0) {
        for (CNPostalAddress* newAddress in addresses) {
            [addressArray addObject:[formatter stringFromPostalAddress:newAddress]];
        }
    }
    return addressArray;
}
@end
