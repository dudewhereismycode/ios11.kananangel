//
//  ContactsViewController.m
//  KANAN angel
//
//  Created by Jorge Macias on 11/3/17.
//  Copyright © 2017 Jorge Macias. All rights reserved.
//

#import "ContactsViewController.h"
#import <Contacts/Contacts.h>

@interface ContactsViewController ()
@property (weak, nonatomic) IBOutlet UITableView *tableViewContactData;


@end

@implementation ContactsViewController
{
   
    NSMutableArray *tableData;
   
      NSMutableArray *phones;
      NSMutableArray *tempNames;
     NSMutableArray *tempPhones;
    NSMutableArray *tempData;
 NSMutableArray *labels;
    long myIndex;
}
- (void)viewDidLoad {
    [super viewDidLoad];
        tableData =  [[NSMutableArray alloc] init];
    phones =  [[NSMutableArray alloc] init];
    tempNames =  [[NSMutableArray alloc] init];
    tempPhones =  [[NSMutableArray alloc] init];
    tempData=  [[NSMutableArray alloc] init];
    [self contactsDetailsFromAddressBook];
    // Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

// Table View
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    // Number of rows is the number of time zones in the region for the specified section.
    return [tableData count];
}


- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    static NSString *simpleTableIdentifier = @"SimpleTableItem";
    
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:simpleTableIdentifier];
    
    if (cell == nil) {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:simpleTableIdentifier];
    }
    
    cell.textLabel.text = [tableData objectAtIndex:indexPath.row];
    cell.textLabel.textAlignment = NSTextAlignmentLeft;
    cell.accessoryType = UITableViewCellAccessoryDisclosureIndicator;
    cell.selectionStyle = UITableViewCellSelectionStyleNone;

    /*
    UIButton *button = [UIButton buttonWithType:UIButtonTypeCustom];
    [button addTarget:self
               action:@selector(myCustomFunction:)
     forControlEvents:UIControlEventTouchUpInside];
    [button setTitle:@">" forState:UIControlStateNormal];
    button.frame = CGRectMake(80.0, 210.0, 160.0, 40.0);
    [button setTitleColor:[UIColor blueColor] forState:UIControlStateNormal];

    cell.accessoryView = button;
    */
     return cell;
}
- (void)myCustomFunction:(id)sender{
    NSLog(@"button was clicked");
}
- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (editingStyle == UITableViewCellEditingStyleDelete)
    {   
        
    }
}
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath
                                                                    *)indexPath

{
    myIndex=indexPath.row;
    NSLog(@"Index: %ld",myIndex);
    NSLog(@"Phone: %@",[phones objectAtIndex:indexPath.row]);
    self.enableViewController=[self.storyboard instantiateViewControllerWithIdentifier:@"enable"];
    self.enableViewController.phoneNumber=[phones objectAtIndex:indexPath.row];
    self.enableViewController.userName=[tableData objectAtIndex:indexPath.row];
    [self presentViewController:self.enableViewController animated:NO completion:nil];

}
-(void)contactsDetailsFromAddressBook{
    //ios 9+
    NSLog(@"contacts details");
    CNContactStore *store = [[CNContactStore alloc] init];
    [store requestAccessForEntityType:CNEntityTypeContacts completionHandler:^(BOOL granted, NSError * _Nullable error) {
        if (granted == YES) {
            NSLog(@"GRANTED!!");
            //keys with fetching properties
            NSArray *keys = @[CNContactBirthdayKey,CNContactFamilyNameKey, CNContactGivenNameKey, CNContactPhoneNumbersKey, CNContactImageDataKey, CNContactEmailAddressesKey];
            NSString *containerId = store.defaultContainerIdentifier;
            NSPredicate *predicate = [CNContact predicateForContactsInContainerWithIdentifier:containerId];
            NSError *error;
            NSArray *cnContacts = [store unifiedContactsMatchingPredicate:predicate keysToFetch:keys error:&error];
            if (error) {
                NSLog(@"error fetching contacts %@", error);
            } else {
                
                NSString *phone;
                NSString *label;
                NSString *fullName;
                NSString *firstName;
                NSString *lastName;
                UIImage *profileImage;
                NSDateComponents *birthDayComponent;
                NSMutableArray *contactNumbersArray;
                NSString *birthDayStr;
                NSMutableArray *emailArray;
                NSString* email = @"";
                for (CNContact *contact in cnContacts) {
                    // copy data to my custom Contacts class.
                    /*
                    NSArray <CNLabeledValue<CNPhoneNumber *> *> *phoneNumbers = contact.phoneNumbers;
                    CNLabeledValue<CNPhoneNumber *> *firstPhone = [phoneNumbers firstObject];
                    CNPhoneNumber *number = firstPhone.value;
                    NSString *digits = number.stringValue; //
                    [phones addObject:digits];
                    NSString *label = firstPhone.label; // Mobile
                    [labels addObject:label];
                    NSLog(@"Phone: %@",digits);
                    */
                    firstName = contact.givenName;
                    lastName = contact.familyName;
                    birthDayComponent = contact.birthday;
                    if (birthDayComponent == nil) {
                        // NSLog(@"Component: %@",birthDayComponent);
                        birthDayStr = @"DOB not available";
                    }else{
                        birthDayComponent = contact.birthday;
                        NSInteger day = [birthDayComponent day];
                        NSInteger month = [birthDayComponent month];
                        NSInteger year = [birthDayComponent year];
                        // NSLog(@"Year: %ld, Month: %ld, Day: %ld",(long)year,(long)month,(long)day);
                        birthDayStr = [NSString stringWithFormat:@"%ld/%ld/%ld",(long)day,(long)month,(long)year];
                    }
                    if (lastName == nil) {
                        fullName=[NSString stringWithFormat:@"%@",firstName];
                    }else if (firstName == nil){
                        fullName=[NSString stringWithFormat:@"%@",lastName];
                    }
                    else{
                        fullName=[NSString stringWithFormat:@"%@ %@",firstName,lastName];
                    }
                    NSLog(@"FULL name:%@",fullName);
                    UIImage *image = [UIImage imageWithData:contact.imageData];
                    if (image != nil) {
                        profileImage = image;
                    }else{
                        profileImage = [UIImage imageNamed:@"placeholder.png"];
                    }
                    for (CNLabeledValue *telephones in contact.phoneNumbers) {
                        
                       
                        
                        phone = [telephones.value stringValue];
                        label = telephones.label;
                        
                        if ([label isEqualToString:@"_$!<Mobile>!$_"]){
                            
                            
                            phone = [phone stringByReplacingOccurrencesOfString:@" "
                                                                     withString:@""];
                            phone = [phone stringByReplacingOccurrencesOfString:@"("
                                                                     withString:@""];
                            phone = [phone stringByReplacingOccurrencesOfString:@")"
                                                                     withString:@""];
                            phone = [phone stringByReplacingOccurrencesOfString:@" "
                                                                     withString:@""];
                            phone = [phone stringByReplacingOccurrencesOfString:@"+521"
                                                                     withString:@""];
    
                            phone = [[phone componentsSeparatedByCharactersInSet:
                                      [[NSCharacterSet decimalDigitCharacterSet] invertedSet]]
                                     componentsJoinedByString:@""];
                            [self->tempPhones addObject:phone];
                           // [labels addObject:label];
                            [self->tempData addObject:fullName];
                            [self->tempNames addObject:fullName];
                        }
                        
                        NSLog(@"Phone: %@",phone);
                        NSLog(@"label: %@",label);

                        if ([phone length] > 0) {
                            [contactNumbersArray addObject:phone];
                        }
                    }
                    ////Get all E-Mail addresses from contacts
                    for (CNLabeledValue *label in contact.emailAddresses) {
                        email = label.value;
                        if ([email length] > 0) {
                            [emailArray addObject:email];
                        }
                    }
                    //NSLog(@"EMAIL: %@",email);
                    /*
                    NSDictionary* personDict = [[NSDictionary alloc] initWithObjectsAndKeys: fullName,@"fullName",profileImage,@"userImage",phone,@"PhoneNumbers",birthDayStr,@"BirthDay",email,@"userEmailId", nil];
                     */
                     //NSLog(@"Response: %@",personDict);
                    
                }
                dispatch_async(dispatch_get_main_queue(), ^{
                    [self->tempData sortUsingSelector:@selector(localizedCaseInsensitiveCompare:)];
                    for (int i = 0; i < [self->tempData count];i++ )
                    {
                        NSLog(@"Name: %@",[self->tempData objectAtIndex:i]);
                        for (int y = 0; y < [self->tempNames count];y++ ){
                            //NSLog(@"search:%@",[tempNames objectAtIndex:y]);
                            if ([[self->tempData objectAtIndex:i] isEqualToString:[self->tempNames objectAtIndex:y]]){
                                NSLog(@"Found at:%d Telephone:%@",y,[self->tempPhones objectAtIndex:y]);
                                [self->tableData addObject:[self->tempData objectAtIndex:i]];
                                [self->phones addObject:[self->tempPhones objectAtIndex:y]];

                            }
                        }
                    }
                    [self.tableViewContactData reloadData];
                });
            }
        }
        else{
            NSLog(@"NOT GRANTED!");
        }
    }];
}
@end

