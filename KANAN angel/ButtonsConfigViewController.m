//
//  ButtonsConfigViewController.m
//  KANAN angel
//
//  Created by Jorge Macias on 11/21/17.
//  Copyright © 2017 Jorge Macias. All rights reserved.
//

#import "ButtonsConfigViewController.h"
#import "CoreData.h"
#import "AppDelegate.h"
@interface ButtonsConfigViewController ()
@property (weak, nonatomic) IBOutlet UIButton *leftButton;
@property (weak, nonatomic) IBOutlet UIButton *rightButton;
@property (weak, nonatomic) IBOutlet UIImageView *buttonImage;
@property (weak, nonatomic) IBOutlet UISegmentedControl *whichButton;
@property (weak, nonatomic) IBOutlet UITextField *alarmReponse;
@property (weak, nonatomic) IBOutlet UISwitch *alarmResponseSwitch;
@property (weak, nonatomic) IBOutlet UITextField *alarmResponseTime;
@property (weak, nonatomic) NSString *kananError;
@property (weak, nonatomic) NSString *kananMessage;
@property (weak, nonatomic) NSString *selectedButton;


@end

@implementation ButtonsConfigViewController{
    NSArray* nameArr;
}
@synthesize whichButton;

- (void)viewDidLoad {
    [super viewDidLoad];
    nameArr = [NSArray arrayWithObjects: @"kanan_yellow1.png", @"kanan_yellow2.png",@"kanan_yellow3.png", @"kanan_yellow4.png",@"kanan_yellow5.png", @"kanan_red1.png",@"kanan_red2.png", @"kanan_red3.png",
               @"kanan_red4.png", @"kanan_red5.png",@"kanan_black1.png", @"kanan_black2.png",@"kanan_black3.png", @"kanan_black4.png",@"kanan_black5.png", nil];
    // Do any additional setup after loading the view.
}
- (int) nextElement:(NSString *) name{
    int i = -1;
    int index = -1;
    for (NSString *arrayString in nameArr) {

        i++;
        if ([arrayString isEqualToString:name]) {
            index = i;
            break;
        }
    }
    if (index<([nameArr count]-1)){
        index=index+1;
    }
    else{
        index=0;
    }

    return index;
}
- (int) lastElement:(NSString *) name{
    int i = -1;
    int index = -1;
    for (NSString *arrayString in nameArr) {
        
        i++;
        if ([arrayString isEqualToString:name]) {
            index = i;
            break;
        }
    }
    if (index>0){
        index=index-1;
    }
    else{
        index=(int)([nameArr count]-1);
    }

    return index;
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
-(void) viewWillAppear:(BOOL)animated{

    CoreData *orm=[[CoreData alloc]init];

    NSString *actualButtonName=@"button1";
    NSLog(@"KEY: %@",actualButtonName);
    NSString *actualName=[orm retrieve:actualButtonName];
    self.buttonImage.image=[UIImage imageNamed:actualName];
    self.alarmReponse.text=[orm retrieve:@"response1"];
    self.alarmResponseTime.text=[orm retrieve:@"time1"];
    NSString *follow=[orm retrieve:@"follow1"];
    if ([follow isEqualToString:@"T"]){
        [self.alarmResponseSwitch setOn:YES animated:NO];
    }
    else{
         [self.alarmResponseSwitch setOn:NO animated:NO];
    }

}
- (IBAction)decodeButton:(UISegmentedControl *)sender {
    NSLog(@"selected: %ld",whichButton.selectedSegmentIndex);
    CoreData *orm=[[CoreData alloc]init];
    NSString *number = [[NSString alloc] initWithFormat:@"%ld", whichButton.selectedSegmentIndex+1];
    
    NSString *actualButtonName=[@"button" stringByAppendingString:number];
    NSLog(@"KEY: %@",actualButtonName);
    NSString *actualName=[orm retrieve:actualButtonName];
    self.buttonImage.image=[UIImage imageNamed:actualName];
    NSString *actualResponseName=[@"response" stringByAppendingString:number];
    self.alarmReponse.text=[orm retrieve:actualResponseName];
    NSString *actualResponseTime=[@"time" stringByAppendingString:number];
    self.alarmResponseTime.text=[orm retrieve:actualResponseTime];
    NSString *actualFollowName=[@"follow" stringByAppendingString:number];
    NSString *follow=[orm retrieve:actualFollowName];
    if ([follow isEqualToString:@"T"]){
        [self.alarmResponseSwitch setOn:YES animated:NO];
    }
    else{
        [self.alarmResponseSwitch setOn:NO animated:NO];
    }
    if(whichButton.selectedSegmentIndex==2){
        self.leftButton.userInteractionEnabled=NO;
        self.leftButton.enabled=NO;
        self.rightButton.userInteractionEnabled=NO;
        self.rightButton.enabled=NO;
    }
    else{
        self.leftButton.userInteractionEnabled=YES;
        self.leftButton.enabled=YES;
        self.rightButton.userInteractionEnabled=YES;
        self.rightButton.enabled=YES;
        
    }
}
- (IBAction)rightButtonTapped:(UIButton *)sender {
    NSLog(@"Right which:%ld",whichButton.selectedSegmentIndex);
    CoreData *orm=[[CoreData alloc]init];
    NSString *number = [[NSString alloc] initWithFormat:@"%ld", whichButton.selectedSegmentIndex+1];
    
    NSString *actualButtonName=[@"button" stringByAppendingString:number];
    NSLog(@"KEY: %@",actualButtonName);
    NSString *actualName=[orm retrieve:actualButtonName];
    NSLog(@"actual button name: %@",actualName);
     NSString *nextName=[nameArr objectAtIndex:[self nextElement:actualName]];
    NSLog(@"next name: %@",nextName);

    self.buttonImage.image=[UIImage imageNamed:nextName];
    [orm save:actualButtonName :nextName];


}
- (IBAction)leftButtonTapped:(UIButton *)sender {
        NSLog(@"Left");
    
    NSLog(@"Right which:%ld",whichButton.selectedSegmentIndex);
    CoreData *orm=[[CoreData alloc]init];
    NSString *number = [[NSString alloc] initWithFormat:@"%ld", whichButton.selectedSegmentIndex+1];
    
    NSString *actualButtonName=[@"button" stringByAppendingString:number];
    NSLog(@"KEY: %@",actualButtonName);
    NSString *actualName=[orm retrieve:actualButtonName];
    NSLog(@"actual button name: %@",actualName);
    NSString *nextName=[nameArr objectAtIndex:[self lastElement:actualName]];
    NSLog(@"next name: %@",nextName);
    
    self.buttonImage.image=[UIImage imageNamed:nextName];
    [orm save:actualButtonName :nextName];
    
}
- (IBAction)didonExitAlarmResponse:(UITextField *)sender {
}
- (IBAction)didTimeExit:(UITextField *)sender {
}
-(NSString *) getphonenumber{
    CoreData *orm=[[CoreData alloc] init];
    return [orm retrieve:@"phone_number"];
}
-(void)showAlert:(NSString *)title :(NSString *)body {
    UIAlertController * alert=   [UIAlertController
                                  alertControllerWithTitle:title
                                  message:body
                                  preferredStyle:UIAlertControllerStyleAlert];
    
    UIAlertAction* ok = [UIAlertAction
                         actionWithTitle:@"OK"
                         style:UIAlertActionStyleDefault
                         handler:^(UIAlertAction * action)
                         {
                             [alert dismissViewControllerAnimated:YES completion:nil];
                             
                         }];
    
    [alert addAction:ok];
    [self presentViewController:alert animated:YES completion:nil];
}
-(void) temp{

}
- (IBAction)saveButtonTapped:(UIButton *)sender {
    NSScanner *scanner = [NSScanner scannerWithString:self.alarmResponseTime.text];
    BOOL isNumeric = [scanner scanInteger:NULL] && [scanner isAtEnd];
    if (isNumeric==false){
         [self showAlert:@"Message" : @"You must capture a valid number (0-60)"];
    }
    else{
        unsigned long mydata=[self.alarmReponse.text length];
        
        if (mydata<1){
              [self showAlert:@"Message" : @"Empty Alarm Response text"];
        }
        else{
            NSLog(@"phone: %@",[self getphonenumber] );
            NSLog(@"button: %ld", whichButton.selectedSegmentIndex+1 );
            self.selectedButton = [NSString stringWithFormat:@"%ld",whichButton.selectedSegmentIndex+1];
            NSLog(@"text: %@", self.alarmReponse.text );
            NSLog(@"time: %@", self.alarmResponseTime.text );
            NSString *follow;
            if([self.alarmResponseSwitch isOn]){
                follow=@"T";
            }
            else{
                follow=@"F";
            }
            NSLog(@"follow: %@", follow);
            NSString *mask=[NSString stringWithFormat:@"%@%@%@", @"http://", KANAN_url, @"/api/noa/setAngelButton?myphone=%@&button=%@&response=%@&time=%@&follow=%@"];
            
            NSString *urlString = [NSString stringWithFormat:mask,
                                   [self getphonenumber],
                                   self.selectedButton,
                                   [self.alarmReponse.text stringByAddingPercentEncodingWithAllowedCharacters:[NSCharacterSet URLHostAllowedCharacterSet]],
                                   self.alarmResponseTime.text,
                                   follow
                                   ];
            NSLog(@"URL: %@",urlString);
            
            NSMutableURLRequest *request = [[NSMutableURLRequest alloc] initWithURL:[NSURL URLWithString:urlString]];
            [request setHTTPMethod:@"GET"];
            [request setValue:@"application/json" forHTTPHeaderField:@"Accept"];
            [request setValue:@"application/json" forHTTPHeaderField:@"Content-Type"];
            NSURLSession *session = [NSURLSession sharedSession];
            [[session dataTaskWithURL:[NSURL URLWithString:urlString]
                    completionHandler:^(NSData *responseData,
                                        NSURLResponse *responseCode,
                                        NSError *responseError)
              {
                  // This will get the NSURLResponse into NSHTTPURLResponse format
                  NSHTTPURLResponse* httpResponse = (NSHTTPURLResponse*)responseCode;
                  // This will Fetch the status code from NSHTTPURLResponse object
                  int responseStatusCode = (int)[httpResponse statusCode];
                  //Just to make sure, it works or not
                  NSLog(@"Status Code :: %d", responseStatusCode);
                  NSDictionary *userInfo = [responseError userInfo];
                  NSString *errorString = [[userInfo objectForKey:NSUnderlyingErrorKey] localizedDescription];
                  NSLog(@"Error string: %@",errorString);
                  if ([responseData length] > 0 && responseError == nil)
                  {
                      id jsonData = [NSJSONSerialization JSONObjectWithData:responseData options:NSJSONReadingAllowFragments error:&responseError];
                      NSDictionary *responseDict = jsonData;
                      //NSString *kananError = @"";
                      //NSString *kananMessage = @"Distributor";
                      NSString *title=@"Error1";
                      NSLog(@"sucess reading");
                      dispatch_async(dispatch_get_main_queue(), ^{
                          // UI Update
                          if ([responseDict objectForKey:@"Error"])
                          {

                              if ([[responseDict objectForKey:@"Error"] isEqual:@"none"])
                              {
                                  CoreData *orm=[[CoreData alloc] init];
                                  
                                  NSString *responseKey=[@"response" stringByAppendingString:self.selectedButton];
                                  NSString *timeKey=[@"time" stringByAppendingString:self.selectedButton];
                                  NSString *followKey=[@"follow" stringByAppendingString:self.selectedButton];
                                  
                                  [orm save:responseKey:self.alarmReponse.text];
                                  [orm save:timeKey :self.alarmResponseTime.text];
                                  if([self.alarmResponseSwitch isOn]){
                                      [orm save:followKey :@"T"];
                                  }
                                  else{
                                      [orm save:followKey :@"F"];
                                  }
                                  [self showAlert:@"Message" : @"Data saved"];
                                  
                              }
                              
                              else
                              {
                                  self.kananMessage = [responseDict objectForKey:@"message"];
                                  NSLog(@"KANAN Message: %@",self.kananMessage);
                                  [self showAlert:title :self.kananMessage];
                              }
                              
                          }
                      });
                      
                  }
                  
                  else if ([responseData length] == 0 && responseError == nil)
                  {
                      
                      [self showAlert: @"Data Error" :@"Please check credentials or connection"];
                      NSLog(@"data error: %@", responseError);
                  }
                  else if (responseError != nil && responseError.code == NSURLErrorTimedOut)
                  {
                      [self showAlert:@"Data Time Out" :@"Please check internet is available"];
                      NSLog(@"data timeout: %ld", (long)NSURLErrorTimedOut);
                  }
                  else if (responseError != nil)
                  {
                      [self showAlert:@"Access is denied" :@"Please check credentials or connection"];
                      NSLog(@"data download error: %@",responseError);
                  }
                  
                  
                  
              }]
             resume];
        }
        
    }
   
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
