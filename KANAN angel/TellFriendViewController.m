//
//  TellFriendViewController.m
//  KANAN angel
//
//  Created by Jorge Macias on 11/8/17.
//  Copyright © 2017 Jorge Macias. All rights reserved.
//

#import "TellFriendViewController.h"
#import <Contacts/Contacts.h>


@interface TellFriendViewController ()
@property (weak, nonatomic) IBOutlet UITableView *tableView;
@property (weak, nonatomic) IBOutlet UIButton *cancelButton;
@property (weak, nonatomic) IBOutlet UIButton *sendButton;

@end

@implementation TellFriendViewController
{
    NSMutableArray *tableData;
    NSMutableArray *subtitles;
    NSString *emailOrsms;
    MFMailComposeViewController *mailVC;
    NSMutableArray *tempTitles;
    NSMutableArray *tempSubtitles;
    NSMutableArray *tempData;
}
- (void)viewDidLoad {
    [super viewDidLoad];
    emailOrsms=@"";
    tableData =  [[NSMutableArray alloc] init];
    subtitles =  [[NSMutableArray alloc] init];
    tempTitles=[[NSMutableArray alloc] init];
    tempSubtitles=[[NSMutableArray alloc] init];
    tempData=[[NSMutableArray alloc] init];
    // Do any additional setup after loading the view.
}
- (void) viewWillAppear:(BOOL)animated{
    
    UIAlertController * alert=[UIAlertController alertControllerWithTitle:nil
                                                                  message:nil
                                                           preferredStyle:UIAlertControllerStyleAlert];
    
    UIAlertAction* smsButton = [UIAlertAction actionWithTitle:@"SMS"
                                                        style:UIAlertActionStyleDefault
                                                      handler:^(UIAlertAction * action)
    {
        self->emailOrsms=@"S";
        /** What we write here???????? **/
        NSLog(@"you pressed SMS button");
        [self.tableView setEditing:YES animated:YES];
        [self phonesFromAddressBook];
        self.tableView.hidden=NO;
        self.cancelButton.hidden=NO;
        self.sendButton.hidden=NO;
        [self.tableView reloadData];
    }];
    
    UIAlertAction* mailButton = [UIAlertAction actionWithTitle:@"Mail"
                                                       style:UIAlertActionStyleDefault
                                                     handler:^(UIAlertAction * action)
    {
        if ([MFMailComposeViewController canSendMail]) {
            //Your code will go here
            self->mailVC.mailComposeDelegate = self;
            NSLog(@"you pressed mail button");
            self->emailOrsms=@"E";
            [self.tableView setEditing:YES animated:YES];
            [self emailsFromAddressBook];
            self.tableView.hidden=NO;
            self.cancelButton.hidden=NO;
            self.sendButton.hidden=NO;
            [self.tableView reloadData];
        } else {
            //This device cannot send email
            [self.navigationController popToRootViewControllerAnimated:YES];
        }
        /** What we write here???????? **/

    }];
    
    UIAlertAction* cancelButton = [UIAlertAction actionWithTitle:@"Cancel"
                                                         style:UIAlertActionStyleDefault
                                                       handler:^(UIAlertAction * action)
                                 {
                                     /** What we write here???????? **/
                                     NSLog(@"you pressed cancel button");
                                     [self.navigationController popToRootViewControllerAnimated:YES];

                                 }];
    [alert addAction:smsButton];
    [alert addAction:mailButton];
    [alert addAction:cancelButton];
    [self presentViewController:alert animated:YES completion:nil];

}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    // Number of rows is the number of time zones in the region for the specified section.
    return [tableData count];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    static NSString *simpleTableIdentifier = @"SimpleTableItem";
    
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:simpleTableIdentifier];
    
    if (cell == nil) {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleSubtitle reuseIdentifier:simpleTableIdentifier];
    }

    cell.textLabel.text = [tableData objectAtIndex:indexPath.row];
    cell.detailTextLabel.text = [subtitles objectAtIndex:indexPath.row];
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath
                                                                    *)indexPath

{
    NSLog(@"Index: %ld",indexPath.row);
}
- (UITableViewCellEditingStyle) tableView:(UITableView *)tableView editingStyleForRowAtIndexPath:(NSIndexPath *)indexPath{
    return 3;
}

- (IBAction)cancelButtonTapped:(UIButton *)sender {
    [self.navigationController popToRootViewControllerAnimated:YES];
}
- (IBAction)sendButtonTapped:(UIButton *)sender {
    NSArray *selectedRows=[self.tableView indexPathsForSelectedRows];
    NSMutableArray *recipients = [[NSMutableArray alloc] init];
    NSLog(@"Function: %@",emailOrsms);
    for (int i=0; i<selectedRows.count; i++)
    {
        NSIndexPath *indexPath = [selectedRows objectAtIndex:i];
        NSInteger row = indexPath.row;
        [recipients addObject:[subtitles objectAtIndex:row]];
        NSLog(@"Selected Row: %ld Value: %@",row,[subtitles objectAtIndex:row]);

        

    }
    if ([emailOrsms isEqualToString:@"E"]){
        if ([MFMailComposeViewController canSendMail]) {
            MFMailComposeViewController *mail =
            [[MFMailComposeViewController alloc] init];
            mail.mailComposeDelegate = self;
            [mail setSubject:@"Try KANAN Angel"];
            [mail setMessageBody:@"Try KANAN angel here: http://app.dwim.mx" isHTML:NO];
            //    cell.textLabel.text = [tableData objectAtIndex:indexPath.row];
            //cell.detailTextLabel.text = [subtitles objectAtIndex:indexPath.row];
            //[mail setToRecipients:@[[subtitles objectAtIndex:row]]];
            [mail setToRecipients:recipients];
            [self presentViewController:mail animated:YES completion:NULL];
        } else {
            NSLog(@"This device cannot send email");
        }
    }
    else{
        if([MFMessageComposeViewController canSendText]) {
            MFMessageComposeViewController *sms = [[MFMessageComposeViewController alloc] init]; // Create message VC
            sms.messageComposeDelegate = self; // Set delegate to current instance
            /*
            NSMutableArray *recipients = [[NSMutableArray alloc] init]; // Create an array to hold the recipients
            [recipients addObject:[subtitles objectAtIndex:row]]; // Append example phone number to array
            */
             sms.recipients = recipients; // Set the recipients of the message to the created array
            
            sms.body = @"Try KANAN Angel here: http://app.dwim.mx"; // Set initial text to example message
            [self presentViewController:sms animated:YES completion:NULL];
            /*
            dispatch_async(dispatch_get_main_queue(), ^{ // Present VC when possible
                //
            });
             */
        }
        else {
            NSLog(@"This device cannot send sms");
        }
    }
    [self.navigationController popToRootViewControllerAnimated:YES];
}
-(void)emailsFromAddressBook{
    //ios 9+
    NSLog(@"contacts details");
    CNContactStore *store = [[CNContactStore alloc] init];
    [store requestAccessForEntityType:CNEntityTypeContacts completionHandler:^(BOOL granted, NSError * _Nullable error) {
        if (granted == YES) {
            NSLog(@"GRANTED!!");
            //keys with fetching properties
            NSArray *keys = @[CNContactBirthdayKey,CNContactFamilyNameKey, CNContactGivenNameKey, CNContactPhoneNumbersKey, CNContactImageDataKey, CNContactEmailAddressesKey];
            NSString *containerId = store.defaultContainerIdentifier;
            NSPredicate *predicate = [CNContact predicateForContactsInContainerWithIdentifier:containerId];
            NSError *error;
            NSArray *cnContacts = [store unifiedContactsMatchingPredicate:predicate keysToFetch:keys error:&error];
            if (error) {
                NSLog(@"error fetching contacts %@", error);
            } else {
                
                NSString *phone;
                NSString *label;
                NSString *fullName;
                NSString *firstName;
                NSString *lastName;
                UIImage *profileImage;
                NSDateComponents *birthDayComponent;
                NSMutableArray *contactNumbersArray;
                NSString *birthDayStr;
                NSMutableArray *emailArray;
                NSString* email = @"";
                for (CNContact *contact in cnContacts) {
                    // copy data to my custom Contacts class.
                    /*
                     NSArray <CNLabeledValue<CNPhoneNumber *> *> *phoneNumbers = contact.phoneNumbers;
                     CNLabeledValue<CNPhoneNumber *> *firstPhone = [phoneNumbers firstObject];
                     CNPhoneNumber *number = firstPhone.value;
                     NSString *digits = number.stringValue; //
                     [phones addObject:digits];
                     NSString *label = firstPhone.label; // Mobile
                     [labels addObject:label];
                     NSLog(@"Phone: %@",digits);
                     */
                    firstName = contact.givenName;
                    lastName = contact.familyName;
                    birthDayComponent = contact.birthday;
                    if (birthDayComponent == nil) {
                        // NSLog(@"Component: %@",birthDayComponent);
                        birthDayStr = @"DOB not available";
                    }else{
                        birthDayComponent = contact.birthday;
                        NSInteger day = [birthDayComponent day];
                        NSInteger month = [birthDayComponent month];
                        NSInteger year = [birthDayComponent year];
                        // NSLog(@"Year: %ld, Month: %ld, Day: %ld",(long)year,(long)month,(long)day);
                        birthDayStr = [NSString stringWithFormat:@"%ld/%ld/%ld",(long)day,(long)month,(long)year];
                    }
                    if (lastName == nil) {
                        fullName=[NSString stringWithFormat:@"%@",firstName];
                    }else if (firstName == nil){
                        fullName=[NSString stringWithFormat:@"%@",lastName];
                    }
                    else{
                        fullName=[NSString stringWithFormat:@"%@ %@",firstName,lastName];
                    }
                    NSLog(@"FULL name:%@",fullName);
                    UIImage *image = [UIImage imageWithData:contact.imageData];
                    if (image != nil) {
                        profileImage = image;
                    }else{
                        profileImage = [UIImage imageNamed:@"placeholder.png"];
                    }
                    for (CNLabeledValue *telephones in contact.phoneNumbers) {
                        
                        
                        
                        phone = [telephones.value stringValue];
                        label = telephones.label;
                        
                        if ([label isEqualToString:@"_$!<Mobile>!$_"]){
                            
                            
                            phone = [phone stringByReplacingOccurrencesOfString:@" "
                                                                     withString:@""];
                            phone = [phone stringByReplacingOccurrencesOfString:@"("
                                                                     withString:@""];
                            phone = [phone stringByReplacingOccurrencesOfString:@")"
                                                                     withString:@""];
                            phone = [phone stringByReplacingOccurrencesOfString:@" "
                                                                     withString:@""];
                            phone = [phone stringByReplacingOccurrencesOfString:@"+521"
                                                                     withString:@""];
                            
                            phone = [[phone componentsSeparatedByCharactersInSet:
                                      [[NSCharacterSet decimalDigitCharacterSet] invertedSet]]
                                     componentsJoinedByString:@""];

                            // [labels addObject:label];

                        }
                        
                        NSLog(@"Phone: %@",phone);
                        NSLog(@"label: %@",label);
                        
                        if ([phone length] > 0) {
                            [contactNumbersArray addObject:phone];
                        }
                    }
                    int flag=0;
                    ////Get all E-Mail addresses from contacts
                    for (CNLabeledValue *label in contact.emailAddresses) {
                        email = label.value;
                        if ([email length] > 0) {
                            [emailArray addObject:email];
                            if (flag==0){
                              //[tableData addObject:fullName];
                                //[subtitles addObject:email];
                                [self->tempData addObject:fullName];
                                [self->tempTitles addObject:fullName];
                                [self->tempSubtitles addObject:email];
                                flag++;
                            }
                        }
                    }
                    //NSLog(@"EMAIL: %@",email);
                    /*
                     NSDictionary* personDict = [[NSDictionary alloc] initWithObjectsAndKeys: fullName,@"fullName",profileImage,@"userImage",phone,@"PhoneNumbers",birthDayStr,@"BirthDay",email,@"userEmailId", nil];
                     */
                    //NSLog(@"Response: %@",personDict);
                    
                }
                dispatch_async(dispatch_get_main_queue(), ^{
                    [self->tempData sortUsingSelector:@selector(localizedCaseInsensitiveCompare:)];
                    for (int i = 0; i < [self->tempData count];i++ )
                    {
                        NSLog(@"Name: %@",[self->tempData objectAtIndex:i]);
                        for (int y = 0; y < [self->tempTitles count];y++ ){
                            //NSLog(@"search:%@",[tempNames objectAtIndex:y]);
                            if ([[self->tempData objectAtIndex:i] isEqualToString:[self->tempTitles objectAtIndex:y]]){
                                NSLog(@"Found at:%d Telephone:%@",y,[self->tempSubtitles objectAtIndex:y]);
                                [self->tableData addObject:[self->tempData objectAtIndex:i]];
                                [self->subtitles addObject:[self->tempSubtitles objectAtIndex:y]];
                                
                            }
                        }
                    }
                    [self.tableView reloadData];
  
                });
            }
        }
        else{
            NSLog(@"NOT GRANTED!");
        }
    }];
}
-(void)phonesFromAddressBook{
    //ios 9+
    NSLog(@"contacts details");
    CNContactStore *store = [[CNContactStore alloc] init];
    [store requestAccessForEntityType:CNEntityTypeContacts completionHandler:^(BOOL granted, NSError * _Nullable error) {
        if (granted == YES) {
            NSLog(@"GRANTED!!");
            //keys with fetching properties
            NSArray *keys = @[CNContactBirthdayKey,CNContactFamilyNameKey, CNContactGivenNameKey, CNContactPhoneNumbersKey, CNContactImageDataKey, CNContactEmailAddressesKey];
            NSString *containerId = store.defaultContainerIdentifier;
            NSPredicate *predicate = [CNContact predicateForContactsInContainerWithIdentifier:containerId];
            NSError *error;
            NSArray *cnContacts = [store unifiedContactsMatchingPredicate:predicate keysToFetch:keys error:&error];
            if (error) {
                NSLog(@"error fetching contacts %@", error);
            } else {
                
                NSString *phone;
                NSString *label;
                NSString *fullName;
                NSString *firstName;
                NSString *lastName;
                UIImage *profileImage;
                NSDateComponents *birthDayComponent;
                NSMutableArray *contactNumbersArray;
                NSString *birthDayStr;

                for (CNContact *contact in cnContacts) {
                    // copy data to my custom Contacts class.
                    /*
                     NSArray <CNLabeledValue<CNPhoneNumber *> *> *phoneNumbers = contact.phoneNumbers;
                     CNLabeledValue<CNPhoneNumber *> *firstPhone = [phoneNumbers firstObject];
                     CNPhoneNumber *number = firstPhone.value;
                     NSString *digits = number.stringValue; //
                     [phones addObject:digits];
                     NSString *label = firstPhone.label; // Mobile
                     [labels addObject:label];
                     NSLog(@"Phone: %@",digits);
                     */
                    firstName = contact.givenName;
                    lastName = contact.familyName;
                    birthDayComponent = contact.birthday;
                    if (birthDayComponent == nil) {
                        // NSLog(@"Component: %@",birthDayComponent);
                        birthDayStr = @"DOB not available";
                    }else{
                        birthDayComponent = contact.birthday;
                        NSInteger day = [birthDayComponent day];
                        NSInteger month = [birthDayComponent month];
                        NSInteger year = [birthDayComponent year];
                        // NSLog(@"Year: %ld, Month: %ld, Day: %ld",(long)year,(long)month,(long)day);
                        birthDayStr = [NSString stringWithFormat:@"%ld/%ld/%ld",(long)day,(long)month,(long)year];
                    }
                    if (lastName == nil) {
                        fullName=[NSString stringWithFormat:@"%@",firstName];
                    }else if (firstName == nil){
                        fullName=[NSString stringWithFormat:@"%@",lastName];
                    }
                    else{
                        fullName=[NSString stringWithFormat:@"%@ %@",firstName,lastName];
                    }
                    NSLog(@"FULL name:%@",fullName);
                    UIImage *image = [UIImage imageWithData:contact.imageData];
                    if (image != nil) {
                        profileImage = image;
                    }else{
                        profileImage = [UIImage imageNamed:@"placeholder.png"];
                    }
                    int flag=0;
                    for (CNLabeledValue *telephones in contact.phoneNumbers) {
                        
                        
                        
                        phone = [telephones.value stringValue];
                        label = telephones.label;
                        
                        if ([label isEqualToString:@"_$!<Mobile>!$_"]){
                            
                            
                            phone = [phone stringByReplacingOccurrencesOfString:@" "
                                                                     withString:@""];
                            phone = [phone stringByReplacingOccurrencesOfString:@"("
                                                                     withString:@""];
                            phone = [phone stringByReplacingOccurrencesOfString:@")"
                                                                     withString:@""];
                            phone = [phone stringByReplacingOccurrencesOfString:@" "
                                                                     withString:@""];
                            phone = [phone stringByReplacingOccurrencesOfString:@"+521"
                                                                     withString:@""];
                            
                            phone = [[phone componentsSeparatedByCharactersInSet:
                                      [[NSCharacterSet decimalDigitCharacterSet] invertedSet]]
                                     componentsJoinedByString:@""];
                            if (flag==0){
                                [self->tempData addObject:fullName];
                                [self->tempTitles addObject:fullName];
                                [self->tempSubtitles addObject:phone];
                                NSLog(@"Phone: %@",phone);
                                NSLog(@"label: %@",label);
                                flag++;
                            }
                            // [labels addObject:label];
                            
                        }
                        

                        
                        if ([phone length] > 0) {
                            [contactNumbersArray addObject:phone];
                        }
                    }

                    
                    

                    
                }
                dispatch_async(dispatch_get_main_queue(), ^{
                   // [self.tableView reloadData];
                    [self->tempData sortUsingSelector:@selector(localizedCaseInsensitiveCompare:)];
                    for (int i = 0; i < [self->tempData count];i++ )
                    {
                        NSLog(@"Name: %@",[self->tempData objectAtIndex:i]);
                        for (int y = 0; y < [self->tempTitles count];y++ ){
                            //NSLog(@"search:%@",[tempNames objectAtIndex:y]);
                            if ([[self->tempData objectAtIndex:i] isEqualToString:[self->tempTitles objectAtIndex:y]]){
                                NSLog(@"Found at:%d Telephone:%@",y,[self->tempSubtitles objectAtIndex:y]);
                                [self->tableData addObject:[self->tempData objectAtIndex:i]];
                                [self->subtitles addObject:[self->tempSubtitles objectAtIndex:y]];
                                
                            }
                        }
                    }
                    [self.tableView reloadData];
                    
                });
            }
        }
        else{
            NSLog(@"NOT GRANTED!");
        }
    }];
}
- (void)mailComposeController:(MFMailComposeViewController *)controller didFinishWithResult:(MFMailComposeResult)result error:(NSError *)error
{
    switch (result) {
        case MFMailComposeResultSent:
            //Email sent
            NSLog(@"Email sent");
            break;
        case MFMailComposeResultSaved:
            //Email saved
            NSLog(@"Email saved");
            break;
        case MFMailComposeResultCancelled:
            //Handle cancelling of the email
            NSLog(@"Email canceled");
            break;
        case MFMailComposeResultFailed:
            //Handle failure to send.
            NSLog(@"Email failure");
            break;
        default:
            //A failure occurred while completing the email
            break;
    }
    [self dismissViewControllerAnimated:YES completion:NULL];
}

- (void)messageComposeViewController:(MFMessageComposeViewController *)controller didFinishWithResult:(MessageComposeResult)result
{
    switch (result) {
        case MessageComposeResultSent:
            //Email sent
            NSLog(@"SMS sent");
            break;

        default:
            //A failure occurred while completing the email
            NSLog(@"Failure");
            break;
    }
    [self dismissViewControllerAnimated:YES completion:NULL];
    
}
@end
